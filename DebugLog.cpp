#include <stdio.h>
#include <string>
#include <stdarg.h>
#include "DebugLog.h"

#ifdef _WIN32
#include <Windows.h>
	extern void SaveToLog(std::string Log,int _AdvancedLog);	//������ � ���
#endif _WIN32

std::string DebugLogName="";
long DebugLogMaxSize=-1;

std::string ctime1(time_t Param)
{
	char TimeStr[200];
	if(Param>0)
	{
		tm tmt=*gmtime(&Param);
		sprintf(TimeStr,"%4.4d-%2.2d-%2.2d %2.2d:%2.2d:%2.2d",tmt.tm_year+1900,tmt.tm_mon+1,tmt.tm_mday,tmt.tm_hour,tmt.tm_min,tmt.tm_sec);
	}
	else
	{
		strcpy(TimeStr,"1970-01-01 00:00:00");
	}
	return std::string(TimeStr);
}

std::string cdate(time_t Param)
{
	char TimeStr[200];
	if(Param>0)
	{
		tm tmt=*localtime(&Param);
		sprintf(TimeStr,"%4.4d-%2.2d-%2.2d",tmt.tm_year+1900,tmt.tm_mon+1,tmt.tm_mday);
	}
	else
	{
		strcpy(TimeStr,"1970-01-01");
	}
	return std::string(TimeStr);
}


void OpenDebugLog(const std::string &Name,long MaxSize)
{
	DebugLogName=Name;
	DebugLogMaxSize=MaxSize;
}

void WriteToDebugLog(int s,int ClientId,bool IsErr,const char *ErrorMessage,...)
{
	va_list ap;
	va_start (ap, ErrorMessage);
	char EventBody[18000];
	vsprintf(EventBody,ErrorMessage,ap);
	va_end (ap);
#ifdef _WIN32
	//��� ��������� ��� �����
	char Logg[4096];
	wsprintf(Logg,"[sock:%8.8x][id:%8.8x]%s%s\n",s,ClientId,(IsErr)?"ERR: ":" ",EventBody);
	SaveToLog(Logg,1);	

	return;
#endif _WIN32

	long FileSize=0;
	if(DebugLogName.size() && DebugLogMaxSize >0)
	{
		time_t tm1=time(NULL);
		FILE *F=fopen((DebugLogName+"_"+cdate(tm1)).c_str(),"a");
		if(F)
		{
			fprintf(F,"[%s][sock:%8.8x][id:%8.8x]%s%s\n",ctime1(tm1).c_str(),s,ClientId,(IsErr)?"ERR: ":" ",EventBody);
			FileSize=ftell(F);
			fclose(F);
		}
		if(FileSize>DebugLogMaxSize) //���� ������ ����� �������� ����������, �������� ��������� ����
		{
			DebugLogName+="_";
		}
	}
}
