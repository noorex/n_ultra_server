#include "Directory.h"
#include "DebugLog.h"
#include "n_util.h"
#include <algorithm>

std::map <int,std::string> CarrierTypeDirectory;
std::map <int,std::string> OrderStatusDirectory;
std::map <int,std::string> CityDirectory;
std::map <int,ClientStruct> ClientDb;

std::map <int,ultra_rates_disp> DispRates;
std::map <int,ultra_rates_service> ServiceRates;
std::map <int,ultra_rates_service_city> CityRates;

#ifdef _WIN32
#include <windows.h>
extern void	SendMessageRepaint();
#endif //_WIN32

PGconn *PostgresConnection=0;
static bool isPostgresConnected=false;
int ExpiredTime=300;
int DBExpiredTime=7200;

char ReturnOrderStatusName[100];
char RefuseOrderStatusName[100];

std::string Convert2XML(std::string Src)
{
	std::string Res;

	std::string::iterator i;
	for(i=Src.begin();i!=Src.end();i++)
	{
		switch(*i)
		{
		case '\"': Res+="&quot;"; break;
		case '<': Res+="&lt;"; break;
		case '>': Res+="&gt;"; break;
		case '&': Res+="&amp;"; break;
		default: Res+=*i; break;
		}
	}
	return Res;
}

std::string ToString(int a)
{
	char StrA[12];
	sprintf(StrA,"%d",a);
	return std::string(StrA);
}

std::string ToString(float a)
{
	char StrA[20];
	sprintf(StrA,"%10.2f",a);
	return std::string(StrA);
}


std::string ListToString(const std::list <int> &a)
{
	std::string RetVal;
	std::list <int>::const_iterator i;
	for(i=a.begin();i!=a.end();i++)
	{
		char StrA[12];
		sprintf(StrA,"%d ",*i);
		RetVal+=std::string(StrA);
	}
	RetVal=RetVal.substr(0,RetVal.size()-1);
	return RetVal;
}

bool ConnectDB(const std::string &Addr,const std::string & Port,const std::string &Login,const std::string &Password,const std::string &DBName)
{
	strcpy(ReturnOrderStatusName,"������� ������");
	strcpy(RefuseOrderStatusName,"����� �������");
	ConvertAnsiToUTF8(ReturnOrderStatusName,100);
	ConvertAnsiToUTF8(RefuseOrderStatusName,100);

	//disconnect db
	DisconnectDB();
	//Now postgres only supported
	PostgresConnection = PQsetdbLogin(
		Addr.c_str(),
		Port.c_str(),
		"", 
		"",
		DBName.c_str(),
		Login.c_str(),
		Password.c_str());

	if (PQstatus(PostgresConnection) != CONNECTION_OK)
	{
		WriteToDebugLog(0,0,true,"ConnectDB not success");
		PostgresConnection = 0;
		isPostgresConnected=false;
#ifdef _WIN32
	SendMessageRepaint();
#endif _WIN32		
		return false;
	}
	WriteToDebugLog(0,0,false,"ConnectDB success");
	isPostgresConnected=true;
#ifdef _WIN32
	SendMessageRepaint();
#endif _WIN32
	return true; 
}

bool ReconnectDB()
{
	DisconnectDB();
	return ConnectDB(CommonConfig.DBAddr,CommonConfig.DBPort,CommonConfig.DBLogin,CommonConfig.DBPassword,CommonConfig.DBName);
}

bool isSQLServerConnected()
{
	return isPostgresConnected;
}


void DisconnectDB()
{
	if(PostgresConnection)
		PQfinish(PostgresConnection); 
	PostgresConnection=0;
	isPostgresConnected=false;
	WriteToDebugLog(0,0,false,"DisconnectDB");
#ifdef _WIN32
	SendMessageRepaint();
#endif _WIN32	
}

int ReadClients()
{
	PGresult*		pResult;
	ExecStatusType tQueryStatus;

	std::string SQLQuery="SELECT \"id\",\"FullName\",\"ShortName\",\"INN\",\"KPP\",\"LegalAddress\",\"FactAddress\",\"LeaderName\",\"EMail\",\"Comment\",\"IsTest\",\"IsService\",\"isDialer\",\"IsDisp\",\"Login\",\"Password\",\"CreditLimit\",extract(epoch from \"ContractDate\")::bigint,\"ContractNum\",extract(epoch from \"ContractEndedBy\")::bigint,\"TimeZone\" FROM \"ultra_clients\";";
	WriteToDebugLog(0,0,false,"SQLQuery: %s",SQLQuery.c_str());
	pResult = PQexec(PostgresConnection, SQLQuery.c_str());
	tQueryStatus = PQresultStatus(pResult);
	if ((tQueryStatus == PGRES_TUPLES_OK)||(tQueryStatus == PGRES_COMMAND_OK))
	{
		WriteToDebugLog(0,0,false,"SQLQuery result Ok");
		if (PQntuples(pResult) > 0) 
		{
			for (int i = 0; i < PQntuples(pResult); i++) 
			{
				ClientStruct CurClient;
				CurClient.ClientId=  atoi(PQgetvalue(pResult, i, 0)); //int ClientId;
				CurClient.FullName=       PQgetvalue(pResult, i, 1); //std::string FullName;
				CurClient.ShortName=      PQgetvalue(pResult, i, 2);  //std::string ShortName;
				CurClient.INN=            PQgetvalue(pResult, i, 3); //std::string INN;
				CurClient.KPP=            PQgetvalue(pResult, i, 4); //std::string KPP;
				CurClient.LegalAddress=   PQgetvalue(pResult, i, 5); //std::string LegalAddress;
				CurClient.FactAddress=    PQgetvalue(pResult, i, 6); //std::string FactAddress;
				CurClient.LeaderName=     PQgetvalue(pResult, i, 7); //std::string LeaderName;
				CurClient.EMail=          PQgetvalue(pResult, i, 8); //std::string EMail;
				CurClient.Comment=        PQgetvalue(pResult, i, 9); //std::string Comment;
				CurClient.IsTest=   (atoi(PQgetvalue(pResult, i, 10))!=0); //bool IsTest;
				CurClient.IsService=(atoi(PQgetvalue(pResult, i, 11))!=0); //bool IsService;
				CurClient.IsDealer= (atoi(PQgetvalue(pResult, i, 12))!=0); //bool IsTest;
				CurClient.IsDisp=   (atoi(PQgetvalue(pResult, i, 13))!=0); //bool IsService;
				CurClient.Login=          PQgetvalue(pResult, i, 14); //std::string Login;
				CurClient.Password=       PQgetvalue(pResult, i, 15); //std::string Password;
				CurClient.CreditLimit=atof(PQgetvalue(pResult, i, 16));//float CreditLimit;
				CurClient.ContractDate=(time_t)atoi(PQgetvalue(pResult, i, 17));//time_t ContractDate;
				CurClient.ContractNum=atoi(PQgetvalue(pResult, i, 18));//int ContractNum;
				CurClient.ContractEndedBy=(time_t)atoi(PQgetvalue(pResult, i, 19));//time_t ContractEndedBy;
				CurClient.TimeZone=   atoi(PQgetvalue(pResult, i, 20));//int TimeZone;
				ClientDb[CurClient.ClientId]=CurClient;
			}
		}
	}
	else
		WriteToDebugLog(0,0,true,"SQLQuery result Error=%d, ErrorString=%s",PQresultStatus(pResult), PQerrorMessage(PostgresConnection));
	PQclear(pResult);

	std::map <int,ClientStruct>::iterator ClientDBIt;
	for(ClientDBIt=ClientDb.begin();ClientDBIt!=ClientDb.end();ClientDBIt++)
	{
		ClientDBIt->second.CityIdList.clear();
	}

	SQLQuery="SELECT \"dispatcher_id\", \"city_id\", \"id\" FROM \"ultra_clients_city\" ORDER BY \"dispatcher_id\",\"id\";";
	WriteToDebugLog(0,0,false,"SQLQuery: %s",SQLQuery.c_str());
	pResult = PQexec(PostgresConnection, SQLQuery.c_str());
	tQueryStatus = PQresultStatus(pResult);
	if ((tQueryStatus == PGRES_TUPLES_OK)||(tQueryStatus == PGRES_COMMAND_OK))
	{
		WriteToDebugLog(0,0,false,"SQLQuery result Ok");
		if (PQntuples(pResult) > 0) 
		{
			for (int i = 0; i < PQntuples(pResult); i++) 
			{
				int ClientId = atoi(PQgetvalue(pResult, i, 0)); //dispatcher_id
				int CityId = atoi(PQgetvalue(pResult, i, 1)); //city_id
				int Pos = atoi(PQgetvalue(pResult, i, 2)); //id
				std::map <int,ClientStruct>::iterator ClientDBIt=ClientDb.find(ClientId);
				if(ClientDBIt!=ClientDb.end())
				{
					ClientDBIt->second.CityIdList.push_back(CityId);
				}
			}
		}
	}
	else
		WriteToDebugLog(0,0,true,"SQLQuery result Error=%d, ErrorString=%s",PQresultStatus(pResult), PQerrorMessage(PostgresConnection));
	PQclear(pResult);
	return 0;
}


int GetDirectoryesFromDB()
{
	ReadClients();

	PGresult*		pResult;
	ExecStatusType tQueryStatus;
	std::string SQLQuery="SELECT \"id\",\"CarTypeName\" FROM \"ultra_car_type\";";
	WriteToDebugLog(0,0,false,"SQLQuery: %s",SQLQuery.c_str());
	pResult = PQexec(PostgresConnection, SQLQuery.c_str());
	tQueryStatus = PQresultStatus(pResult);
	if (tQueryStatus == PGRES_TUPLES_OK)	
	{
		WriteToDebugLog(0,0,false,"SQLQuery result Ok");
		if (PQntuples(pResult) > 0) 
		{
			for (int i = 0; i < PQntuples(pResult); i++) 
			{
				std::string s1 = PQgetvalue(pResult, i, 0); //key
				std::string s2 = PQgetvalue(pResult, i, 1); //value
				CarrierTypeDirectory[atoi(s1.c_str())]=s2;
			}
		}
	}
	else
		WriteToDebugLog(0,0,true,"SQLQuery result Error=%d, ErrorString=%s",PQresultStatus(pResult), PQerrorMessage(PostgresConnection));
	PQclear(pResult);

	SQLQuery="SELECT \"id\",\"StatusName\" FROM \"ultra_orders_status\";";
	WriteToDebugLog(0,0,false,"SQLQuery: %s",SQLQuery.c_str());
	pResult = PQexec(PostgresConnection, SQLQuery.c_str());
	tQueryStatus = PQresultStatus(pResult);
	if (tQueryStatus == PGRES_TUPLES_OK)	
	{
		WriteToDebugLog(0,0,false,"SQLQuery result Ok");
		if (PQntuples(pResult) > 0) 
		{
			for (int i = 0; i < PQntuples(pResult); i++) 
			{
				std::string s1 = PQgetvalue(pResult, i, 0); //key
				std::string s2 = PQgetvalue(pResult, i, 1); //value
				OrderStatusDirectory[atoi(s1.c_str())]=s2;
			}
		}
	}
	else
		WriteToDebugLog(0,0,true,"SQLQuery result Error=%d, ErrorString=%s",PQresultStatus(pResult), PQerrorMessage(PostgresConnection));
	PQclear(pResult);

	SQLQuery="SELECT \"ultra_city\".\"id\", \"CityName\", \"RegionName\", \"CountryName\" FROM \"ultra_city\", \"ultra_regions\", \"ultra_countries\" WHERE \"RegionId\"=\"ultra_regions\".\"id\" AND \"ultra_regions\".\"CountryId\"=\"ultra_countries\".\"id\";";
	WriteToDebugLog(0,0,false,"SQLQuery: %s",SQLQuery.c_str());
	pResult = PQexec(PostgresConnection, SQLQuery.c_str());
	tQueryStatus = PQresultStatus(pResult);
	if (tQueryStatus == PGRES_TUPLES_OK)	
	{
		WriteToDebugLog(0,0,false,"SQLQuery result Ok");
		if (PQntuples(pResult) > 0) 
		{
			for (int i = 0; i < PQntuples(pResult); i++) 
			{
				std::string s1 = PQgetvalue(pResult, i, 0); //key
				std::string s2 = PQgetvalue(pResult, i, 1); //city
				std::string s3 = PQgetvalue(pResult, i, 2); //region
				std::string s4 = PQgetvalue(pResult, i, 3); //country
				CityDirectory[atoi(s1.c_str())]=s2+" ("+s3+","+s4+")";
			}
		}
	}
	else
		WriteToDebugLog(0,0,true,"SQLQuery result Error=%d, ErrorString=%s",PQresultStatus(pResult), PQerrorMessage(PostgresConnection));
	PQclear(pResult);

	SQLQuery="SELECT \"ExpiredTime\" FROM \"ultra_param\";";
	WriteToDebugLog(0,0,false,"SQLQuery: %s",SQLQuery.c_str());
	pResult = PQexec(PostgresConnection, SQLQuery.c_str());
	tQueryStatus = PQresultStatus(pResult);
	if (tQueryStatus == PGRES_TUPLES_OK)	
	{
		WriteToDebugLog(0,0,false,"SQLQuery result Ok");
		if (PQntuples(pResult) > 0) 
		{
			ExpiredTime=atoi(PQgetvalue(pResult, 0, 0));
		}
	}
	else
		WriteToDebugLog(0,0,true,"SQLQuery result Error=%d, ErrorString=%s",PQresultStatus(pResult), PQerrorMessage(PostgresConnection));
	PQclear(pResult);

	SQLQuery="SELECT \"DBExpiredTime\" FROM \"ultra_param\";";
	WriteToDebugLog(0,0,false,"SQLQuery: %s",SQLQuery.c_str());
	pResult = PQexec(PostgresConnection, SQLQuery.c_str());
	tQueryStatus = PQresultStatus(pResult);
	if (tQueryStatus == PGRES_TUPLES_OK)	
	{
		WriteToDebugLog(0,0,false,"SQLQuery result Ok");
		if (PQntuples(pResult) > 0) 
		{
			DBExpiredTime=atoi(PQgetvalue(pResult, 0, 0));
		}
	}
	else
		WriteToDebugLog(0,0,true,"SQLQuery result Error=%d, ErrorString=%s",PQresultStatus(pResult), PQerrorMessage(PostgresConnection));
	PQclear(pResult);

	SQLQuery="SELECT \"id\", \"ClientId\", extract(epoch from \"BeginTime\")::bigint,extract(epoch from \"EndTime\")::bigint, \"PublisherPercent\", \"PublisherFixed\", \"PublisherReturnForeit\" FROM \"ultra_rates_disp\";";
	WriteToDebugLog(0,0,false,"SQLQuery: %s",SQLQuery.c_str());
	pResult = PQexec(PostgresConnection, SQLQuery.c_str());
	tQueryStatus = PQresultStatus(pResult);
	if (tQueryStatus == PGRES_TUPLES_OK)	
	{
		WriteToDebugLog(0,0,false,"SQLQuery result Ok");
		if (PQntuples(pResult) > 0) 
		{
			WriteToDebugLog(0,0,false,"SQLQuery result >0");
			for (int i = 0; i < PQntuples(pResult); i++) 
			{
				int Id = atoi(PQgetvalue(pResult, i, 0)); //id
				ultra_rates_disp CurRate;
				CurRate.ClientId=atoi(PQgetvalue(pResult, i, 1)); //ClientId
				CurRate.BeginTime=atoi(PQgetvalue(pResult, i, 2)); //BeginTime
				CurRate.EndTime=atoi(PQgetvalue(pResult, i, 3)); //EndTime
				CurRate.PublisherPercent=atof(PQgetvalue(pResult, i, 4)); //PublisherPercent
				CurRate.PublisherFixed=atof(PQgetvalue(pResult, i, 5)); //PublisherFixed
				CurRate.PublisherReturnForeit=atof(PQgetvalue(pResult, i, 6)); //PublisherReturnForeit
				DispRates[Id]=CurRate;
			}
		}
	}
	else
		WriteToDebugLog(0,0,true,"SQLQuery result Error=%d, ErrorString=%s",PQresultStatus(pResult), PQerrorMessage(PostgresConnection));
	PQclear(pResult);

	SQLQuery="SELECT \"id\", \"ClientId\", extract(epoch from \"BeginTime\")::bigint,extract(epoch from \"EndTime\")::bigint, \"ServicePercent\", \"ServiceFixed\", \"ServiceReturnForeit\" FROM \"ultra_rates_service\";";
	WriteToDebugLog(0,0,false,"SQLQuery: %s",SQLQuery.c_str());
	pResult = PQexec(PostgresConnection, SQLQuery.c_str());
	tQueryStatus = PQresultStatus(pResult);
	if (tQueryStatus == PGRES_TUPLES_OK)	
	{
		WriteToDebugLog(0,0,false,"SQLQuery result Ok");
		if (PQntuples(pResult) > 0) 
		{
			WriteToDebugLog(0,0,false,"SQLQuery result >0");
			for (int i = 0; i < PQntuples(pResult); i++) 
			{
				int Id = atoi(PQgetvalue(pResult, i, 0)); //id
				ultra_rates_service CurRate;
				CurRate.ClientId=atoi(PQgetvalue(pResult, i, 1)); //ClientId
				CurRate.BeginTime=atoi(PQgetvalue(pResult, i, 2)); //BeginTime
				CurRate.EndTime=atoi(PQgetvalue(pResult, i, 3)); //EndTime
				CurRate.ServicePercent=atof(PQgetvalue(pResult, i, 4)); //ServicePercent
				CurRate.ServiceFixed=atof(PQgetvalue(pResult, i, 5)); //ServiceFixed
				CurRate.ServiceReturnForeit=atof(PQgetvalue(pResult, i, 6)); //ServiceReturnForeit
				ServiceRates[Id]=CurRate;
			}
		}
	}
	else
		WriteToDebugLog(0,0,true,"SQLQuery result Error=%d, ErrorString=%s",PQresultStatus(pResult), PQerrorMessage(PostgresConnection));
	PQclear(pResult);

	SQLQuery="SELECT \"id\", \"CityId\", extract(epoch from \"BeginTime\")::bigint,extract(epoch from \"EndTime\")::bigint, \"ServicePercent\", \"ServiceFixed\", \"ServiceReturnForeit\", \"MinOrderPrice\" FROM \"ultra_rates_service_city\";";
	WriteToDebugLog(0,0,false,"SQLQuery: %s",SQLQuery.c_str());
	pResult = PQexec(PostgresConnection, SQLQuery.c_str());
	tQueryStatus = PQresultStatus(pResult);
	if (tQueryStatus == PGRES_TUPLES_OK)	
	{
		WriteToDebugLog(0,0,false,"SQLQuery result Ok");
		if (PQntuples(pResult) > 0) 
		{
			WriteToDebugLog(0,0,false,"SQLQuery result >0");
			for (int i = 0; i < PQntuples(pResult); i++) 
			{
				int Id = atoi(PQgetvalue(pResult, i, 0)); //id
				ultra_rates_service_city CurRate;
				CurRate.CityId=atoi(PQgetvalue(pResult, i, 1)); //CityId
				CurRate.BeginTime=atoi(PQgetvalue(pResult, i, 2)); //BeginTime
				CurRate.EndTime=atoi(PQgetvalue(pResult, i, 3)); //EndTime
				CurRate.ServicePercent=atof(PQgetvalue(pResult, i, 4)); //ServicePercent
				CurRate.ServiceFixed=atof(PQgetvalue(pResult, i, 5)); //ServiceFixed
				CurRate.ServiceReturnForeit=atof(PQgetvalue(pResult, i, 6)); //ServiceReturnForeit
				CurRate.MinOrderPrice=atof(PQgetvalue(pResult, i, 7)); //MinOrderPrice
				CityRates[Id]=CurRate;
			}
		}
	}
	else
		WriteToDebugLog(0,0,true,"SQLQuery result Error=%d, ErrorString=%s",PQresultStatus(pResult), PQerrorMessage(PostgresConnection));
	PQclear(pResult);
	return 0;
}

int GetFirstStatusId()
{
	static int StatusId=-1;
	if(StatusId>=0)
		return StatusId;

	for(int NumAttempt=0;NumAttempt<10;NumAttempt++)
	{
		PGresult*		pResult;
		ExecStatusType tQueryStatus;
		std::string SQLQuery="SELECT min(\"id\")  FROM \"ultra_orders_status\" WHERE \"StatusName\"=\'����� �����\';";
		WriteToDebugLog(0,0,false,"SQLQuery: %s",SQLQuery.c_str());
		pResult = PQexec(PostgresConnection, SQLQuery.c_str());
		tQueryStatus = PQresultStatus(pResult);
		if (tQueryStatus == PGRES_TUPLES_OK)	
		{
			WriteToDebugLog(0,0,false,"SQLQuery result Ok");
			if (PQntuples(pResult) > 0) 
			{
				for (int i = 0; i < PQntuples(pResult); i++) 
				{
					StatusId = atoi(PQgetvalue(pResult, 0, 0)); 
				}
			}
			PQclear(pResult);
			return StatusId;
		}
		else
		{
			ReconnectDB();
		}
	}
	return StatusId;
}

bool IsOrderSameUnikey(const OrderStruct &CurOrder)
{
	if(CurOrder.UniKey==0)
		return false;
	for(int NumAttempt=0;NumAttempt<10;NumAttempt++)
	{

		PGresult*		pResult;
		ExecStatusType tQueryStatus;
		std::string SQLQuery="SELECT \"id\" FROM \"ultra_orders\" WHERE \"Publisher\"=";
		SQLQuery+=ToString(CurOrder.Publisher);
		SQLQuery+=" AND \"UniKey\"=";
		SQLQuery+=ToString(CurOrder.UniKey);
		SQLQuery+=";";
		WriteToDebugLog(0,0,false,"SQLQuery: %s",SQLQuery.c_str());
		pResult = PQexec(PostgresConnection, SQLQuery.c_str());
		tQueryStatus = PQresultStatus(pResult);
		if ((tQueryStatus == PGRES_TUPLES_OK)||(tQueryStatus == PGRES_COMMAND_OK))
		{
			WriteToDebugLog(0,0,false,"SQLQuery result Ok");
			if (PQntuples(pResult) > 0) 
			{
				PQclear(pResult);
				return true;
			}
			PQclear(pResult);
			return false;
		}
		else
		{
			ReconnectDB();
		}
	}
	return false;
}

int GetNextOrderId()
{
	for(int NumAttempt=0;NumAttempt<10;NumAttempt++)
	{
		int OrderId=0;
		PGresult*		pResult;
		ExecStatusType tQueryStatus;
		std::string SQLQuery="SELECT max(\"id\")  FROM \"ultra_orders\";";
		WriteToDebugLog(0,0,false,"SQLQuery: %s",SQLQuery.c_str());
		pResult = PQexec(PostgresConnection, SQLQuery.c_str());
		tQueryStatus = PQresultStatus(pResult);
		if ((tQueryStatus == PGRES_TUPLES_OK)||(tQueryStatus == PGRES_COMMAND_OK))
		{
			WriteToDebugLog(0,0,false,"SQLQuery result Ok");
			if (PQntuples(pResult) > 0) 
			{
				for (int i = 0; i < PQntuples(pResult); i++) 
				{
					OrderId = atoi(PQgetvalue(pResult, 0, 0)); 
				}
			}
			PQclear(pResult);
			OrderId++;
			return OrderId;
		}
		else
		{
			ReconnectDB();
		}
	}
	return 0;
}

TariffStruct GetOrderTariff(int PublisherId,int CityId)
{
	TariffStruct RetVal;
	time_t CurTime=time(0);

	std::map <int,ultra_rates_service_city>::iterator It1;
	for(It1=CityRates.begin();It1!=CityRates.end();It1++)
	{
		if((It1->second.CityId==CityId)&&(CurTime>=It1->second.BeginTime))
		{
			if((It1->second.EndTime==0)||((It1->second.EndTime)&&(CurTime<=It1->second.EndTime)))
			{
				RetVal.TariffId+="\\";
				RetVal.TariffId+=ToString(It1->first);

				RetVal.ServicePercent=It1->second.ServicePercent;
				RetVal.ServiceFixed=It1->second.ServiceFixed;
				RetVal.ServiceReturnForeit=It1->second.ServiceReturnForeit;
				RetVal.MinOrderPrice=It1->second.MinOrderPrice;
			}
			break;
		}
	}

	std::map <int,ultra_rates_service>::iterator It2;
	for(It2=ServiceRates.begin();It2!=ServiceRates.end();It2++)
	{
		if((It2->second.ClientId==PublisherId)&&(CurTime>=It2->second.BeginTime))
		{
			if((It2->second.EndTime==0)||((It2->second.EndTime)&&(CurTime<=It2->second.EndTime)))
			{
				RetVal.TariffId+="\\";
				RetVal.TariffId+=ToString(It2->first);

				RetVal.ServicePercent=It2->second.ServicePercent;
				RetVal.ServiceFixed=It2->second.ServiceFixed;
				RetVal.ServiceReturnForeit=It2->second.ServiceReturnForeit;
				break;
			}
		}
	}

	std::map <int,ultra_rates_disp>::iterator It3;
	for(It3=DispRates.begin();It3!=DispRates.end();It3++)
	{
		if((It3->second.ClientId==PublisherId)&&(CurTime>=It3->second.BeginTime))
		{
			if((It3->second.EndTime==0)||((It3->second.EndTime)&&(CurTime<=It3->second.EndTime)))
			{
				RetVal.TariffId+="\\";
				RetVal.TariffId+=ToString(It3->first);

				RetVal.PublisherPercent=It3->second.PublisherPercent;
				RetVal.PublisherFixed=It3->second.PublisherFixed;
				RetVal.PublisherReturnForeit=It3->second.PublisherReturnForeit;
				break;
			}
		}
	}

	RetVal.TariffId+="\\";

	return RetVal; 
}

int InsertOrderDB(const OrderStruct &CurOrder)
{
	WriteToDebugLog(0,0,false,"InsertOrder to db");
	if(IsOrderSameUnikey(CurOrder))
	{
		WriteToDebugLog(0,0,true,"InsertOrder whth same UniKey");
		return -1;	
	}
	for(int NumAttempt=0;NumAttempt<10;NumAttempt++)
	{
		PGresult*		pResult;
		ExecStatusType tQueryStatus;
		char QueryBuffer[14096];
		//������ � ������� �������
		std::string SQLQuery="INSERT INTO \"ultra_orders\" (\"id\",\"CityIdFrom\",\"CityIdTo\",\"StreetFrom\",\"AdditionalFrom\",\"LatitudeFrom\",\"LongitudeFrom\",\"StreetTo\",\"AdditionalTo\",\"LatitudeTo\",\"LongitudeTo\",\"CarrierType\",\"Price\",\"UniKey\",\"OrderState\",\"StatusId\",\"ClientPhone\",\"WriteTime\",\"Publisher\",\"Acceptor\",\"ServiceComission\",\"PublisherComission\",\"PreliminaryTime\",\"Time0Km1Fixed2\",\"AcceptTime\",\"Comment\",\"TimeToLiveSec\",\"ToCarMin\",\"SetCarInfoTime\") VALUES ";
		sprintf(QueryBuffer,"(%d,%d,%d,'%s','%s',%2.10f,%2.10f,'%s','%s',%2.10f,%2.10f,%d,%10.2f,%d,%d,%d,'%s','%s',%d,%d,%10.2f,%10.2f,'%s',%d,'%s','%s',%d,%d,'%s');",
			CurOrder.OrderId,CurOrder.CityIdFrom,CurOrder.CityIdTo,CurOrder.StreetFrom.c_str(),CurOrder.AdditionalFrom.c_str(),CurOrder.LatitudeFrom,CurOrder.LongitudeFrom,CurOrder.StreetTo.c_str(),CurOrder.AdditionalTo.c_str(),CurOrder.LatitudeTo,CurOrder.LongitudeTo,
			CurOrder.CarrierType,CurOrder.Price,CurOrder.UniKey,CurOrder.OrderState,CurOrder.StatusId,CurOrder.ClientPhone.c_str(),
			ctime1(CurOrder.WriteTime).c_str(),CurOrder.Publisher,CurOrder.Acceptor,CurOrder.ServiceComission,
			CurOrder.PublisherComission,ctime1(CurOrder.PreliminaryTime).c_str(),CurOrder.Time0Km1Fixed2,ctime1(CurOrder.AcceptTime).c_str(),CurOrder.Comment.c_str(),CurOrder.TimeToLiveSec,CurOrder.ToCarMin,"1970-01-01 00:00:00");

		SQLQuery+=QueryBuffer;
		WriteToDebugLog(0,0,false,"SQLQuery: %s",SQLQuery.c_str());
		pResult = PQexec(PostgresConnection, SQLQuery.c_str());
		if ((PQresultStatus(pResult)!=PGRES_COMMAND_OK) && (PQresultStatus(pResult)!=PGRES_TUPLES_OK))
		{  
			WriteToDebugLog(0,0,true,"SQLQuery result Error=%d, ErrorString=%s",PQresultStatus(pResult), PQerrorMessage(PostgresConnection));
			ReconnectDB();
		}
		else
		{
			WriteToDebugLog(0,0,false,"SQLQuery result Ok");
			PQclear(pResult);
			RereadClientRelations();

			int res1=WriteToOrderLog(CurOrder.OrderId,CurOrder.WriteTime,CurOrder.StatusId,CurOrder.OrderState,CurOrder.Publisher,CurOrder.ServiceComission,CurOrder.PublisherComission);
			if(res1<0)
				return res1;
			return 1;
		}
	}
	return -1;
}


int AcceptOrderDB(const OrderStruct &CurOrder)
{
	WriteToDebugLog(0,0,false,"AcceptOrderDB to db");
	
	for(int NumAttempt=0;NumAttempt<10;NumAttempt++)
	{
		PGresult*		pResult;
		ExecStatusType tQueryStatus;
		char QueryBuffer[14096];

		std::string SQLQuery="UPDATE \"ultra_orders\" SET \"OrderState\"=\'";
		SQLQuery+=ToString(CurOrder.OrderState);
		SQLQuery+="\', \"AcceptTime\"=\'";
		SQLQuery+=ctime1(CurOrder.AcceptTime);
		SQLQuery+="\', \"Acceptor\"=\'";
		SQLQuery+=ToString(CurOrder.Acceptor);
		SQLQuery+="\', \"ServiceComission\"=\'";
		SQLQuery+=ToString(CurOrder.ServiceComission);
		SQLQuery+="\', \"PublisherComission\"=\'";
		SQLQuery+=ToString(CurOrder.PublisherComission);
		SQLQuery+="\' WHERE \"id\"=\'";
		SQLQuery+=ToString(CurOrder.OrderId);
		SQLQuery+="\';";

		WriteToDebugLog(0,0,false,"SQLQuery: %s",SQLQuery.c_str());
		pResult = PQexec(PostgresConnection, SQLQuery.c_str());
		if ((PQresultStatus(pResult)!=PGRES_COMMAND_OK) && (PQresultStatus(pResult)!=PGRES_TUPLES_OK))
		{  
			WriteToDebugLog(0,0,true,"SQLQuery result Error=%d, ErrorString=%s",PQresultStatus(pResult), PQerrorMessage(PostgresConnection));
			ReconnectDB();
		}
		else
		{
			WriteToDebugLog(0,0,false,"SQLQuery result Ok");
			PQclear(pResult);

			int res1=WriteToOrderLog(CurOrder.OrderId,CurOrder.AcceptTime,CurOrder.StatusId,CurOrder.OrderState,CurOrder.Acceptor,CurOrder.ServiceComission,CurOrder.PublisherComission);
			if(res1<0)
				return res1;
			return 1;
		}
	}
	return -1;
}

int ReturnMyOrderDB(int OrderId, int ClientId, OrderStruct *RetOrderStruct)
{
	std::string AddStr=" WHERE \"id\"=\'";
	AddStr+=ToString(OrderId);
	AddStr+="\';";

	std::list <OrderStruct> OrderList=GetOrderList(AddStr);
	if(OrderList.size()!=1)
		return -1;
	OrderStruct CurOrder=*OrderList.begin();

	time_t CurTime=time(0);
	*RetOrderStruct=CurOrder;
	bool IsRetNewOrder=false;

	if(CurOrder.Publisher!=ClientId) //
		return -3;
	if(CurOrder.StatusId==100) //
		return -4;

	if(CurOrder.OrderState==OrderStateNew) //
	{
		IsRetNewOrder=true;
	}
	else
		return -5;

	WriteToDebugLog(0,0,false,"Order: %d returned by publisher",CurOrder.OrderId);

	std::map <int,std::string>::const_iterator StatusIt;
	for(StatusIt=OrderStatusDirectory.begin();StatusIt!=OrderStatusDirectory.end();StatusIt++)
	{
		if(StatusIt->second==ReturnOrderStatusName)
		{
			CurOrder.StatusId=StatusIt->first;
			break;
		}
	}

	int CurServceComission=CurOrder.ServiceComission;
	int CurPublisherComission=CurOrder.PublisherComission;

	CurOrder.AcceptTime=CurTime;
	CurOrder.Acceptor=ClientId;
	CurOrder.OrderState=OrderStateReturned;
	CurOrder.ServiceComission=0;
	CurOrder.PublisherComission=0;
	*RetOrderStruct=CurOrder;
	PGresult*		pResult;

	

	std::string SQLQuery="UPDATE \"ultra_orders\" SET \"OrderState\"=\'";
	SQLQuery+=ToString(CurOrder.OrderState);
	SQLQuery+="\', \"AcceptTime\"=\'";
	SQLQuery+=ctime1(CurOrder.AcceptTime);
	SQLQuery+="\', \"Acceptor\"=\'";
	SQLQuery+=ToString(CurOrder.Acceptor);
	SQLQuery+="\', \"StatusId\"=\'";
	SQLQuery+=ToString(CurOrder.StatusId);
	SQLQuery+="\', \"ServiceComission\"=\'";
	SQLQuery+=ToString(CurOrder.ServiceComission);
	SQLQuery+="\', \"PublisherComission\"=\'";
	SQLQuery+=ToString(CurOrder.PublisherComission);
	SQLQuery+="\' WHERE \"id\"=\'";
	SQLQuery+=ToString(CurOrder.OrderId);
	SQLQuery+="\';";

	//
	WriteToDebugLog(0,0,false,"SQLQuery: %s",SQLQuery.c_str());
	pResult = PQexec(PostgresConnection, SQLQuery.c_str());
	if ((PQresultStatus(pResult)!=PGRES_COMMAND_OK) && (PQresultStatus(pResult)!=PGRES_TUPLES_OK))
	{  
		WriteToDebugLog(0,0,true,"SQLQuery result Error=%d, ErrorString=%s",PQresultStatus(pResult), PQerrorMessage(PostgresConnection));
		ReconnectDB();
	}
	else
	{
		WriteToDebugLog(0,0,false,"SQLQuery result Ok");
		PQclear(pResult);

		int res1=WriteToOrderLog(CurOrder.OrderId,CurOrder.AcceptTime,CurOrder.StatusId,CurOrder.OrderState,CurOrder.Acceptor,-CurServceComission,-CurPublisherComission);
		if(res1<0)
			return -6;
		if(IsRetNewOrder)
			return 2;
		return 1;
	}
	return -6;
}

int RefuseMyOrderDB(int OrderId, int ClientId, OrderStruct *RetOrderStruct)
{
	std::string AddStr=" WHERE \"id\"=\'";
	AddStr+=ToString(OrderId);
	AddStr+="\';";

	std::list <OrderStruct> OrderList=GetOrderList(AddStr);
	if(OrderList.size()!=1)
		return -1;
	OrderStruct CurOrder=*OrderList.begin();

	time_t CurTime=time(0);
	*RetOrderStruct=CurOrder;
	bool IsRetNewOrder=false;

	if(CurOrder.Publisher!=ClientId) //
		return -3;
	if(CurOrder.StatusId==100) //
		return -4;

	if(CurOrder.OrderState==OrderStateNew) //
	{
		IsRetNewOrder=true;
	}

	WriteToDebugLog(0,0,false,"Order: %d. Customer refused",CurOrder.OrderId);

	std::map <int,std::string>::const_iterator StatusIt;
	for(StatusIt=OrderStatusDirectory.begin();StatusIt!=OrderStatusDirectory.end();StatusIt++)
	{
		if(StatusIt->second==RefuseOrderStatusName)
		{
			CurOrder.StatusId=StatusIt->first;
			break;
		}
	}

	int CurServceComission=CurOrder.ServiceComission;
	int CurPublisherComission=CurOrder.PublisherComission;

	int Acceptor=CurOrder.Acceptor;
	CurOrder.OrderState=OrderStateReturned;
	CurOrder.ServiceComission=0;
	CurOrder.PublisherComission=0;
	*RetOrderStruct=CurOrder;
	PGresult*		pResult;

	std::string SQLQuery="UPDATE \"ultra_orders\" SET \"OrderState\"=\'";
	SQLQuery+=ToString(CurOrder.OrderState);
	SQLQuery+="\', \"AcceptTime\"=\'";
	SQLQuery+=ctime1(CurOrder.AcceptTime);
	SQLQuery+="\', \"Acceptor\"=\'";
	SQLQuery+=ToString(CurOrder.Acceptor);
	SQLQuery+="\', \"StatusId\"=\'";
	SQLQuery+=ToString(CurOrder.StatusId);
	SQLQuery+="\', \"ServiceComission\"=\'";
	SQLQuery+=ToString(CurOrder.ServiceComission);
	SQLQuery+="\', \"PublisherComission\"=\'";
	SQLQuery+=ToString(CurOrder.PublisherComission);
	SQLQuery+="\' WHERE \"id\"=\'";
	SQLQuery+=ToString(CurOrder.OrderId);
	SQLQuery+="\';";

	//
	WriteToDebugLog(0,0,false,"SQLQuery: %s",SQLQuery.c_str());
	pResult = PQexec(PostgresConnection, SQLQuery.c_str());
	if ((PQresultStatus(pResult)!=PGRES_COMMAND_OK) && (PQresultStatus(pResult)!=PGRES_TUPLES_OK))
	{  
		WriteToDebugLog(0,0,true,"SQLQuery result Error=%d, ErrorString=%s",PQresultStatus(pResult), PQerrorMessage(PostgresConnection));
		ReconnectDB();
	}
	else
	{
		WriteToDebugLog(0,0,false,"SQLQuery result Ok");
		PQclear(pResult);

		int res1=WriteToOrderLog(CurOrder.OrderId,CurOrder.AcceptTime,CurOrder.StatusId,CurOrder.OrderState,CurOrder.Acceptor,-CurServceComission,-CurPublisherComission);

		if(res1<0)
			return -6;
		if(IsRetNewOrder)
			return 2;
		return 1;
	}
	return -6;
}

int ReturnForeignOrderDB(int OrderId,int ClientId, OrderStruct *RetOrderStruct)
{
	std::string AddStr=" WHERE \"id\"=\'";
	AddStr+=ToString(OrderId);
	AddStr+="\';";

	std::list <OrderStruct> OrderList=GetOrderList(AddStr);
	if(OrderList.size()!=1)
		return -1;
	OrderStruct CurOrder=*OrderList.begin();
	time_t CurTime=time(0);
	*RetOrderStruct=CurOrder;

	if(CurOrder.Acceptor<0) //
		return -2;
	if(CurOrder.Acceptor!=ClientId) //
		return -3;
	if(CurOrder.Publisher==ClientId) //
		return -3;
	if(CurOrder.StatusId==100) //
		return -4;
	if((CurOrder.OrderState!=OrderStateAccept)&&(CurOrder.OrderState!=OrderStateAcceptWe)) //����� �� ��������� � ������� "������"
		return -5;


	WriteToDebugLog(0,0,false,"Force return order to publisher");

	
	TariffStruct CurTariff=GetOrderTariff(CurOrder.Publisher,CurOrder.CityIdFrom);
	WriteToDebugLog(0,0,false,"CurTariff.ServiceReturnForeit: %d",CurTariff.ServiceReturnForeit);
	WriteToDebugLog(0,0,false,"CurTariff.PublisherReturnForeit: %d",CurTariff.PublisherReturnForeit);

	//res2=WriteToOperationLog(WrTime,-CurTariff.ServiceReturnForeit-CurTariff.PublisherReturnForeit,"������������ �����",WrTime,"",0,ClientId,CurOrder.OrderId,"�����");
	//res3=WriteToOperationLog(WrTime,CurTariff.PublisherReturnForeit,"������������ �����",WrTime,"",0,CurOrder.Publisher,CurOrder.OrderId,"��������� ������ � ������ �����������");

	CurOrder.Acceptor=0;
	CurOrder.AcceptTime=0;
	CurOrder.ServiceComission=0;
	CurOrder.PublisherComission=0;
	CurOrder.OrderState=OrderStateNew;
	*RetOrderStruct=CurOrder;
	PGresult*		pResult;

	std::map <int,std::string>::const_iterator StatusIt;
	for(StatusIt=OrderStatusDirectory.begin();StatusIt!=OrderStatusDirectory.end();StatusIt++)
	{
		if(StatusIt->second=="������� ������")
		{
			CurOrder.StatusId=StatusIt->first;
			break;
		}
	}

	std::string SQLQuery="UPDATE \"ultra_orders\" SET \"OrderState\"=\'";
	SQLQuery+=ToString(CurOrder.OrderState);
	SQLQuery+="\', \"AcceptTime\"=\'";
	SQLQuery+=ctime1(CurOrder.AcceptTime);
	SQLQuery+="\', \"Acceptor\"=\'";
	SQLQuery+=ToString(CurOrder.Acceptor);
	SQLQuery+="\', \"StatusId\"=\'";
	SQLQuery+=ToString(CurOrder.StatusId);
	SQLQuery+="\', \"ServiceComission\"=\'";
	SQLQuery+=ToString(CurOrder.ServiceComission);
	SQLQuery+="\', \"PublisherComission\"=\'";
	SQLQuery+=ToString(CurOrder.PublisherComission);
	SQLQuery+="\' WHERE \"id\"=\'";
	SQLQuery+=ToString(CurOrder.OrderId);
	SQLQuery+="\';";

	WriteToDebugLog(0,0,false,"SQLQuery: %s",SQLQuery.c_str());
	pResult = PQexec(PostgresConnection, SQLQuery.c_str());
	if ((PQresultStatus(pResult)!=PGRES_COMMAND_OK) && (PQresultStatus(pResult)!=PGRES_TUPLES_OK))
	{  
		WriteToDebugLog(0,0,true,"SQLQuery result Error=%d, ErrorString=%s",PQresultStatus(pResult), PQerrorMessage(PostgresConnection));
		ReconnectDB();
	}
	else
	{
		WriteToDebugLog(0,0,false,"SQLQuery result Ok");
		PQclear(pResult);

		int res1=WriteToOrderLog(CurOrder.OrderId,CurOrder.AcceptTime,CurOrder.StatusId,CurOrder.OrderState,CurOrder.Acceptor,CurTariff.ServiceReturnForeit,CurTariff.PublisherReturnForeit);
		if(res1<0)
			return -6;
		return 1;
	}
	return -6;
}



int WriteToOrderLog(int OrderId,time_t WriteTime,int StatusId,int OrderState,int ClientId,double ServiceComission,double PublisherComission)
{
	PGresult*		pResult;
	ExecStatusType tQueryStatus;
	char QueryBuffer[4096];
	std::string SQLQuery="INSERT INTO \"ultra_orders_log\" (\"OrderId\",\"DateUpdate\",\"StatusId\",\"ClientId\",\"OrderState\",\"ServiceComission\",\"PublisherComission\") VALUES ";
	sprintf(QueryBuffer,"(%d,'%s',%d,%d,%d,%10.2f,%10.2f);",
		OrderId,ctime1(WriteTime).c_str(),StatusId,ClientId,OrderState,ServiceComission,PublisherComission);
	SQLQuery+=QueryBuffer;
	WriteToDebugLog(0,0,false,"SQLQuery: %s",SQLQuery.c_str());
	pResult = PQexec(PostgresConnection, SQLQuery.c_str());
	if ((PQresultStatus(pResult)!=PGRES_COMMAND_OK) && (PQresultStatus(pResult)!=PGRES_TUPLES_OK))
	{  
		WriteToDebugLog(0,0,true,"SQLQuery result Error=%d, ErrorString=%s",PQresultStatus(pResult), PQerrorMessage(PostgresConnection));
		return -1;
	}
	WriteToDebugLog(0,0,false,"SQLQuery result Ok");
	PQclear(pResult);
	return 1;
}

std::list <OrderStruct> GetOrderList(const std::string &AdditionStr)
{
	std::list <OrderStruct> RetVal;
	for(int NumAttempt=0;NumAttempt<10;NumAttempt++)
	{
		PGresult*		pResult;
		ExecStatusType tQueryStatus;
		std::string SQLQuery="SELECT \"id\",\"CityIdFrom\",\"CityIdTo\",\"StreetFrom\",\"AdditionalFrom\",\"LatitudeFrom\",\"LongitudeFrom\",\"StreetTo\",\"AdditionalTo\",\"LatitudeTo\",\"LongitudeTo\",\"CarrierType\",\"Price\",\"UniKey\",\"StatusId\",\"OrderState\",\"ClientPhone\",\"PhoneId\",extract(epoch from \"WriteTime\")::bigint,\"Publisher\",\"Acceptor\",\"ServiceComission\",\"PublisherComission\",extract(epoch from \"PreliminaryTime\")::bigint,\"Time0Km1Fixed2\",extract(epoch from \"AcceptTime\")::bigint,\"Comment\",\"CarDescription\",\"CarGovNumber\",\"CarCallNumber\",\"TimeToLiveSec\",\"ToCarMin\",extract(epoch from \"SetCarInfoTime\")::bigint FROM \"ultra_orders\" ";
		SQLQuery+=AdditionStr;
		SQLQuery+=";";
		WriteToDebugLog(0,0,false,"SQLQuery: %s",SQLQuery.c_str());
		pResult = PQexec(PostgresConnection, SQLQuery.c_str());
		tQueryStatus = PQresultStatus(pResult);
		WriteToDebugLog(0,0,false,"SQLQuery result -- %d",tQueryStatus);
		if ((tQueryStatus == PGRES_TUPLES_OK)||(tQueryStatus == PGRES_COMMAND_OK))
		{
			WriteToDebugLog(0,0,false,"SQLQuery result Ok");
			if (PQntuples(pResult) > 0) 
			{
				for (int i = 0; i < PQntuples(pResult); i++) 
				{
					OrderStruct CurStruct;
					CurStruct.OrderId=atoi(PQgetvalue(pResult, i, 0));
					CurStruct.CityIdFrom=atoi(PQgetvalue(pResult, i, 1));
					CurStruct.CityIdTo=atoi(PQgetvalue(pResult, i, 2));
					CurStruct.StreetFrom=PQgetvalue(pResult, i, 3);
					CurStruct.AdditionalFrom=PQgetvalue(pResult, i, 4);
					CurStruct.LatitudeFrom=atof(PQgetvalue(pResult, i, 5));
					CurStruct.LongitudeFrom=atof(PQgetvalue(pResult, i, 6));
					CurStruct.StreetTo=PQgetvalue(pResult, i, 7);
					CurStruct.AdditionalTo=PQgetvalue(pResult, i, 8);
					CurStruct.LatitudeTo=atof(PQgetvalue(pResult, i, 9));
					CurStruct.LongitudeTo=atof(PQgetvalue(pResult, i, 10));
					CurStruct.CarrierType=atoi(PQgetvalue(pResult, i, 11));
					CurStruct.Price=atof(PQgetvalue(pResult, i, 12));
					CurStruct.UniKey=atoi(PQgetvalue(pResult, i, 13));
					CurStruct.StatusId=atoi(PQgetvalue(pResult, i, 14));
					CurStruct.OrderState=atoi(PQgetvalue(pResult, i, 15));
					CurStruct.ClientPhone=PQgetvalue(pResult, i, 16);
					CurStruct.PhoneId=atoi(PQgetvalue(pResult, i, 17));
					CurStruct.WriteTime=(time_t)atoi(PQgetvalue(pResult, i, 18));
					CurStruct.Publisher=atoi(PQgetvalue(pResult, i, 19));
					CurStruct.Acceptor=atoi(PQgetvalue(pResult, i, 20));
					CurStruct.ServiceComission=atof(PQgetvalue(pResult, i, 21));
					CurStruct.PublisherComission=atof(PQgetvalue(pResult, i, 22));
					CurStruct.PreliminaryTime=(time_t)atoi(PQgetvalue(pResult, i, 23));
					CurStruct.Time0Km1Fixed2=atoi(PQgetvalue(pResult, i, 24));
					CurStruct.AcceptTime=(time_t)atoi(PQgetvalue(pResult, i, 25));
					CurStruct.Comment=PQgetvalue(pResult, i, 26);
					CurStruct.CarDescription=PQgetvalue(pResult, i, 27);
					CurStruct.CarGovNumber=PQgetvalue(pResult, i, 28);
					CurStruct.CarCallNumber=atoi(PQgetvalue(pResult, i, 29));
					CurStruct.TimeToLiveSec=atoi(PQgetvalue(pResult, i, 30));
					CurStruct.ToCarMin=atoi(PQgetvalue(pResult, i, 31));
					CurStruct.SetCarInfoTime=(time_t)atoi(PQgetvalue(pResult, i, 32));
					RetVal.push_back(CurStruct);
				}
			}
			PQclear(pResult);
			return RetVal;
		}
		else
		{
			ReconnectDB();
		}
	}
	return RetVal;
}

int WriteOrderParamsDB(int OrderId,float Price,int StatusId,int OrderState,OrderStruct *RetOrderStruct)
{
	PGresult*		pResult;
	ExecStatusType tQueryStatus;
	char QueryBuffer[4096];

	if(OrderId<=0)
		return -1;

	if((Price<0)&&(StatusId<0)&&(OrderState==OrderStateNotPublished))
		return -2;

	std::string SQLQuery="UPDATE \"ultra_orders\" SET ";

	if(Price>=0)
	{
		SQLQuery+="\"Price\"=\'";
		SQLQuery+=ToString(Price);
		SQLQuery+="\'";
	}
	if(StatusId>=0)
	{
		if(Price>=0)
			SQLQuery+=",";

		SQLQuery+="\"StatusId\"=\'";
		SQLQuery+=ToString(StatusId);
		SQLQuery+="\'";
	}
	if(OrderState!=OrderStateNotPublished)
	{
		if((StatusId>=0)||(Price>=0))
			SQLQuery+=",";

		SQLQuery+="\"OrderState\"=\'";
		SQLQuery+=ToString(OrderState);
		SQLQuery+="\'";
	}

	SQLQuery+=" WHERE \"id\"=\'";
	SQLQuery+=ToString(OrderId);
	SQLQuery+="\';";

	WriteToDebugLog(0,0,false,"SQLQuery: %s",SQLQuery.c_str());
	pResult = PQexec(PostgresConnection, SQLQuery.c_str());
	if ((PQresultStatus(pResult)!=PGRES_COMMAND_OK) && (PQresultStatus(pResult)!=PGRES_TUPLES_OK))
	{  
		WriteToDebugLog(0,0,true,"SQLQuery result Error=%d, ErrorString=%s",PQresultStatus(pResult), PQerrorMessage(PostgresConnection));
		PQclear(pResult);
		return -3;
	}
	else
	{
		WriteToDebugLog(0,0,false,"SQLQuery result Ok");
		PQclear(pResult);
		WriteToOrderLog(OrderId,time(0),StatusId,OrderState,-1,0,0);

		std::string AddStr=" WHERE \"id\"=\'";
		AddStr+=ToString(OrderId);
		AddStr+="\';";

		std::list <OrderStruct> OrderList=GetOrderList(AddStr);
		if(OrderList.size()!=1)
		{
			return -1;
		}
		OrderStruct CurOrder=*OrderList.begin();
		*RetOrderStruct=CurOrder;
		if((CurOrder.StatusId==100) && (StatusId!=100)) //����� ������
			return -4;

		if(CurOrder.OrderState==OrderStateNew) //����� �����
			return 2;

		return 0;
	}
	return -1;
}

int CalculateStatement(int ClientId)
{
	int Statement=0;
	/*
	std::string AdditionStr="WHERE \"ClientId\"=\'";
	AdditionStr+=ToString(ClientId);
	AdditionStr+="\'";

	std::list <TransactionStruct> Transaction = GetTransactionList(AdditionStr);
	std::list <TransactionStruct>::iterator It;
	for(It=Transaction.begin();It!=Transaction.end();It++)
	{
	Statement+=It->Price;
	}
	*/
	return Statement;
}

int RereadClientRelations()
{
	int RetVal=-1;

	static time_t LastTime=0;
	time_t CurTime=time(0);
	if(LastTime+60>CurTime)
		return 0;

	LastTime=CurTime;

	PGresult*		pResult;
	ExecStatusType tQueryStatus;
	std::string SQLQuery="SELECT \"From\",\"To\",\"NotSend\",\"NotRecv\",\"Priority\" FROM \"ultra_disp_relation\";";
	WriteToDebugLog(0,0,false,"SQLQuery: %s",SQLQuery.c_str());
	pResult = PQexec(PostgresConnection, SQLQuery.c_str());
	tQueryStatus = PQresultStatus(pResult);
	if ((tQueryStatus == PGRES_TUPLES_OK)||(tQueryStatus == PGRES_COMMAND_OK))
	{
		WriteToDebugLog(0,0,false,"SQLQuery result Ok");

		std::map <int,ClientStruct>::iterator ItFrom,ItTo;
		for(ItFrom=ClientDb.begin();ItFrom!=ClientDb.end();ItFrom++)
		{
			ItFrom->second.ClientRelations.clear();  
		}

		if (PQntuples(pResult) > 0) 
		{
			RetVal=0;
			for (int i = 0; i < PQntuples(pResult); i++) 
			{
				int From=atoi(PQgetvalue(pResult, i, 0));
				int To=atoi(PQgetvalue(pResult, i, 1));
				RelationStruct R;
				R.IsNotSend=atoi(PQgetvalue(pResult, i, 2));
				R.IsNotRecv=atoi(PQgetvalue(pResult, i, 3));
				R.PriorityDelay=atoi(PQgetvalue(pResult, i,4));

				std::map <int,ClientStruct>::iterator ItFrom,ItTo;
				ItFrom=ClientDb.find(From);
				ItTo=ClientDb.find(To);
				if((ItFrom!=ClientDb.end())&&(ItTo!=ClientDb.end()))
				{
					ItFrom->second.ClientRelations[To]=R;
				}
			}
		}
	}
	PQclear(pResult);
	return RetVal;
}


int WriteConnectonState()
{
	PGresult*		pResult;
	ExecStatusType tQueryStatus;
	char QueryBuffer[4096];

	std::string SQLQuery="    DELETE FROM \"ultra_connected\"; ";
	pResult = PQexec(PostgresConnection, SQLQuery.c_str());
	tQueryStatus = PQresultStatus(pResult);
	if (tQueryStatus != PGRES_COMMAND_OK)
	{
		WriteToDebugLog(0,0,true,"    SQLQuery '%s' result Error=%d, ErrorString=%s",SQLQuery.c_str(), PQresultStatus(pResult), PQerrorMessage(PostgresConnection));
		PQclear(pResult);
		return -1;
	}
	std::map<int,ClientContext>::iterator ClientsIt;
	for(ClientsIt=clients.begin();ClientsIt!=clients.end();ClientsIt++)
	{
		std::string ShortName="<not found>";
		std::map <int,ClientStruct>::iterator ClientDbIt=ClientDb.find(ClientsIt->second.ClientId);
		if(ClientDbIt!=ClientDb.end())
			ShortName=ClientDbIt->second.ShortName;
		std::string Ip="";
		for(int i=3;i>=0;i--)
		{
			unsigned char Buf=((unsigned char *)(&ClientsIt->second.FromAddr))[i];
			char StrBuf[10];
			sprintf(StrBuf,"%hu",(unsigned short)Buf);
			Ip+=StrBuf;
			if(i!=0)
				Ip+=".";
		}
		std::string SQLQuery="    INSERT INTO \"ultra_connected\" (\"ClientId\",\"ShortName\",\"ConnectTime\",\"SocketId\", \"FromAddr\") VALUES ";
		sprintf(QueryBuffer,"('%d','%s','%s','%d','%s');",
			ClientsIt->second.ClientId,ShortName.c_str(),ctime1(ClientsIt->second.ConnectTime).c_str(),ClientsIt->first,Ip.c_str());
		SQLQuery+=QueryBuffer;
		pResult = PQexec(PostgresConnection, SQLQuery.c_str());
		if ((PQresultStatus(pResult)!=PGRES_COMMAND_OK) && (PQresultStatus(pResult)!=PGRES_TUPLES_OK))
		{  
			WriteToDebugLog(0,0,true,"    SQLQuery '%s' result Error=%d, ErrorString=%s",SQLQuery.c_str(), PQresultStatus(pResult), PQerrorMessage(PostgresConnection));
			return -1;
		}
		PQclear(pResult);
	}
	return 0;
}

int AddCarDescriptionDB(int OrderId,int ClientId,const std::string &CarDescription,const std::string &CarGovNumber,int CarCallNumber,OrderStruct *RetOrderStruct,time_t SetCarInfoTime, int ToCarMin)
{
	PGresult*		pResult;

	std::string AddStr="WHERE \"id\"=\'";
	AddStr+=ToString(OrderId);
	AddStr+="\'";
	std::list <OrderStruct> OrderList=GetOrderList(AddStr);
	if(OrderList.size()!=1)
		return -1;
	OrderStruct CurOrder=*OrderList.begin();
	time_t CurTime=time(0);
	*RetOrderStruct=CurOrder;

	if(CurOrder.Acceptor<0) 
		return -1;
	if(CurOrder.Acceptor!=ClientId) 
		return -2;
	if(CurOrder.StatusId==100) 
		return -3;
	if((CurOrder.OrderState!=OrderStateAccept)&&(CurOrder.OrderState!=OrderStateAcceptWe)) 
		return -4;

	CurOrder.CarDescription=CarDescription;
	CurOrder.CarGovNumber=CarGovNumber;
	CurOrder.CarCallNumber=CarCallNumber;
	CurOrder.ToCarMin=ToCarMin;
	CurOrder.SetCarInfoTime=SetCarInfoTime;

	*RetOrderStruct=CurOrder;

	std::string SQLQuery="UPDATE \"ultra_orders\" SET \"CarDescription\"=\'";
	SQLQuery+=CarDescription;
	SQLQuery+="\', \"CarGovNumber\"=\'";
	SQLQuery+=CarGovNumber;
	SQLQuery+="\', \"CarCallNumber\"=\'";
	SQLQuery+=ToString(CarCallNumber);
	SQLQuery+="\', \"SetCarInfoTime\"=\'";
	SQLQuery+=ctime1(SetCarInfoTime);
	SQLQuery+="\', \"ToCarMin\"=\'";
	SQLQuery+=ToString(ToCarMin);
	SQLQuery+="\' WHERE \"id\"=\'";
	SQLQuery+=ToString(CurOrder.OrderId);
	SQLQuery+="\';";

	WriteToDebugLog(0,0,false,"SQLQuery: %s",SQLQuery.c_str());
	pResult = PQexec(PostgresConnection, SQLQuery.c_str());
	if ((PQresultStatus(pResult)!=PGRES_COMMAND_OK) && (PQresultStatus(pResult)!=PGRES_TUPLES_OK))
	{  
		WriteToDebugLog(0,0,true,"SQLQuery result Error=%d, ErrorString=%s",PQresultStatus(pResult), PQerrorMessage(PostgresConnection));
		ReconnectDB();
	}
	else
	{
		WriteToDebugLog(0,0,false,"SQLQuery result Ok");
		PQclear(pResult);

		int res1=WriteToOrderLog(OrderId,time(0),CurOrder.StatusId,CurOrder.OrderState,ClientId,0,0);

		if(res1<0)
			return res1;
	}
	return 0;
}

int RelationsCheck(int Publisher, int ProbablyAcceptor) 
{
	std::map <int,ClientStruct>::iterator ClientDbItPub=ClientDb.find(Publisher);
	std::map <int,ClientStruct>::iterator ClientDbItAcc=ClientDb.find(ProbablyAcceptor);
	if(ClientDbItPub==ClientDb.end())
		return -1;
	if(ClientDbItAcc==ClientDb.end())
		return -2;
	std::map<int /*ClientdId*/,RelationStruct>::iterator ClientRelationsIt;
	
	ClientRelationsIt=ClientDbItAcc->second.ClientRelations.find(Publisher);
	if(ClientRelationsIt!=ClientDbItAcc->second.ClientRelations.end())
	{
		if(ClientRelationsIt->second.IsNotRecv)
			return -3;
	}
	
	ClientRelationsIt=ClientDbItPub->second.ClientRelations.find(ProbablyAcceptor);
	if(ClientRelationsIt!=ClientDbItPub->second.ClientRelations.end())
	{
		if(ClientRelationsIt->second.IsNotSend)
			return -3;
		return ClientRelationsIt->second.PriorityDelay;
	}
	
	return 0;
}

//��� ������� ����������� ��������� � ����������� ����, ��� ������� ���������� � �� �������� ������ �� ��� �������, >90%
//������� ������� ���������� � �� ����� ������, ��� � ������ ������
void CheckOldOrdersDB() 
{
	PGresult*		pResult;

	for(int NumAttempt=0;NumAttempt<50;NumAttempt++)
	{
		if(isPostgresConnected)
		{
			time_t CurTime=time(NULL);
			CurTime-=DBExpiredTime;
			std::string CompareTimeStr=ctime1(CurTime);


			std::string SQLQuery="UPDATE \"ultra_orders\" SET \"StatusId\"='100' WHERE ((\"WriteTime\" < '";
			SQLQuery+=CompareTimeStr;
			SQLQuery+="') AND (\"StatusId\" <> '100') AND (((\"PreliminaryTime\" = '1970-01-01 00:00:00') AND (\"AcceptTime\" <> '1970-01-01 00:00:00') AND (\"AcceptTime\" < '";
			SQLQuery+=CompareTimeStr;
			SQLQuery+="')) OR ((\"PreliminaryTime\" = '1970-01-01 00:00:00') AND (\"AcceptTime\" = '1970-01-01 00:00:00')) OR ((\"PreliminaryTime\" <> '1970-01-01 00:00:00') AND (\"PreliminaryTime\" < '";
			SQLQuery+=CompareTimeStr;
			SQLQuery+="'))));";

			WriteToDebugLog(0,0,false,"SQLQuery: %s",SQLQuery.c_str());
			pResult = PQexec(PostgresConnection, SQLQuery.c_str());
			if ((PQresultStatus(pResult)!=PGRES_COMMAND_OK) && (PQresultStatus(pResult)!=PGRES_TUPLES_OK))
			{  
				PQclear(pResult);
				WriteToDebugLog(0,0,true,"SQLQuery result Error=%d, ErrorString=%s",PQresultStatus(pResult), PQerrorMessage(PostgresConnection));
				ReconnectDB();
			}
			else
			{
				PQclear(pResult);
				WriteToDebugLog(0,0,false,"SQLQuery result Ok");
				return;
			}
		}
		else
		{
#ifdef _WIN32			
			Sleep(5000);
#endif
			ReconnectDB();
		}
	}
	return;
}

