#ifndef _DIRECTORY_H
#define _DIRECTORY_H

#include <map>
#include <list>
#include <string>
#ifdef _WIN32
#include "..\n_lib\postgresql\libpq-fe.h"
#else
#include <libpq-fe.h>
#endif
struct SentOrderInfo
{
    int OrderId;
    bool IsNew; //New/Changed
    time_t WriteTime;
};

//������� ���������� �� ����� ���������� � ��������
struct ClientContext
{
    std::string AutentifString;
    std::string CurrentString;
    int ClientId;
    time_t ConnectTime;
    unsigned long FromAddr;
    bool IsAuthent; //0-���� �� ���������������� 1-����������������
    //� ������ �� ������ ����� ������� ����� ���� ��������� �������������� ���������� � ��� �������, 
    //� ������� ������ ������ ����� (�.�. ����� ������,������, ������� ��������� ���� ������, ������, �������� ��������, ������, ����������� ��������)
    std::string AdditionalInfo; 
    //��� �������� ������� ��������� ������ �������, ��������� �������
	std::list <SentOrderInfo> SentOrders; //������ ���������� ������� �������
    
    int NotifyMode; //-1 - �� ������ ������ 0-��� ������ 1-������� ������ 2-�������� ����� NotifyBeginTime � NotifyEndTime
    time_t NotifyBeginTime;
    time_t NotifyEndTime;
};
extern std::map<int,ClientContext> clients;

//������ ������������ ��������
enum OTaxiUltraClientStatus
{
	OTUStatus_PartialContent=3,
	OTUStatus_NoData=2,
	OTUStatus_TimeOutNotify=1,
	OTUStatus_Ok=0,
	OTUStatus_CommonError=-1,
	OTUStatus_SocketError=-2,
	OTUStatus_ConnectError=-3,
	OTUStatus_RecvError=-4,
	OTUStatus_RecvNo=-5,
	OTUStatus_SendError=-6,
	OTUStatus_AuthentError=-7,
	OTUStatus_ParseError=-8,
	OTUStatus_UnknownResponse=-9,
	OTUStatus_InsertOrderError=-10,
	OTUStatus_SetOrderStatusError=-11,
	OTUStatus_IOCtlSocketError=-12,
};

enum OrderStates
{
	OrderStateNew=0,
    OrderStateAcceptWe=1,
	OrderStateReturned=2,
	OrderStateAccept=3,
	OrderStateNotPublished=4
};

enum OTaxiUltraBlockReasons
{
	OTaxiUltraBlockReason_Unknown=0,
	OTaxiUltraBlockReason_Autent=1,
	OTaxiUltraBlockReason_Directory=2,
	OTaxiUltraBlockReason_NotifyNew=3,
	OTaxiUltraBlockReason_NotifyNewDel=4,
	OTaxiUltraBlockReason_NotifyChange=5,
    OTaxiUltraBlockReason_NotifyChangeDel=6,
	OTaxiUltraBlockReason_NumStatus=7,
	OTaxiUltraBlockReason_Statement=8,
	OTaxiUltraBlockReason_Tariff=9,
	OTaxiUltraBlockReason_OrderFullInfo=10,
	OTaxiUltraBlockReason_OrderPartialInfo=11,
	OTaxiUltraBlockReason_TextMessage=12,
	OTaxiUltraBlockReason_Other=100
};

struct RelationStruct
{
    bool IsNotSend; //�� ���������� ����� ������� ���� ������
    bool IsNotRecv; //�� ��������� �� ����� ������� ��� ������
    int PriorityDelay; //�������� ������ ������� ��������������� ������� (� ��������)
};

struct ClientStruct
{
    int ClientId;
    std::string FullName;
    std::string ShortName;
    std::string INN;
    std::string KPP;
    std::string LegalAddress;
    std::string FactAddress;
    std::string LeaderName;
    std::string EMail;
    std::string Comment;
 
    bool IsTest;
    bool IsService;
    bool IsDealer;
    bool IsDisp;

	std::string Login;
    std::string Password;
    float CreditLimit;
    time_t ContractDate;
    int ContractNum;
    time_t ContractEndedBy;
    int TimeZone;
	std::list<int> CityIdList;
	std::map<int /*ClientdId*/,RelationStruct> ClientRelations;
};

//��������� ��� �������
//��� ��� �������� � ��
struct ultra_rates_disp
{
    //int Id;
    int ClientId;
    time_t BeginTime;
    time_t EndTime;
    float PublisherPercent;
    float PublisherFixed;
    float PublisherReturnForeit;
};
struct ultra_rates_service
{
    //int Id;
    int ClientId;
    time_t BeginTime;
    time_t EndTime;
    float ServicePercent;
    float ServiceFixed;
    float ServiceReturnForeit;
};
struct ultra_rates_service_city
{
    //int Id;
    int CityId;
    time_t BeginTime;
    time_t EndTime;
    float ServicePercent;
    float ServiceFixed;
    float ServiceReturnForeit;
    float MinOrderPrice;
};

//��� ��� ������������ ��� ������� ����� ���������
struct TariffStruct
{
	TariffStruct():TariffId(""),ServicePercent(0),ServiceFixed(0),PublisherPercent(0),PublisherFixed(0),ServiceReturnForeit(0),PublisherReturnForeit(0),MinOrderPrice(0){}
    std::string TariffId;
    float ServicePercent;
    float ServiceFixed;
    float PublisherPercent;
    float PublisherFixed;
	float ServiceReturnForeit;
	float PublisherReturnForeit;
    float MinOrderPrice;
};

struct OrderStruct
{
    //���� ������
    int OrderId; //����������� ��������
	std::string StreetFrom;
	std::string AdditionalFrom;
	float LatitudeFrom;
	float LongitudeFrom;
	std::string StreetTo;
	std::string AdditionalTo;
	float LatitudeTo;
	float LongitudeTo;
	int CarrierType;
	float Price;
	int CityIdFrom;
	int CityIdTo;
    int UniKey;
    int StatusId; //����������� ��������
    int OrderState; 
    //0-����� �����, 1,3 - �������� ����� 
    //(�������� ���� � ����������� � ����, ��� ����������� ��������� 1-��� ���������, 3-��� ���� ���������) 
    //2-����� ������� ����������� 
    //4-���������������� ������ (�� ������� ��� �������� ������� �� ��������)
    std::string ClientPhone;
	int PhoneId;
    time_t PreliminaryTime;
    time_t WriteTime; //����������� ��������
    int Publisher; //����������� ��������
    int Acceptor; //����������� ��������
    float ServiceComission; //����������� ��������
    float PublisherComission; //����������� ��������
    int Time0Km1Fixed2;
    time_t AcceptTime; //����������� ��������
    std::string Comment;
	std::string CarDescription; //���������� � ��������, ������� ������� �� ������
	std::string CarGovNumber;
	int CarCallNumber;
	int TimeToLiveSec;
	int ToCarMin;	
	time_t SetCarInfoTime;

};
/*
struct TransactionStruct
{
    time_t OpTime;
    int Price;
    std::string DocNum;
    time_t DocDate;
    std::string TillId;
    int OperationId;
    int ClientId;
    int OrderId;
    std::string Comment;
    bool IsDeleted;
};
*/
extern int ExpiredTime;
extern int DBExpiredTime;

extern std::map <int,std::string> CarrierTypeDirectory;
extern std::map <int,std::string> OrderStatusDirectory;
extern std::map <int,std::string> CityDirectory;
extern std::map <int,ClientStruct> ClientDb;

extern std::map <int,ultra_rates_disp> DispRates;
extern std::map <int,ultra_rates_service> ServiceRates;
extern std::map <int,ultra_rates_service_city> CityRates;



std::string Convert2XML(std::string Src);
std::string ListToString(const std::list <int> &a);
std::string ToString(int a);
std::string ToString(float a);

bool ConnectDB(const std::string &Addr,const std::string & Port,const std::string &Login,const std::string &Password,const std::string &DBName);
int ReadClients();
bool ReconnectDB();
void DisconnectDB();
int GetDirectoryesFromDB();
int GetFirstStatusId();
int GetNextOrderId();

int InsertOrderDB(const OrderStruct &CurOrder);
int AcceptOrderDB(const OrderStruct &CurOrder);
int ReturnMyOrderDB(int OrderId,int ClientId, OrderStruct *RetOrderStruct);
int RefuseMyOrderDB(int OrderId, int ClientId, OrderStruct *RetOrderStruct);
int ReturnForeignOrderDB(int OrderId,int ClientId, OrderStruct *RetOrderStruct);
int WriteOrderParamsDB(int OrderId,float Price,int StatusId,int OrderState,OrderStruct *RetOrderStruct);

bool GetOrderSameUnikey(const OrderStruct &CurOrder);
int WriteToOrderLog(int OrderId,time_t WriteTime,int StatusId,int OrderState,int ClientId,double ServiceComission,double PublisherComission);
std::list <OrderStruct> GetOrderList(const std::string &AdditionStr);

int AddCarDescriptionDB(int OrderId,int ClientId,const std::string &CarDescription,const std::string &CarGovNumber,int CarCallNumber,OrderStruct *RetOrderStruct, time_t SetCarInfoTime, int ToCarMin);
int CalculateStatement(int ClientId);
int RereadClientRelations();
int WriteConnectonState();
//std::list <TransactionStruct> GetTransactionList(const std::string &AdditionStr);
TariffStruct GetOrderTariff(int PublisherId,int CityId);
void CheckOldOrdersDB(); //��������� �������� ������ ������� � ��

int RelationsCheck(int Publisher, int ProbablyAcceptor); //�������� ��������� ����� ������������ ������ � ������������� ����������

/*
const char ConvertWin2151[]="�����Ũ����������������������������������������������������������";
const char ConvertKoi8r[]  ="�������������������������������������ţ��������������������������";
std::string ConvertKoiToWin(const std::string &Src);
std::string ConvertWinToKoi(const std::string &Src);
*/

//������������ �������
struct TCommonConfig
{
    TCommonConfig():DBAddr("127.0.0.1"),DBPort("5432"),DBLogin("Login"),DBPassword("Password"),
		DBName("o_taxi_ultra"),ServerPort(2222),DebugLogFileName("/OTaxiUltraDebugLog"),DebugLogMaxSize(10000),
		KeepAlive(true),Socket_RCVBUF(0),Socket_SNDBUF(0),SocketReadTimeout(60){};

	bool KeepAlive;		//��������� ����� SO_KEEPALIVE ��� ���������� ������
	int Socket_RCVBUF;		//������ ������ �� �����
	int Socket_SNDBUF;	//������ ������ �� ��������
	int SocketReadTimeout;	//
    int ServerPort; //��������� ����

    std::string DBAddr; //��������� ���������� � ��
    std::string DBPort;
    std::string DBLogin;
    std::string DBPassword;
	std::string DBName;
    std::string DebugLogFileName; //��� � ������ ����������� �������
    int DebugLogMaxSize;
};

extern TCommonConfig CommonConfig;

#endif //_DIRECTORY_H
