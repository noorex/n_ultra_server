//=============================================================================
//  General component library for WIN32
//  Copyright (C) 2014, NooRex ( http://www.noorex.com/ )
//  The author of this program may be contacted at support@noorex.com
//=============================================================================
#include "n_UltraServer.h"
#include <commdlg.h>
#include "resource.h"
#include "Directory.h"

const char ThisModuleName[]="otaxi_ultraserver";
char     gszN_SMSClass[] = "n_UltraServer_WndClass" ;
char     gszAppName[] = "NooRex O-TAXI Ultra Server\0" ;
const char *ThisFileName="n_UltraServer";				//��� ������� ����� ��� ����������
char strServiceName[] = "n_UltraServer";				//������������ ������ �� ���������
char strServiceDisplayName[] = "O-TAXI: Ultra server";	//��������� � ������

//���������� ������������ ����
HWND     hTTYWnd ;
RECT rcPaint;
double scale;
int ImHeight;
int ImWidth;
HBITMAP hBmp;
BITMAP   bmp;
HINSTANCE hInstance;
HACCEL   ghAccel ;

extern CLogFile g_Log;	//��� ���������
extern int	GetActiveConnectionsCount();	//���-�� ������������������� �����������
extern bool isSQLServerConnected();

std::string Ver;		//������ �������
char IniFileNameFull[1024];	//  
int AdvancedLog=1;	//����������� ��� (1)
bool NETEXCEPT=false;		//������������ ���������� ��� �������� �������� ������
bool isService=false;		//�������� ��� ������
int			TCPStatPort=0;		//���� ��� ����������
std::string	Locale;				//������
time_t	ProgramStartTime;		//����� ������� ���������

TCommonConfig CommonConfig;

HANDLE hThread;			//�������� ����� ������ ������
HANDLE shutdownEvent;	//������� ��������� ��

HANDLE GetShutdownEvent()	//��� ��������� ������� ���������� ������
{
	return shutdownEvent;
}
void SaveToLog(std::string Log,int _AdvancedLog)	//������ � ���
{
	if(AdvancedLog>=_AdvancedLog)
		g_Log<<CURRDATETIME<<" "<<Log+CRLF;	
}

void SendMessageRepaint()	//��������� ��������� � ����
{
		if(hTTYWnd!=NULL)
		{
			SendMessage(hTTYWnd,WM_PAINT,0,0);
		}
}
int WINAPI WinMain(HINSTANCE hInstance,HINSTANCE hPrevInstance,
				   LPSTR lpCmdLine,int nCmdShow)
{
	MSG   msg ;
	LPSTR *szArglist;
	int nArgs;
	BOOL bRet;
	::hInstance=hInstance;

	char FileName[MAX_PATH];
	FileName[0]='\0';
	GetModuleFileName(0,FileName,MAX_PATH);
	char *tt=strrchr(FileName,'\\');
	if(tt) *tt='\0';
	SetCurrentDirectory(FileName);
	time(&ProgramStartTime);

	//������ ������� 1.0.0.1 - ������ 4 �����
	char         szBuffer[128];
	char OwnModuleName[512];
	WORD MajorVersion,MinorVersion,BuildNumber,RevisionNumber;
	GetModuleFileName(NULL,OwnModuleName,sizeof(OwnModuleName));
	if (GetAppVersion(OwnModuleName,&MajorVersion,&MinorVersion,&BuildNumber,&RevisionNumber))
	{
		wsprintf( szBuffer, "%d.%d.%d.%d", MajorVersion, MinorVersion,BuildNumber,RevisionNumber) ;
		Ver=szBuffer;
	}else {
		Ver="X.X.X.X";
	}

	//�������� �������� ��
	LoadIni();
	//��������� ������� �������������� ����������
	if(NETEXCEPT==false)	//n_JServer_object.isService==false&&
	{
		bRet = SetCrashHandlerFilter ( &TheCrashHandlerFunction ) ;
	}

	szArglist = CommandLineToArgvA(GetCommandLineA(), &nArgs);
	//���� ��������� �������� ��� ������
	if(StartAsServices(nArgs,szArglist)==-1)
	{
		if (!hPrevInstance)
			if (!InitApplicationWindow( hInstance ))
				return ( FALSE ) ;

		if (NULL == (hTTYWnd = InitInstance( hInstance, nCmdShow )))
			return ( FALSE ) ;

		//����������������� ��� �����
		if(!InitApplication()) 
		{
			SaveToLog("PROGRAM: Initialization error class/WinMain",1);
			DestroyApplication();
			LocalFree(szArglist);
			return false;
		}
		//������ � ���
		if(isService==false&&NETEXCEPT==false)
		{
			if ( TRUE == bRet )
			{
				SaveToLog("PROGRAM: SetCrashHandlerFilter!",0);
			}
			else
			{
				SaveToLog(" PROGRAM: Unable to set crash filter!",0);
			}
		}
		while (GetMessage( &msg, NULL, 0, 0 ))
		{
			if (!TranslateAccelerator( hTTYWnd, ghAccel, &msg ))
			{
				TranslateMessage( &msg ) ;
				DispatchMessage( &msg ) ;
			}
		}
		SaveToLog("PROGRAM: exit /WinMain",1);
		DestroyApplication();
		LocalFree(szArglist);
		return ( (int) msg.wParam ) ;
	}
	else
	{
		g_Log.Close();
		// Free memory allocated for CommandLineToArgvW arguments.
		LocalFree(szArglist);
		return false ;
	}

}

void	DestroyApplication()
{
	SaveToLog(" PROGRAM: set shutdown event/Destroy application",1);
	SetEvent(GetShutdownEvent());

	//pNooRexStat_object.StopServerSocket();

	WaitForSingleObject(hThread,2000);
	int MainControlQueueExitCode=0;
	if(TerminateThread(hThread,MainControlQueueExitCode))	WaitForSingleObject(hThread,2000);

	//��������� �������
	//����� ��� SQL
//	DestroySQLSystem();
	//CoUninitialize();
	SaveToLog("PROGRAM: END",0);
	SaveToLog("==================================================================",0);
	g_Log.Close();
}
//������ ������� � ������������� ����������
bool	InitApplication()
{
	char LogFileNameFull[1024];	//  
	char  buffer[4096]=""; 
	wsprintf(LogFileNameFull,"%s%s",buffer,LogFileName);
	g_Log.Open(LogFileNameFull);
	SaveToLog("==================================================================",0);

	//������ ������, ����� ��� ����� � ������� ������ ���� � "������"
	//setlocale(LC_ALL, 0);
	//������������� ������
	char *locale=setlocale(LC_ALL, Locale.c_str());	//"Russian"
	if(locale==NULL)
		SaveToLog("PROGRAM: set locale LC_ALL failed "+Locale,0);
		else
		SaveToLog(std::string("PROGRAM: set locale LC_ALL success ")+locale,0);
	char *localen=setlocale(LC_NUMERIC, "C");
	if(localen==NULL)
		SaveToLog("PROGRAM: set locale LC_NUMERIC failed",0);
		else
		SaveToLog(std::string("PROGRAM: set locale LC_NUMERIC success ")+localen,0);
	SaveToLog(std::string("PROGRAM: test atof=")+FloatnToStringGPS(atof("33.1111")), 0);
	//������� ���������� ������
    shutdownEvent=CreateEvent(NULL,true, false,false);
	hThread=(HANDLE)_beginthread(MainServerThread,1000000,0);
	if (hThread==NULL)
	{
		SaveToLog("PROGRAM: cann't create main thread",0);
		return false;
	}

	SaveToLog("PROGRAM: starting... Version "+Ver,0);
//	if(TCPStatPort!=0){
//		if(!pNooRexStat_object.StartServerSocket(TCPStatPort,KeepAlive,Socket_RCVBUF,Socket_SNDBUF)) return false;
//	}
	//������������� ����������     
//	if(!InitSQLSystem(SaveQueueCount,SQLReconnectTime)) return false;
	return true;
}

BOOL NEAR InitApplicationWindow( HINSTANCE hInstance )
{
	WNDCLASS  wndclass ;

	// register tty window class

	wndclass.style =         0 ;
	wndclass.lpfnWndProc =   MainWndProc ;
	wndclass.cbClsExtra =    0 ;
	wndclass.cbWndExtra =    n_EXTRABYTES ;
	wndclass.hInstance =     hInstance ;
	wndclass.hIcon =         LoadIcon( hInstance, MAKEINTRESOURCE( IDI_Icon_main ) );
	wndclass.hCursor =       LoadCursor( NULL, IDC_ARROW ) ;
	wndclass.hbrBackground = (HBRUSH) (COLOR_WINDOW + 1) ;
	wndclass.lpszMenuName =  MAKEINTRESOURCE( IDR_MENU1 ) ;
	wndclass.lpszClassName = gszN_SMSClass ;

	return( RegisterClass( &wndclass ) ) ;

} // end of InitApplication()

//---------------------------------------------------------------------------
HWND NEAR InitInstance( HINSTANCE hInstance, int nCmdShow )
{
	HWND  hTTYWnd ;
	HMENU       hMenu ;

	// load accelerators
	//   ghAccel = LoadAccelerators( hInstance, MAKEINTRESOURCE( TTYACCEL ) ) ;

	hTTYWnd = CreateWindow( gszN_SMSClass, gszAppName,
		WS_OVERLAPPED|WS_DLGFRAME|WS_CAPTION|WS_SYSMENU|WS_MINIMIZEBOX ,
		CW_USEDEFAULT, CW_USEDEFAULT,
		411, 300+GetSystemMetrics(SM_CYCAPTION)+GetSystemMetrics(SM_CYMENU),
		NULL, NULL, hInstance, NULL ) ;

	if (NULL == hTTYWnd)
		return ( NULL ) ;
	hBmp = LoadBitmap(hInstance, MAKEINTRESOURCE(IDB_BITMAP3)); 
	if(hBmp) {			
		GetObjectA(hBmp,sizeof(BITMAP),(LPSTR) &bmp);
		rcPaint.bottom=bmp.bmHeight;
		rcPaint.right=bmp.bmWidth;

		HDC hDc=GetDC(hTTYWnd);
		SetBkMode(hDc,TRANSPARENT); //������������� ���������� ���
		HDC hMemDc = CreateCompatibleDC(hDc); //������� �������� � ������, ����������� � ���������� ���������� - � ������ ������ ���� ����������
		SelectObject(hMemDc,hBmp); //����� �������� ����������� � �������� ������ 
		//BitBlt(hDc,0,0,240,240,hMemDc,0,0,SRCCOPY); //��������� �����������
		scale=1;
		ImHeight=bmp.bmHeight;
		ImWidth=bmp.bmWidth;
		StretchBlt(hDc,			// �������� ��� ���������
			0,0,				// x,y-���������� �������� ������ ���� ������� ���������
			bmp.bmWidth,		// ����� ������ �����������
			bmp.bmHeight,		// ����� ������ �����������
			hMemDc,				// ������������� ��������� ���������
			0,0,				// x,y-���������� �������� ������ ���� �������� �������
			bmp.bmWidth,		// ������ ��������� �����������
			bmp.bmHeight,		// ������ ��������� �����������
			SRCCOPY);			// ��� ��������� ��������

		//������� � ��������� ���������			
		DeleteDC(hMemDc);
		DeleteDC(hDc);
		//DeleteObject(hBmp);
	}
	//���. ����
	hMenu = GetMenu( hTTYWnd ) ;
	//EnableMenuItem( hMenu, IDM_DISCONNECT, MF_GRAYED |MF_DISABLED| MF_BYCOMMAND ) ;
	//EnableMenuItem( hMenu, IDM_CONNECT,  MF_ENABLED | MF_BYCOMMAND ) ;

	//��������� ����	
	char lpBuffer[512];
	wsprintf(lpBuffer,"%s %s",gszAppName,Ver.c_str());
	if(hTTYWnd!=NULL) SetWindowText(hTTYWnd,lpBuffer);

	ShowWindow( hTTYWnd, nCmdShow ) ;
	UpdateWindow( hTTYWnd ) ;

	return ( hTTYWnd ) ;

} // end of InitInstance()

LRESULT FAR PASCAL MainWndProc( HWND hWnd, UINT uMsg,
							   WPARAM wParam, LPARAM lParam) 
{
	double SecondsDiff;
	COLORREF myColor;
//	HMENU       hMenu ;
	HDC hdc,hMemDC,hDC;;
	PAINTSTRUCT      ps ;
	hdc=GetDC(hWnd);
	SetBkColor(hdc,TRANSPARENT);

	DWORD dwBlockSize=512;
	char szBuffer[128];

	switch (uMsg)
	{

	case WM_CREATE:
		return 1 ;
	case WM_COMMAND:
		{
			switch ( LOWORD( wParam ) )
			{
/*			case ID_MENU_ABOUT:
				GoModalDialogBoxParam ( GETHINST( hWnd ),
					MAKEINTRESOURCE( IDD_DIALOG_ABOUT ),
					hWnd,
					(DLGPROC) AboutDlgProc, 0L ) ;
				break;
*/
			case ID_MENU_EXIT:
				//SetEvent(shutdownEvent);
				//SaveToLog(" PROGRAM: ��������� ������� ������ �� ���������/ID_MENU_EXIT",1);
				PostMessage( hWnd, WM_CLOSE, 0, 0L ) ;
				break ;
			case ID_MENU_PROP:
			{
				if(DialogBox(hInstance, MAKEINTRESOURCE(IDD_PROP_DIALOG), hWnd, PropDlgProc)==IDOK)
				{
					//WaitForSingleObject(hThread,20000);
					//EndDialog(hwndDlg,IDCANCEL);
				}
				break;
			}
			}
		}
		break ;

		/*
		case WM_SIZE:
		SizeTTY( hWnd, HIWORD( lParam ), LOWORD( lParam ) ) ;
		break ;

		case WM_HSCROLL:
		ScrollTTYHorz( hWnd, LOWORD( wParam ), HIWORD( wParam ) ) ;
		break ;

		case WM_VSCROLL:
		ScrollTTYVert( hWnd, LOWORD( wParam ), HIWORD( wParam ) ) ;
		break ;

		case WM_CHAR:
		ProcessTTYCharacter( hWnd, LOBYTE( LOWORD( wParam ) ) ) ;
		break ;

		case WM_SETFOCUS:
		SetTTYFocus( hWnd ) ;
		break ;

		case WM_KILLFOCUS:
		KillTTYFocus( hWnd ) ;
		break ;
		*/
	case WM_PAINT:
		hDC = BeginPaint (hWnd, &ps);
		//GetObject(hBmp,sizeof(BITMAP),(LPSTR)&bmp);
		SetBkColor(hDC,TRANSPARENT);
		hMemDC=CreateCompatibleDC(hDC);
		SelectObject(hMemDC,hBmp);
		//������ �������������� ���������
		SelectObject(hMemDC,GetStockObject(DC_BRUSH));
		myColor=RGB(255,0,0);
		if(isSQLServerConnected()) myColor=RGB(0,255,0); 

		SetDCBrushColor(hMemDC,myColor);
		RoundRect(hMemDC, 300, 60, 400, 80,5,5);
		SetBkColor(hMemDC, RGB(255,255,255));
		SetTextColor(hMemDC, myColor);
		TextOut(hMemDC, 220, 60, "SQL Server\0", 10);
		//������� ������� ���������� ����������� � �������
		wsprintf(szBuffer,"Connections:%d",GetActiveConnectionsCount());
		TextOut(hMemDC, 220, 100,szBuffer, strlen(szBuffer));
		//������� ����� �� ������ ������� ���������
		time_t CurTime;
		time(&CurTime);
		SecondsDiff=difftime(CurTime,ProgramStartTime);
		wsprintf(szBuffer,"%d:%d",(int)SecondsDiff/(60*60),(int)(SecondsDiff/(60)-(SecondsDiff/(60*60))*60));
		TextOut(hMemDC, 220, 20,szBuffer, strlen(szBuffer));
		StretchBlt(	hdc,
			0,0,
			(int)ImWidth*scale,
			(int)ImHeight*scale,
			hMemDC,
			0,0,
			ImWidth,
			ImHeight,
			SRCCOPY
			);
		//int i=GetLastError();
		DeleteDC(hMemDC);
		EndPaint (hWnd, &ps) ;
		break ;
	case WM_DESTROY:
		//         DestroyTTYInfo( hWnd ) ;
		PostQuitMessage( 0 ) ;
		break ;

	case WM_CLOSE:
		if (IDOK != MessageBox( hWnd, "Close the program?",gszAppName,
			MB_ICONQUESTION | MB_OKCANCEL ))
		{
			if(shutdownEvent!=NULL) 
			{			
				SetEvent(shutdownEvent);
				SaveToLog("PROGRAM: set close event/WM_CLOSE",1);
			}
			break ;
		}

		// fall through

	default:
		return( DefWindowProc( hWnd, uMsg, wParam, lParam ) ) ;
	}
	return 0L ;

}

bool LoadIni()
{
	char lpBuffer[512];
	DWORD nBufferLength=sizeof(lpBuffer);
	DWORD retBufferLength;
	retBufferLength=GetCurrentDirectory(nBufferLength,lpBuffer);
	wsprintf(IniFileNameFull,"%s\\%s",lpBuffer,IniFileName);
	if(FileExists(IniFileNameFull))
	{
		char tmp[1024];
		int nsize;
/*
		nsize=GetPrivateProfileString("ADOServer","ADOProvider","SQLOLEDB.1",tmp,sizeof(tmp),IniFileNameFull);
		ADOProvider=std::string(tmp);
		ADOCommandTimeout=GetPrivateProfileInt("ADOServer","ADOCommandTimeout",0,IniFileNameFull);
		ADOConnectTimeout=GetPrivateProfileInt("ADOServer","ADOConnectTimeout",30,IniFileNameFull);
		ADOCommandRepeateCount=GetPrivateProfileInt("ADOServer","ADOCommandRepeateCount",2,IniFileNameFull);
		SaveQueueCount=GetPrivateProfileInt("ADOServer","SaveQueueCount",2,IniFileNameFull);
		SQLReconnectTime=GetPrivateProfileInt("ADOServer","SQLReconnectTime",0,IniFileNameFull);

		TCPStatPort=GetPrivateProfileInt("MAIN","TCPStatPort",0,IniFileNameFull);
		nsize=GetPrivateProfileString("MAIN","TCPStatPassword","o-taxi",tmp,sizeof(tmp),IniFileNameFull);
		pNooRexStat_object.TCPStatPassword=std::string(tmp);		
*/
		nsize=GetPrivateProfileString("MAIN","DBLogin","",tmp,sizeof(tmp),IniFileNameFull);
		CommonConfig.DBLogin=std::string(tmp);
		nsize=GetPrivateProfileString("MAIN","DBPassword","",tmp,sizeof(tmp),IniFileNameFull);
		CommonConfig.DBPassword=std::string(tmp);
		nsize=GetPrivateProfileString("MAIN","DBAddr","127.0.0.1",tmp,sizeof(tmp),IniFileNameFull);
		CommonConfig.DBAddr=std::string(tmp);
		nsize=GetPrivateProfileString("MAIN","DBName","o-taxi",tmp,sizeof(tmp),IniFileNameFull);
		CommonConfig.DBName=std::string(tmp);

		nsize=GetPrivateProfileString("MAIN","DBPort","5432",tmp,sizeof(tmp),IniFileNameFull);
		CommonConfig.DBPort=std::string(tmp);

		AdvancedLog=GetPrivateProfileInt("MAIN","AdvancedLog",1,IniFileNameFull);
		CommonConfig.ServerPort=GetPrivateProfileInt("MAIN","ServerPort",4980,IniFileNameFull);
		CommonConfig.SocketReadTimeout=GetPrivateProfileInt("MAIN","SocketReadTimeout",60,IniFileNameFull);

		if(GetPrivateProfileInt("MAIN","NETEXCEPT",0,IniFileNameFull)==0)
			NETEXCEPT=false; else NETEXCEPT=true;
		CommonConfig.Socket_RCVBUF=GetPrivateProfileInt("MAIN","SO_RCVBUF",0,IniFileNameFull);
		CommonConfig.Socket_SNDBUF=GetPrivateProfileInt("MAIN","SO_SNDBUF",0,IniFileNameFull);

		nsize=GetPrivateProfileString("MAIN","locale","Russian",lpBuffer,sizeof(lpBuffer),IniFileNameFull);
			Locale=std::string(lpBuffer);
		return true;
	}
	else
	{
		SaveToLog("PROGRAM: Load from ini file failed.",0);
		return false;
	}
}

void WriteIni()
{
	std::string::size_type n=std::string(IniFileNameFull).find_last_of(".");
	if(n!=std::string::npos)
	{
		WritePrivateProfileString("MAIN","ServerPort",nToString(CommonConfig.ServerPort).c_str(),IniFileNameFull);
		WritePrivateProfileString("MAIN","AdvancedLog",nToString(AdvancedLog).c_str(),IniFileNameFull);
		WritePrivateProfileString("MAIN","NETEXCEPT",nToString(NETEXCEPT).c_str(),IniFileNameFull);
		WritePrivateProfileString("MAIN","SO_RCVBUF",nToString(CommonConfig.Socket_RCVBUF).c_str(),IniFileNameFull);
		WritePrivateProfileString("MAIN","SO_SNDBUF",nToString(CommonConfig.Socket_SNDBUF).c_str(),IniFileNameFull);		
		WritePrivateProfileString("MAIN","SocketReadTimeout",nToString(CommonConfig.SocketReadTimeout).c_str(),IniFileNameFull);

		WritePrivateProfileString("MAIN","DBAddr",CommonConfig.DBAddr.c_str(),IniFileNameFull);
		WritePrivateProfileString("MAIN","DBPort",CommonConfig.DBPort.c_str(),IniFileNameFull);
		WritePrivateProfileString("MAIN","DBLogin",CommonConfig.DBLogin.c_str(),IniFileNameFull);
		WritePrivateProfileString("MAIN","DBPassword",CommonConfig.DBPassword.c_str(),IniFileNameFull);
		WritePrivateProfileString("MAIN","DBName",CommonConfig.DBName.c_str(),IniFileNameFull);
/*
		WritePrivateProfileString("MAIN","TCPStatPort",nToString(TCPStatPort).c_str(),IniFileNameFull);		
		WritePrivateProfileString("MAIN","TCPStatPassword",pNooRexStat_object.TCPStatPassword.c_str(),IniFileNameFull);
		WritePrivateProfileString("ADOServer","ADOProvider",ADOProvider.c_str(),IniFileNameFull);

		WritePrivateProfileString("ADOServer","ADOCommandTimeout",nToString(ADOCommandTimeout).c_str(),IniFileNameFull);
		WritePrivateProfileString("ADOServer","ADOConnectTimeout",nToString(ADOConnectTimeout).c_str(),IniFileNameFull);
		WritePrivateProfileString("ADOServer","ADOCommandRepeateCount",nToString(ADOCommandRepeateCount).c_str(),IniFileNameFull);
		WritePrivateProfileString("ADOServer","SaveQueueCount",nToString(SaveQueueCount).c_str(),IniFileNameFull);
		WritePrivateProfileString("ADOServer","SQLReconnectTime",nToString(SQLReconnectTime).c_str(),IniFileNameFull);		
*/
		SaveToLog("PROGRAM: Save to ini file.",0);
	}
}

// Message handler for ��������� box.
BOOL CALLBACK PropDlgProc(HWND hwndDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
	case WM_INITDIALOG:
		{
			SetDlgItemText(hwndDlg,IDC_LOGIN,CommonConfig.DBLogin.c_str());
			SetDlgItemText(hwndDlg,IDC_PASSWORD,CommonConfig.DBPassword.c_str());
			SetDlgItemText(hwndDlg,IDC_IP,CommonConfig.DBAddr.c_str());
			SetDlgItemText(hwndDlg,IDC_EDIT_DBPORT,CommonConfig.DBPort.c_str());
			SetDlgItemText(hwndDlg,IDC_EDIT_PORT,nToString(CommonConfig.ServerPort).c_str());
			SetDlgItemText(hwndDlg,IDC_EDIT_DATABASE,CommonConfig.DBName.c_str());
			//SetDlgItemText(hwndDlg,IDC_EDIT_PORTSTAT,nToString(TCPStatPort).c_str());

			SendDlgItemMessage(hwndDlg,IDC_CHECK_NETEXCEPT,BM_SETCHECK,(NETEXCEPT)?BST_CHECKED:BST_UNCHECKED,0);

			SetDlgItemText(hwndDlg,IDC_EDIT_SO_RCVBUF,nToString(CommonConfig.Socket_RCVBUF).c_str());
			SetDlgItemText(hwndDlg,IDC_EDIT_SO_SNDBUF,nToString(CommonConfig.Socket_SNDBUF).c_str());
			SetDlgItemText(hwndDlg,IDC_EDIT_SocketReadTimeout,nToString(CommonConfig.SocketReadTimeout).c_str());

			SendDlgItemMessage(hwndDlg,IDC_CHECK_LOG,BM_SETCHECK,(AdvancedLog)?BST_CHECKED:BST_UNCHECKED,0);
			SendDlgItemMessage(hwndDlg,IDC_CHECK_KeepAlive,BM_SETCHECK,(CommonConfig.KeepAlive)?BST_CHECKED:BST_UNCHECKED,0);
		}
	case WM_NOTIFY:
		{

		}
	case WM_COMMAND:
		{
			if (LOWORD(wParam) == IDC_OK1)
			{
				char tmp[1024];
				CommonConfig.ServerPort=GetDlgItemInt(hwndDlg,IDC_EDIT_PORT,0,0);
//				TCPStatPort=GetDlgItemInt(hwndDlg,IDC_EDIT_PORTSTAT,0,0);
				CommonConfig.SocketReadTimeout=GetDlgItemInt(hwndDlg,IDC_EDIT_SocketReadTimeout,0,0);

				NETEXCEPT=(SendDlgItemMessage(hwndDlg,IDC_CHECK_NETEXCEPT,BM_GETCHECK,0,0)==BST_CHECKED);
				CommonConfig.KeepAlive=(SendDlgItemMessage(hwndDlg,IDC_CHECK_KeepAlive,BM_GETCHECK,0,0)==BST_CHECKED);
				AdvancedLog=(SendDlgItemMessage(hwndDlg,IDC_CHECK_LOG,BM_GETCHECK,0,0)==BST_CHECKED);

				CommonConfig.Socket_RCVBUF=GetDlgItemInt(hwndDlg,IDC_EDIT_SO_RCVBUF,0,0);
				CommonConfig.Socket_SNDBUF=GetDlgItemInt(hwndDlg,IDC_EDIT_SO_SNDBUF,0,0);

				GetDlgItemText(hwndDlg,IDC_IP,tmp,1024);
				CommonConfig.DBAddr=std::string(tmp);
				GetDlgItemText(hwndDlg,IDC_LOGIN,tmp,32);
				CommonConfig.DBLogin=std::string(tmp);
				GetDlgItemText(hwndDlg,IDC_PASSWORD,tmp,32);
				CommonConfig.DBPassword=std::string(tmp);
				GetDlgItemText(hwndDlg,IDC_EDIT_DATABASE,tmp,128);
				CommonConfig.DBName=std::string(tmp);
				GetDlgItemText(hwndDlg,IDC_EDIT_DBPORT,tmp,128);
				CommonConfig.DBPort=std::string(tmp);

				EndDialog(hwndDlg, IDOK);
				WriteIni();
				SaveToLog("PROGRAM: restart object",0);
				DestroyApplication();
				ResetEvent(shutdownEvent);
				SaveToLog(" PROGRAM: set exit event/WM_COMMAND",1);
				if(hTTYWnd!=NULL)
				{
					SendMessage(hTTYWnd,WM_PAINT,0,0);
				}
				InitApplication();
				return TRUE;
			}
			if (LOWORD(wParam) == IDC_CANCEL1) 
			{
				EndDialog(hwndDlg, IDCANCEL);
				return TRUE;
			}
		}
		break;
	}
	return FALSE;
}
