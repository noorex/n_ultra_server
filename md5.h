/* MD5.HPP - header file for MD5.CPP */
#ifndef __MD5_H
#define __MD5_H

/* UINT2 defines a two byte word */
typedef unsigned short int UINT2;

/* UINT4 defines a four byte word */
typedef unsigned long int UINT4;

/* MD5 context. */
typedef struct {
  UINT4 state[4];                                   /* state (ABCD) */
  UINT4 count[2];        /* number of bits, modulo 2^64 (lsb first) */
  unsigned char buffer[64];                         /* input buffer */
} MD5_CTX;
#define MD5Context MD5_CTX

extern void MD5Init(MD5_CTX *context);
extern void MD5Update(MD5_CTX *context, unsigned char *input, UINT2 inputLen);
extern void MD5Final(unsigned char digest[16],MD5_CTX *context);
extern void makeMD5Sign(unsigned char *sign, unsigned char *buf, unsigned long len);

// Encodes input (UINT4) into output (unsigned char). 
// Assumes len is a multiple of 4. 
extern void EncodeUINT4 (unsigned char *output, UINT4 *input, UINT2 len);

// Decodes input (unsigned char) into output (UINT4). 
// Assumes len is a multiple of 4. 
extern void DecodeUINT4 (UINT4 *output, unsigned char *input, UINT2 len);

#endif

