#ifndef _AUTENTIF_H
#define _AUTENTIF_H

#include <string>
#include "Directory.h"
#include "json.h"

#ifdef _WIN32
#include <winsock2.h>
#else
#include <sys/socket.h>
#endif

//������ ������
enum OTaxiUltraCommandCodes
{
	//������� ��� �������
	OTUCommand_Autentif=0,
	OTUCommand_InsertOrder=1,
	OTUCommand_GetDirectory=2,
	OTUCommand_AcceptOrder=3,
	OTUCommand_SetOrderStatus=4,
	OTUCommand_GetStatement=5,
	OTUCommand_ReturnOrder=6,
	OTUCommand_ForceReturnOrder=7,
	OTUCommand_AddCarDescription=8,
	OTUCommand_GetTariffByPubisherAndAcceptor=9,
	OTUCommand_SetOrderNotifyMode=10,
	OTUCommand_GetOrderFullInfo=11,
	OTUCommand_GetOrderPartialInfo=12,
	OTUCommand_SendMessage=13,
	OTUCommand_Customer_Refuse=14
};

std::string GenerateAutenticString();
std::string SendXMLStatus(int Status,time_t NotifyTime,const std::string &ErrMessage,int OrderId);
bool SendTextBlock(int s,const std::string &String);

//int Authentif(int s, bool IsAutent, const std::string &CommandString,const std::string &RndString,unsigned int &CommandId,unsigned int &CommandCode);
int ProcessCommand(int s, bool IsAutent, const std::string &CommandString);

int Authentif(int s, bool IsAutent, unsigned int CommandId, unsigned int CommandCode, Json::Value &root);
int SendDirectory(int s, bool IsAutent, unsigned int CommandId, Json::Value &root);
int InsertOrder(int s, bool IsAutent, unsigned int CommandId, Json::Value &root);
int AcceptOrder(int s, bool IsAutent, unsigned int CommandId, Json::Value &root);
int SendOrderStatus(int s, bool IsAutent, unsigned int CommandId, Json::Value &root);
int ReturnOrder(int s, bool IsAutent, unsigned int CommandId, Json::Value &root);
int RefuseOrder(int s, bool IsAutent, unsigned int CommandId, Json::Value &root);
int GetStatement(int s, bool IsAutent, unsigned int CommandId, Json::Value &root);
int ForceReturnOrder(int s, bool IsAutent, unsigned int CommandId, Json::Value &root);
int AddCarDescription(int s, bool IsAutent, unsigned int CommandId, Json::Value &root);
int GetTariffByPubisherAndAcceptor(int s, bool IsAutent, unsigned int CommandId, Json::Value &root);
int SetOrderNotifyMode(int s, bool IsAutent, unsigned int CommandId, Json::Value &root);
int GetOrderFullInfo(int s, bool IsAutent, unsigned int CommandId, Json::Value &root);
int GetOrderPartInfo(int s, bool IsAutent, unsigned int CommandId, Json::Value &root);
int SendTextMessage(int s, bool IsAutent, unsigned int CommandId, Json::Value &root);

void Command_Init();
//����������� �������� ���������� � ������
void PropagateOrder(int Reason, const OrderStruct &Order);
void CheckOldOrders(); //��� ������ ����������� ������ ������ �������
void PutAsyncMessages(); //��� ������ ����������� �������� ����� �� AdditionalString
void SentOrderToNewConnection(int s); //��� �������� ����� ������� ����� ��������������� �������
bool IsNotifyCheck(time_t WriteTime,const ClientContext &Client); //��������, ������������� �� ����� �������� ���������� ��� �������������� �� �������� �������


#endif //_AUTENTIF_H
