//=============================================================================
//  Copyright (C) 2014, NooRex ( http://www.noorex.com/ )
//  The author of this program may be contacted at support@noorex.com
//=============================================================================
#ifndef _N_ULTRA_Server_
	#define _N_ULTRA_Server_
#include "..\n_lib\n_crash.h"
#include "..\n_lib\service.h"
#include "..\n_lib\n_util.h"
#include "..\n_lib\n_log.h"

#define IniFileName		"n_UltraServer.ini"	//  
#define LogFileName		"n_UltraServer.log"	//  
#define n_EXTRABYTES        sizeof( LONG )


bool LoadIni();
void WriteIni();
void	DestroyApplication();
bool	InitApplication();
BOOL NEAR InitApplicationWindow( HINSTANCE ) ;
HWND NEAR InitInstance( HINSTANCE, int ) ;
LRESULT FAR PASCAL MainWndProc( HWND, UINT, WPARAM, LPARAM);
BOOL CALLBACK PropDlgProc(HWND hwndDlg, UINT message, WPARAM wParam, LPARAM lParam); // Message handler for ������ box.
BOOL CALLBACK CrashDlgProc(HWND hwndDlg, UINT message, WPARAM wParam, LPARAM lParam);	// ���������� ������� ���� ��� ���� ������.
VOID MainServerThread(PVOID Arg);

#endif _N_ULTRA_Server_
