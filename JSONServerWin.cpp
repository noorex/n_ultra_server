#include <sys/types.h>
#include <winsock2.h>
#include <algorithm>
#include <stdio.h>
#include <time.h>
#include <sys/stat.h>
#include <stdio.h>
#include <algorithm>
#include <string>
#include <map>

#include "json.h"
#include "Directory.h"
#include "Command.h"
#include "DebugLog.h"

extern void SendMessageRepaint();	//��������� ��������� � ����
// ��������� ��������� ��� �������� ������ ClientContext
bool key_lesser (const std::pair <int,ClientContext> &elem1, const std::pair <int,ClientContext> &elem2)
{
	return (elem1.first < elem2.first);
}

void ReadCommonConfig();
std::map<int,ClientContext> clients;

extern HANDLE GetShutdownEvent();

char SocketProcessing();

VOID MainServerThread(PVOID Arg)
{
	srand((unsigned int)time(0));
	//ReadCommonConfig();
	//OpenDebugLog(CommonConfig.DebugLogFileName,CommonConfig.DebugLogMaxSize);
	WriteToDebugLog(0,0,false,"Server Start Port=%d",CommonConfig.ServerPort);
	int NumAttempt=0;
	while(1)
	{
		if(WaitForSingleObject(GetShutdownEvent(),10000)==WAIT_OBJECT_0)
			return;
		if(!ConnectDB(CommonConfig.DBAddr,CommonConfig.DBPort,CommonConfig.DBLogin,CommonConfig.DBPassword,CommonConfig.DBName))
		{
			WriteToDebugLog(0,0,true,"PROGRAM: Can't connect to db. Attempt Num=%d",NumAttempt);
		}
		else
		{
			WriteToDebugLog(0,0,false,"PROGRAM: Connect to database success");
			break;
		}
	}
	//if(NumAttempt==10) return;

	GetDirectoryesFromDB();
	if(CarrierTypeDirectory.size()==0)
	{
		WriteToDebugLog(0,0,true,"LOAD: CarrierTypeDirectory size = 0");
		return;
	}
	if(OrderStatusDirectory.size()==0)
	{
		WriteToDebugLog(0,0,true,"LOAD: OrderStatusDirectory size = 0");
		//return;
	}
	if(CityDirectory.size()==0)
	{
		WriteToDebugLog(0,0,true,"LOAD: CityDirectory size = 0");
		//return;
	}
	if(ClientDb.size()==0)
	{
		WriteToDebugLog(0,0,true,"LOAD: ClientDb list size =0 ");
		//return;
	}

	WriteToDebugLog(0,0,false,"LOAD: Command_Init");
	Command_Init();

	while(1)
	{
		char RetVal=SocketProcessing();
		if(RetVal==0)
			return;

		for(std::map<int,ClientContext>::iterator clients_it = clients.begin(); clients_it != clients.end(); clients_it++)
			if(clients_it->first>0) closesocket(clients_it->first);
		clients.clear();
	}
}

char SocketProcessing()
{
	char buf[1024];
	char buf1[1024];
	int bytes_read;

	int listener;
	struct sockaddr_in addr;


	WriteToDebugLog(0,0,false,"LOAD: server socket");
	listener = socket(AF_INET, SOCK_STREAM, 0);
	if(listener < 0)
	{
		WriteToDebugLog(listener,0,true,"LOAD: Can't open server socket");
		DisconnectDB();
		return -1;
	}
	unsigned long bl=1;
	ioctlsocket(listener, FIONBIO, &bl);

	addr.sin_family = AF_INET;
	addr.sin_port = htons(CommonConfig.ServerPort);
	addr.sin_addr.s_addr = INADDR_ANY;
	WriteToDebugLog(listener,0,false,"LOAD: bind");
	if(bind(listener, (struct sockaddr *)&addr, sizeof(addr)) < 0)
	{
		WriteToDebugLog(listener,0,true,"LOAD: Can't bind socket");
		DisconnectDB();
		closesocket(listener);
		return -2;
	}
	WriteToDebugLog(listener,0,false,"LOAD: listen socket");
	listen(listener, 2);

	clients.clear();

	while(1)
	{
		if(WaitForSingleObject(GetShutdownEvent(),1)==WAIT_OBJECT_0)
			break;


		//��� ������ ����������� ������ ������ �������
		CheckOldOrders();
		//��� ������ ����������� �������� ����� �� AdditionalString
		PutAsyncMessages();

		WriteToDebugLog(listener,0,false,"MAINLOOP: Cycle-It. fd_set-listener");
		// ��������� ��������� �������
		fd_set readset;
		FD_ZERO(&readset);
		FD_SET(listener, &readset);

		WriteToDebugLog(listener,0,false,"MAINLOOP: fd_set-clients");
		for(std::map<int,ClientContext>::iterator it = clients.begin(); it != clients.end(); it++)
		{
			FD_SET(it->first, &readset);
		}

		WriteToDebugLog(listener,0,false,"MAINLOOP: select");
		// ����� �������
		timeval timeout;
		timeout.tv_sec = 10;
		timeout.tv_usec = 0;

		// ��� ������� � ����� �� �������
		std::map<int,ClientContext>::iterator ClientIt=max_element(clients.begin(), clients.end(), key_lesser);
		int mx=0;
		if(ClientIt!=clients.end())
		{
			mx = max(listener, ClientIt->first);
		}
		else
		{
			mx=listener;
		}

		int res=select(mx+1, &readset, NULL, NULL, &timeout);
		if(res == 0)
		{
			WriteToDebugLog(listener,0,false,"MAINLOOP: select=0");
			continue;
		}
		if(res < 0)
		{
			WriteToDebugLog(mx+1,0,true,"MAINLOOP: Can't select. restart thread");
			DisconnectDB();
			SendMessageRepaint();
			closesocket(listener);
			return -3;
		}

		WriteToDebugLog(listener,0,false,"MAINLOOP: select=true");
		// ���������� ��� ������� � ��������� ��������������� ��������
		if(FD_ISSET(listener, &readset))
		{
			WriteToDebugLog(listener,0,false,"MAINLOOP: accept-begin");
			struct sockaddr_in FromAddr;
			int FromAddrLen=(int)sizeof(FromAddr);

			// �������� ����� ������ �� ����������, ���������� accept
			int sock = accept(listener, (sockaddr *)&FromAddr, &FromAddrLen);
			if(sock < 0)
			{
				WriteToDebugLog(listener,0,true,"MAINLOOP: accept < 0");
				DisconnectDB();
				SendMessageRepaint();
				closesocket(listener);
				return -4;
			}
			WriteToDebugLog(listener,0,false,"MAINLOOP: accept-true");
			unsigned long bl=1;
			ioctlsocket(sock, FIONBIO, &bl);

			//���������� �����������
			ClientContext CurContext;
			CurContext.FromAddr=ntohl(FromAddr.sin_addr.s_addr);
			WriteToDebugLog(sock,0,false,"MAINLOOP: new connection from addr %8.8lX",CurContext.FromAddr);
			CurContext.ConnectTime=time(0);
			CurContext.IsAuthent=0;
			CurContext.ClientId=-1;
			CurContext.AutentifString=GenerateAutenticString();
			CurContext.NotifyMode=-1;
			CurContext.NotifyBeginTime=ExpiredTime;
			CurContext.NotifyEndTime=0;

			clients[sock]=CurContext;
			WriteConnectonState();

			Json::Value root_put;
			root_put["reason"]=Json::Value(OTaxiUltraBlockReason_Autent);
			root_put["RndStr"]=Json::Value(CurContext.AutentifString.c_str());
			Json::FastWriter writer;
			std::string StrPut=writer.write(root_put);
			SendTextBlock(sock,StrPut);
		}

		std::map<int,ClientContext>::iterator it1;
		for(std::map<int,ClientContext>::iterator it = clients.begin(); it != clients.end(); )
		{
			if(FD_ISSET(it->first, &readset))
			{
				WriteToDebugLog(listener,0,false,"MAINLOOP: recv-begin");
				// ��������� ������ �� �������, ������ ��
				bytes_read = recv(it->first, buf, 1024, 0);

				if(bytes_read == 0)
				{
					// ���������� ���������, ������� ����� �� ���������
					WriteToDebugLog(it->first,0,false,"MAINLOOP: recv == 0. Disconnect");
					closesocket(it->first);
					it1=it,it1++;
					clients.erase(it);
					it=it1;
					WriteConnectonState();
					continue;
				}
				if(bytes_read < 0)
				{
					// ���������� ���������, ������� ����� �� ���������
					WriteToDebugLog(it->first,0,false,"MAINLOOP: recv < 0. Disconnect");
					closesocket(it->first);
					it1=it,it1++;
					clients.erase(it);
					it=it1;
					WriteConnectonState();
					continue;
				}
				if(bytes_read > 0)
				{
					memcpy(buf1,buf,bytes_read);
					buf1[bytes_read]=0;
					WriteToDebugLog(it->first,0,false,"MAINLOOP: Recv:%s",buf1);
					bool EndCommandFlag=false;
					char *n=strstr(buf1,"\b");
					if(n)
					{
						WriteToDebugLog(listener,0,false,"MAINLOOP: Recept command");
						EndCommandFlag=true;
						*n='\0';
						it->second.CurrentString+=buf1;
						//std::string CommandString=ConvertWinToKoi(it->second.CurrentString);
						std::string CommandString=it->second.CurrentString;
						it->second.CurrentString=n+1;
						if(CommandString=="Disconnect")
						{
							WriteToDebugLog(it->first,it->second.ClientId,false,"MAINLOOP: Disconnect client");
							closesocket(it->first);
							it1=it,it1++;
							clients.erase(it);
							it=it1;
							WriteConnectonState();
							SendMessageRepaint();
							continue;
						}
						WriteToDebugLog(listener,0,false,"MAINLOOP: ProcessCommand-begin");
						if(ProcessCommand(it->first,it->second.IsAuthent,CommandString)<0)
						{
							WriteToDebugLog(it->first,it->second.ClientId,true,"MAINLOOP: ProcessCommand < 0. Disconnect");
							closesocket(it->first);
							it1=it,it1++;
							clients.erase(it);
							it=it1;
							WriteConnectonState();
							SendMessageRepaint();
							continue;
						}
						WriteToDebugLog(listener,0,false,"MAINLOOP: ProcessCommand-end");
					}
					else
						it->second.CurrentString+=buf1;
				}
			}
			it++;
		}
	}
	DisconnectDB();
	SendMessageRepaint();
	closesocket(listener);
	for(std::map<int,ClientContext>::iterator clients_it = clients.begin(); clients_it != clients.end(); clients_it++)
		closesocket(clients_it->first);
	clients.clear();
	return 0;
}


void ReadCommonConfig()
{
	FILE *F=fopen("taxi_ultra_json","r");
	if(F)
	{
		while(!feof(F))
		{
			char CurString[200];
			fgets(CurString,200,F);
			int Len=strlen(CurString);
			for(int i=0;i<Len;i++)
				if((CurString[i]=='\n')||(CurString[i]=='\r'))
					CurString[i]=0;

			if(strstr(CurString,"DBAddr:")==CurString)
				CommonConfig.DBAddr=CurString+strlen("DBAddr:");
			if(strstr(CurString,"DBPort:")==CurString)
				CommonConfig.DBPort=atoi(CurString+strlen("DBPort:"));
			if(strstr(CurString,"DBLogin:")==CurString)
				CommonConfig.DBLogin=CurString+strlen("DBLogin:");
			if(strstr(CurString,"DBPassword:")==CurString)
				CommonConfig.DBPassword=CurString+strlen("DBPassword:");
			if(strstr(CurString,"DBName:")==CurString)
				CommonConfig.DBName=CurString+strlen("DBName:");
			if(strstr(CurString,"ServerPort:")==CurString)
				CommonConfig.ServerPort=atoi(CurString+strlen("ServerPort:"));
			if(strstr(CurString,"DebugLogFileName:")==CurString)
				CommonConfig.DebugLogFileName=CurString+strlen("DebugLogFileName:");
			if(strstr(CurString,"DebugLogMaxSize:")==CurString)
				CommonConfig.DebugLogMaxSize=atoi(CurString+strlen("DebugLogMaxSize:"));
		}
		fclose(F);
	}
}

//������� ���������� �������������� �����������
int	GetActiveConnectionsCount()
{
	int Count=0;
	std::map <int,ClientContext>::iterator clients_it;
	for(clients_it=clients.begin();clients_it!=clients.end();clients_it++)
	{
		if(clients_it->second.IsAuthent) Count++;
	}
	return Count;
}