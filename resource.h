//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by resource.rc
//
#define IDB_BITMAP2                     101
#define IDI_Icon_main                   102
#define IDD_LOGINDIALOG                 103
#define IDB_BITMAP3                     103
#define IDD_DIALOG_ABOUT                104
#define IDB_BITMAP4                     104
#define IDB_BITMAP1                     105
#define IDI_ICON1                       105
#define IDD_PROP_DIALOG                 106
#define IDR_MENU1                       107
#define IDD_WINDOWSMODE                 0x214
#define IDC_IP                          1000
#define IDD_TITLELINE                   1001
#define IDC_CHECK_LOG                   1001
#define IDD_VERSION                     1002
#define IDC_LOGIN                       1002
#define IDD_OK                          1003
#define IDC_PASSWORD                    1003
#define IDC_EDIT_DATABASE               1004
#define IDC_OrgName                     1005
#define IDC_EDIT_PORT                   1005
#define IDC_INN                         1006
#define IDC_Address                     1007
#define IDC_OrgNameFull                 1008
#define IDC_EDIT_UpdateServerPropertiesInterval 1010
#define IDC_EDIT_DBPORT                 1010
#define IDC_COMBO_ADOPROVIDER           1011
#define IDC_CHECK_NETEXCEPT             1012
#define IDC_EDIT_SO_SNDBUF              1015
#define IDC_EDIT_SO_RCVBUF              1016
#define IDC_EDIT_SocketReadTimeout      1017
#define IDC_EDIT_ADOCommandTimeout      1020
#define IDC_EDIT_ADOCONNECTTIMEOUT      1021
#define IDC_EDIT_ADOCommandRepeateCount 1022
#define IDC_STATIC_ADOCommandRepeateCount 1023
#define IDC_CHECK_KeepAlive             1024
#define IDC_STATIC_ADOCommandRepeateCount2 1025
#define IDC_EDIT_SaveQueueCount         1026
#define IDC_OK1                         1033
#define IDC_CANCEL1                     1034
#define IDC_STATIC1                     1035
#define IDC_STATIC2                     1036
#define IDC_STATIC3                     1037
#define IDC_STATIC4                     1038
#define IDC_STATIC_ADOPROVIDER          1040
#define IDC_STATIC_DATABASE             1042
#define IDC_STATIC9                     1047
#define IDC_STATIC12                    1048
#define IDC_STATIC13                    1049
#define IDC_STATIC_SocketReadTimeout    1050
#define IDC_STATIC_ADOCommandTimeout    1051
#define IDC_STATIC_ADOCommandTimeout2   1052
#define ID_40001                        40001
#define ID_40002                        40002
#define ID_MENU_PROP                    40003
#define ID_MENU_EXIT                    40004

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        108
#define _APS_NEXT_COMMAND_VALUE         40005
#define _APS_NEXT_CONTROL_VALUE         1001
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
