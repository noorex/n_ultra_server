#ifndef _DEBUGLOG_1__H
#define _DEBUGLOG_1__H

#include <string>
#include "Directory.h"
#include <stdarg.h>

#ifdef _WIN32
#include <time.h>
#else
#include <sys/time.h>
#endif

std::string ctime1(time_t Param);
void OpenDebugLog(const std::string &Name,long MaxSize);
void WriteToDebugLog(int s,int ClientId,bool IsErr,const char *ErrorMessage,...);

#endif //_DEBUGLOG_1__H
