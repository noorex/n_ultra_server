#include "Command.h"
#include "Directory.h"
#include "DebugLog.h"
#include "md5.h"
#include <string>
#include <list>
#include <algorithm>
#include <stdio.h>
#include "json.h"

//#define _COUNT_CLIENTS_2

#ifdef _WIN32
extern void	SendMessageRepaint();
#endif _WIN32

#ifdef _WIN32

#else
#include <unistd.h>
#include <sys/ioctl.h>

#define SOCKET int

int closesocket(SOCKET s)
{
	return close(s);
}
int ioctlsocket(SOCKET s,long cmd,u_long *argp)
{
	return ioctl(s,cmd,argp);
}

#endif

std::list <OrderStruct> FreeOrderList;

//инициализация командного интерпретатора
void Command_Init()
{
	FreeOrderList.clear();
	std::string AdditionStr="WHERE \"OrderState\"=\'0\'";
	FreeOrderList=GetOrderList(AdditionStr);
	WriteToDebugLog(0,0,false,"FreeOrderList - size=%d",FreeOrderList.size());
}

//что делает эта функция ?
bool IsCrossCityList(const std::list <int> &List1,const std::list <int> &List2)
{
	std::list <int>::const_iterator It1,It2;
	for(It1=List1.begin();It1!=List1.end();It1++)
	{
		for(It2=List2.begin();It2!=List2.end();It2++)
		{
			if(*It1 == *It2)
				return true;
		}
	}
	return false;
}

//что делает эта функция ?
bool IsInCityList(int City, const std::list <int> &List1)
{
	std::list <int>::const_iterator It1;
	for(It1=List1.begin();It1!=List1.end();It1++)
	{
		if(*It1 == City)
			return true;
	}
	return false;
}

//что делает эта функция ?
int GetReturnedStatusId()
{
	static int StatusId=-1;
	if(StatusId>=0)
		return StatusId;

	std::map <int,std::string>::const_iterator StatusIt;
	for(StatusIt=OrderStatusDirectory.begin();StatusIt!=OrderStatusDirectory.end();StatusIt++)
	{
		if(StatusIt->second=="возврат заказа")
		{
			StatusId=StatusIt->first;
			return StatusId;
		}
	}
	return StatusId;
}

//что делает эта функция ?
std::string GenerateAutenticString()
{
	std::string Result;
	for(int i=0;i<20;i++)
	{
		char Cur[5];
		sprintf(Cur,"%2.2X",rand()%255);
		Result+=Cur;
	}
	return Result;
}

//что делает эта функция ?
bool SendTextBlock(int s,const std::string &String)
{
	std::string SendString=String;
	//блок данных должен иметь формат JSON и обязательные поля
	if(SendString.length()<5)
		return false;

	//если в блоке нет разделит. символов - добавляем в конец
	if(SendString.find_last_of('\b')!=(SendString.length()-1))
		SendString+="\b";

	int Size=SendString.size();
	int i=0;

	while(i<Size)
	{
		int SendSize=Size-i;
		//разбиваем блоками по 500 байт
		if(SendSize>500)
			SendSize=500;

		int Size1=send(s,SendString.c_str()+i,SendSize,0);
		if(Size1<0)
		{
			WriteToDebugLog(s,0,false,"Ошибка записи в сокет. i=%d",i);
			return false;
		}
		else
		{
			i+=Size1;
		}
	}
	return true;
}

//что делает эта функция ?
int ProcessCommand(int s,bool IsAutent,const std::string &CommandString)
{
	WriteToDebugLog(s,0,false,"ProcessCommand. CommandString=%s",CommandString.c_str());

	Json::Value root;
	Json::Reader reader;

	bool parsingSuccessful = reader.parse( CommandString, root );
	if ( !parsingSuccessful )
	{
		return -1;
	}
	int CommandCode=root.get("CommandCode", "" ).asInt();
	unsigned int CommandId;
	try
	{
		CommandId=root.get("CommandId", "" ).asUInt();
	}
	catch(std::exception &)
	{
		CommandId=0;
	}

	switch(CommandCode)
	{
	case OTUCommand_Autentif:
		{
			return Authentif(s,IsAutent,CommandId,CommandCode,root);
		}
	case OTUCommand_GetDirectory:
		{
			return SendDirectory(s,IsAutent,CommandId,root);
		}
	case OTUCommand_InsertOrder:
		{
			return InsertOrder(s,IsAutent,CommandId,root);
		}
	case OTUCommand_AcceptOrder:
		{
			return AcceptOrder(s,IsAutent,CommandId,root);
		}
	case OTUCommand_ReturnOrder:
		{
			return ReturnOrder(s,IsAutent,CommandId,root);
		}
	case OTUCommand_Customer_Refuse:
		{
			return RefuseOrder(s,IsAutent,CommandId,root);
		}
	case OTUCommand_SetOrderStatus:
		{
			return SendOrderStatus(s,IsAutent,CommandId,root);
		}
	case OTUCommand_ForceReturnOrder:
		{
			return ForceReturnOrder(s,IsAutent,CommandId,root);
		}
	case OTUCommand_AddCarDescription:
		{
			return AddCarDescription(s,IsAutent,CommandId,root);
		}
	case OTUCommand_GetStatement:
		{
			return GetStatement(s,IsAutent,CommandId,root);
		}
	case OTUCommand_GetTariffByPubisherAndAcceptor:
		{
			return GetTariffByPubisherAndAcceptor(s,IsAutent,CommandId,root);
		}
	case OTUCommand_SetOrderNotifyMode:
		{
			return SetOrderNotifyMode(s,IsAutent,CommandId,root);
		}
	case OTUCommand_GetOrderFullInfo:
		{
			return GetOrderFullInfo(s,IsAutent,CommandId,root);
		}
	case OTUCommand_GetOrderPartialInfo:
		{
			return GetOrderPartInfo(s,IsAutent,CommandId,root);
		}
	case OTUCommand_SendMessage:
		{
			return SendTextMessage(s,IsAutent,CommandId,root);
		}
	}
	return -1;
}


//что делает эта функция ?
int CompareAutentInfo(int s,const std::string &RndString,const std::string &Login,const std::string &PassHash)
{
	ReadClients();

	std::map <int,ClientStruct>::iterator ClientDbIt;
	for(ClientDbIt=ClientDb.begin();ClientDbIt!=ClientDb.end();ClientDbIt++)
	{
		if(ClientDbIt->second.Login==Login)
			break;
	}
	if(ClientDbIt==ClientDb.end())
	{
		WriteToDebugLog(s,0,false,"Authent: login not found");
		return -2;
	}
	WriteToDebugLog(s,0,false,"Authent: login found");
	std::string PassStr=RndString+ClientDbIt->second.Password;

	unsigned char Sign1[16];
	makeMD5Sign(Sign1,(unsigned char *)PassStr.c_str(),PassStr.size());

	PassStr="";
	char Cur1[5];
	for(int i=0;i<16;i++)
	{
		sprintf(Cur1,"%2.2X",Sign1[i]);
		PassStr+=Cur1;
	}
	WriteToDebugLog(s,0,false,"Authent: PassStr=%s",PassStr.c_str());

	if(PassHash==PassStr)
	{
		return ClientDbIt->first;
	}
	return -3;  
}

//что делает эта функция ?
int Authentif(int s, bool IsAutent, unsigned int CommandId, unsigned int CommandCode, Json::Value &root)
{
	WriteToDebugLog(s,0,false,"Autentif command");
	std::string Login;
	std::string PassHash;
	int ClientId;

	try{
	Login=root.get("Login", "" ).asString();
	PassHash=root.get("PassHash", "" ).asString();
	}
	catch(std::exception &)
	{
		return -4;
	}

	std::map <int,ClientContext>::iterator clients_it;
	clients_it=clients.find(s);
	if(clients_it==clients.end())
	{
		WriteToDebugLog(s,0,true,"Absent client structure");
		return 0;
	}    

#ifdef _COUNT_CLIENTS_2
	ClientId=CompareAutentInfo(s,clients_it->second.AutentifString,Login,PassHash);
	if(clients.size()>=3)
	ClientId=-1;
#else
	ClientId=CompareAutentInfo(s,clients_it->second.AutentifString,Login,PassHash);
#endif

	if(ClientId<0)
	{
		Json::Value root_put;
		root_put["reason"]=Json::Value(OTaxiUltraBlockReason_Autent);
		root_put["CommandId"]=CommandId;
		root_put["CommandCode"]=CommandCode;
		root_put["Num"]=Json::Value(-1);
		root_put["ErrorString"]=Json::Value("Non autentif");
		root_put["OperationStatus"]=Json::Value(OTUStatus_AuthentError);
		root_put["ServerTime"]=Json::Value((int)time(0));
		clients_it->second.IsAuthent=false;
		clients_it->second.ClientId=-1;
		Json::FastWriter writer;
		std::string StrPut=writer.write(root_put);
		SendTextBlock(s,StrPut);
		return ClientId;
	}

	Json::Value root_put;
	root_put["reason"]=Json::Value(OTaxiUltraBlockReason_Autent);
	root_put["CommandId"]=CommandId;
	root_put["CommandCode"]=CommandCode;
	root_put["Num"]=Json::Value(ClientId);
	root_put["ErrorString"]=Json::Value("Ok");
	root_put["OperationStatus"]=Json::Value(OTUStatus_Ok);
	root_put["ServerTime"]=Json::Value((int)time(0));
	Json::FastWriter writer;
	std::string StrPut=writer.write(root_put);
	SendTextBlock(s,StrPut);
	clients_it->second.IsAuthent=true;
	clients_it->second.ClientId=ClientId;
	clients_it->second.SentOrders.clear();
	SentOrderToNewConnection(s);
#ifdef _WIN32
	SendMessageRepaint();
#endif _WIN32
	return ClientId;
}
    
int SendDirectory(int s, bool IsAutent, unsigned int CommandId, Json::Value &root)
{
    Json::Value CarrierTypeDir1;
    Json::Value OrderStatusDir1;
    Json::Value CityDir1;
    Json::Value ClientDbDir1;

	std::map <int,ClientContext>::iterator clients_it;
	clients_it=clients.find(s);
	if(clients_it==clients.end())
	{
		WriteToDebugLog(s,0,true,"Absent client structure");
		return 0;
	} 
	
	if(!IsAutent)
	{
		int ClientId=Authentif(s,IsAutent,CommandId,OTUCommand_GetDirectory,root);
		if(ClientId<0)
			return ClientId;
	}
	
	const std::list <int> &CityList = ClientDb[clients_it->second.ClientId].CityIdList;
	int IsAdmin=ClientDb[clients_it->second.ClientId].IsService;

    std::map <int,std::string>::const_iterator It;
    for(It=CityDirectory.begin();It!=CityDirectory.end();It++)
    {
        if(IsInCityList(It->first,CityList))
            CityDir1[ToString(It->first)]=Json::Value(It->second.c_str());
    }
    for(It=CarrierTypeDirectory.begin();It!=CarrierTypeDirectory.end();It++)
    {
        CarrierTypeDir1[ToString(It->first)]=Json::Value(It->second.c_str());
    }
    for(It=OrderStatusDirectory.begin();It!=OrderStatusDirectory.end();It++)
    {
        OrderStatusDir1[ToString(It->first)]=Json::Value(It->second.c_str());
    }
    std::map <int,ClientStruct>::iterator ClientDbIt;
    for(ClientDbIt=ClientDb.begin();ClientDbIt!=ClientDb.end();ClientDbIt++)
    {
        if(IsAdmin || IsCrossCityList(CityList,ClientDbIt->second.CityIdList))
        {
            Json::Value CurClient;
            CurClient["ClientId"]=Json::Value(ClientDbIt->second.ClientId);
            CurClient["FullName"]=Json::Value(ClientDbIt->second.FullName);
            CurClient["ShortName"]=Json::Value(ClientDbIt->second.ShortName);
            CurClient["LeaderName"]=Json::Value(ClientDbIt->second.LeaderName);
            CurClient["Comment"]=Json::Value(ClientDbIt->second.Comment);
            CurClient["TimeZone"]=Json::Value(ClientDbIt->second.TimeZone);

            Json::Value CurCityIdList;
            //вычищаем из списка городов те, в которых не работает наш клиент
            std::list<int>::iterator CityIdIt;
            for(CityIdIt=ClientDbIt->second.CityIdList.begin();CityIdIt!=ClientDbIt->second.CityIdList.end();CityIdIt++)
            {
                if(IsInCityList(*CityIdIt,CityList))
                    CurCityIdList[ToString(*CityIdIt)]=Json::Value(*CityIdIt);
            }

            CurClient["CityIdList"]=CurCityIdList;
            ClientDbDir1[ToString(ClientDbIt->second.ClientId)]=CurClient;
        }
    }
    Json::Value root_put;
	root_put["reason"]=Json::Value(OTaxiUltraBlockReason_Directory);
	root_put["CommandId"]=CommandId;
	root_put["CommandCode"]=OTUCommand_GetDirectory;
	root_put["Num"]=Json::Value(0);
	root_put["ErrorString"]=Json::Value("Ok");
	root_put["OperationStatus"]=Json::Value(OTUStatus_Ok);
	

    root_put["CarrierTypeDirectory"]=CarrierTypeDir1;
    root_put["OrderStatusDirectory"]=OrderStatusDir1;
    root_put["CityDirectory"]=CityDir1;
    root_put["ClientDbDirectory"]=ClientDbDir1;

    Json::FastWriter writer;
    std::string StrPut=writer.write(root_put);
    WriteToDebugLog(s,0,false,"String to put");
    //StrPut=ConvertKoiToWin(StrPut);
    WriteToDebugLog(s,0,false,"%s",StrPut.c_str());
    SendTextBlock(s,StrPut);
    return 1;
}



int InsertOrder(int s,bool IsAutent,unsigned int CommandId, Json::Value &root)
{
	WriteToDebugLog(s,0,false,"Insert Order");

	std::map <int,ClientContext>::iterator clients_it;
	clients_it=clients.find(s);
	if(clients_it==clients.end())
	{
		WriteToDebugLog(s,0,true,"Absent client structure");
		return 0;
	}
	if(!IsAutent)
	{
		int ClientId=Authentif(s,IsAutent,CommandId,OTUCommand_InsertOrder,root);
		if(ClientId<0)
			return ClientId;
	}

	//поля ордера
	OrderStruct CurOrder;
	CurOrder.LatitudeFrom=0.0;
	CurOrder.LongitudeFrom=0.0;
	CurOrder.LatitudeTo=0.0;
	CurOrder.LongitudeTo=0.0;
	
	CurOrder.TimeToLiveSec=0;
	CurOrder.ToCarMin=0;	
	CurOrder.SetCarInfoTime=0;

	CurOrder.CityIdFrom=root.get("CityIdFrom", "" ).asInt();
	CurOrder.StreetFrom=root.get("StreetFrom", "" ).asString();
	CurOrder.AdditionalFrom=root.get("AdditionalFrom", "" ).asString();
	
	CurOrder.CityIdTo=root.get("CityIdTo", "" ).asInt();
	CurOrder.StreetTo=root.get("StreetTo", "" ).asString();
	CurOrder.AdditionalTo=root.get("AdditionalTo", "" ).asString();
	CurOrder.CarrierType=root.get("CarrierType", "" ).asInt();
	CurOrder.Price=root.get("Price", "" ).asDouble();
	CurOrder.UniKey=root.get("UniKey", "" ).asInt();
	CurOrder.ClientPhone=root.get("ClientPhone", "" ).asString();
	CurOrder.StatusId=root.get("StatusId", "" ).asInt();
	CurOrder.PreliminaryTime=(time_t)(root.get("PreliminaryTime", "" ).asUInt());
	CurOrder.Time0Km1Fixed2=root.get("Time0Km1Fixed2", "" ).asInt();
	CurOrder.Comment=root.get("Comment", "" ).asString();
	


	try{
		CurOrder.LatitudeFrom=root.get("LatitudeFrom", "" ).asDouble();
		CurOrder.LongitudeFrom=root.get("LongitudeFrom", "" ).asDouble();
		CurOrder.LatitudeTo=root.get("LatitudeTo", "" ).asDouble();
		CurOrder.LongitudeTo=root.get("LongitudeTo", "" ).asDouble();
		CurOrder.TimeToLiveSec=root.get("TimeToLiveSec", "" ).asInt();
	}catch(std::exception &)
	{}

	//необязательное поле CityId заполняется значением по умолчанию
	if(CurOrder.CityIdFrom<0)
	{
		std::map <int,ClientStruct>::iterator ClientDbIt;
		ClientDbIt=ClientDb.find(clients_it->second.ClientId);
		if(ClientDbIt!=ClientDb.end())
		{
			CurOrder.CityIdFrom=*ClientDbIt->second.CityIdList.begin();
		}
	}
	if(CurOrder.CityIdTo<0)
	{
		std::map <int,ClientStruct>::iterator ClientDbIt;
		ClientDbIt=ClientDb.find(clients_it->second.ClientId);
		if(ClientDbIt!=ClientDb.end())
		{
			CurOrder.CityIdTo=*ClientDbIt->second.CityIdList.begin();
		}
	}
	//необязательное поле "состояние заказа" заполняется значением по умолчанию
	if(CurOrder.StatusId<0)
		CurOrder.StatusId=GetFirstStatusId();
	std::string ErrorString="Ok";
	WriteToDebugLog(s,clients_it->second.ClientId,false,"Insert Order: correctness check");

	while(1) //цикл только чтобы сделать break
	{
		//проверки на корректность введенных данных
		if(CurOrder.StreetFrom.size()==0)
		{
			ErrorString="не введено поле улица (откуда)";
			break;
		}
		//проверки на корректность введенных данных
		if(CurOrder.ClientPhone.size()==0)
		{
			ErrorString="не введено поле номера телефона";
			break;
		}
		std::map <int,std::string>::iterator CarrierTypeDirectoryIt=CarrierTypeDirectory.find(CurOrder.CarrierType);
		if(CarrierTypeDirectoryIt==CarrierTypeDirectory.end())
		{
			ErrorString="указано транспортное средство, отсутсвующее в справочнике";
			break;
		}
		if(CurOrder.PreliminaryTime && (CurOrder.PreliminaryTime<time(NULL)))
		{
			ErrorString="указанное в предварительном заказе время, уже наступило";
			break;
		}
		break;
	}
	//теперь вычисление тех полей, которые должны быть вычислены

	CurOrder.OrderId=GetNextOrderId(); //заполняется сервером (последний номер из таблицы+1)
	CurOrder.OrderState=OrderStateNew; //статус заказа - новый
	CurOrder.WriteTime=time(NULL);
	CurOrder.Publisher=clients_it->second.ClientId;
	CurOrder.Acceptor=-1;

	CurOrder.ServiceComission=0;
	CurOrder.PublisherComission=0;
	time_t CurTime=time(NULL);
	CurOrder.AcceptTime=0;
	CurOrder.CarCallNumber=0;
	WriteToDebugLog(s,clients_it->second.ClientId,false,"Insert Order: correctness check. Errorstring=%s",ErrorString.c_str());

	if(ErrorString=="Ok")
	{
		//делаем записи в таблице заказов и в таблице журнала операций
		if(InsertOrderDB(CurOrder)<0)
		{
			ErrorString="не могу записать заказ в БД";
		}
		else
		{
			FreeOrderList.push_back(CurOrder);
			PropagateOrder(OTaxiUltraBlockReason_NotifyNew, CurOrder);
			//PropagateOrder(OTaxiUltraBlockReason_NotifyChange, CurOrder);
			WriteToDebugLog(0,0,false,"FreeOrderList - size=%d",FreeOrderList.size());
		}
	}

	Json::Value root_put;
	root_put["reason"]=Json::Value(OTaxiUltraBlockReason_NumStatus);
	root_put["CommandId"]=Json::Value(CommandId);
	root_put["CommandCode"]=OTUCommand_InsertOrder;
	root_put["Num"]=Json::Value(CurOrder.OrderId);
	root_put["ErrorString"]=Json::Value(ErrorString);
	root_put["OperationStatus"]=Json::Value((ErrorString=="Ok")?OTUStatus_Ok:OTUStatus_InsertOrderError);
	root_put["UniKey"]=Json::Value(CurOrder.UniKey);

	Json::FastWriter writer;
	std::string StrPut=writer.write(root_put);
	WriteToDebugLog(s,0,false,"String to put");
	WriteToDebugLog(s,0,false,"%s",StrPut.c_str());
	SendTextBlock(s,StrPut);
	return 1;
}

int AcceptOrder(int s,bool IsAutent,unsigned int CommandId, Json::Value &root)
{
	WriteToDebugLog(s,0,false,"AcceptOrder Order");

	int OrderId=root.get("OrderId", "" ).asInt();

	std::map <int,ClientContext>::iterator clients_it;
	clients_it=clients.find(s);
	if(clients_it==clients.end())
	{
		WriteToDebugLog(s,0,true,"Absent client structure");
		return 0;
	}    

	if(!IsAutent)
	{
		int ClientId=Authentif(s,IsAutent,CommandId,OTUCommand_AcceptOrder,root);
		if(ClientId<0)
			return ClientId;
	}

	std::map <int,ClientStruct>::iterator ClientDbIt=ClientDb.find(clients_it->second.ClientId);
	if(ClientDbIt==ClientDb.end())
	{
		WriteToDebugLog(s,0,true,"Absent ClientDbIt structure");
		return 0;
	}

	std::string ErrorString="Не удалось найти заказ среди свободных";
	std::list <OrderStruct>::iterator FreeOrderListIt;
	for(FreeOrderListIt=FreeOrderList.begin();FreeOrderListIt!=FreeOrderList.end();FreeOrderListIt++)
	{
		//проверяем состояние захватываемого заказа и его наличие в списке свободных заказов
		if(FreeOrderListIt->OrderId==OrderId)
		{
			//проверяем, работает ли клиент в городах откуда и куда
			if(IsInCityList(FreeOrderListIt->CityIdFrom, ClientDbIt->second.CityIdList) && IsInCityList(FreeOrderListIt->CityIdTo, ClientDbIt->second.CityIdList))
			{
				//проверяем, не является ли клиент публикатором этого заказа
				if(FreeOrderListIt->Publisher!=ClientDbIt->first)
				{
					//тут надо выждать тайм-аут по отношениям
					int TimeOut=RelationsCheck(FreeOrderListIt->Publisher,clients_it->second.ClientId);

					if(TimeOut<0)
					{
						ErrorString="Невозможно взять заказ из-за запрещенных отношений между клиентами";
						break;
					}
					if(time(0)>FreeOrderListIt->WriteTime+TimeOut)
					{
						ErrorString="Ok";
						break;
					}
					else
					{
						ErrorString="Установлен тайм-аут для приёма заказов данного публикатора";
					}
				}
				else
				{
					ErrorString="Невозможно взять заказ, размещенный самим клиентом";
					break;
				}
			}
			else
			{
				ErrorString="Невозможно взять заказ, размещенный для другом городе";
				break;
			}
		}
	}

	WriteToDebugLog(s,clients_it->second.ClientId,false,"AcceptOrder Order: correctness check. Errorstring=%s",ErrorString.c_str());
	if(ErrorString=="Ok")
	{
		FreeOrderListIt->OrderState=OrderStateAccept; //статус заказа - новый
		FreeOrderListIt->AcceptTime=time(NULL);
		FreeOrderListIt->Acceptor=clients_it->second.ClientId;
		//здесь должен быть подсчет комиссий в соответствии с тарифом

		TariffStruct CurTariff=GetOrderTariff(FreeOrderListIt->Publisher,FreeOrderListIt->CityIdFrom);
		float OrderPrice=FreeOrderListIt->Price;
		if(OrderPrice<CurTariff.MinOrderPrice)
			OrderPrice=CurTariff.MinOrderPrice;

		float ServiceComission=CurTariff.ServiceFixed+(OrderPrice*CurTariff.ServicePercent)/100;
		float PublisherComission=CurTariff.PublisherFixed+(OrderPrice*CurTariff.PublisherPercent)/100;

		FreeOrderListIt->ServiceComission=ServiceComission;
		FreeOrderListIt->PublisherComission=PublisherComission;
		WriteToDebugLog(s,clients_it->second.ClientId,false,"AcceptOrder ServiceComission=%d, PublisherComission=%d",FreeOrderListIt->ServiceComission,FreeOrderListIt->PublisherComission);

		//обновление заказа в БД
		if(AcceptOrderDB(*FreeOrderListIt)<0)
		{
			ErrorString="не могу обновить заказ в БД";
		}
		else
		{
			PropagateOrder(OTaxiUltraBlockReason_NotifyNewDel, *FreeOrderListIt);
			PropagateOrder(OTaxiUltraBlockReason_NotifyChange, *FreeOrderListIt);
			FreeOrderList.erase(FreeOrderListIt);
		}
	}

	Json::Value root_put;
	root_put["reason"]=Json::Value(OTaxiUltraBlockReason_NumStatus);
	root_put["CommandId"]=Json::Value(CommandId);
	root_put["CommandCode"]=OTUCommand_AcceptOrder;
	root_put["Num"]=Json::Value(OrderId);
	root_put["ErrorString"]=Json::Value(ErrorString);
	root_put["OperationStatus"]=Json::Value((ErrorString=="Ok")?OTUStatus_Ok:OTUStatus_SetOrderStatusError);

	if(ErrorString=="Установлен тайм-аут для приёма заказов данного публикатора")
	{
		root_put["OperationStatus"]=Json::Value(OTUStatus_TimeOutNotify);
	}

	Json::FastWriter writer;
	std::string StrPut=writer.write(root_put);
	WriteToDebugLog(s,0,false,"String to put");
	WriteToDebugLog(s,0,false,"%s",StrPut.c_str());
	SendTextBlock(s,StrPut);
	return 1;
}

int ReturnOrder(int s,bool IsAutent,unsigned int CommandId, Json::Value &root)
{
	WriteToDebugLog(s,0,false,"ReturnOrder");

	int OrderId=root.get("OrderId", "" ).asInt();

	std::map <int,ClientContext>::iterator clients_it;
	clients_it=clients.find(s);
	if(clients_it==clients.end())
	{
		WriteToDebugLog(s,0,true,"Absent client structure");
		return 0;
	}    

	if(!IsAutent)
	{
		int ClientId=Authentif(s,IsAutent,CommandId,OTUCommand_ReturnOrder,root);
		if(ClientId<0)
			return ClientId;
	}

	std::map <int,ClientStruct>::iterator ClientDbIt=ClientDb.find(clients_it->second.ClientId);
	if(ClientDbIt==ClientDb.end())
	{
		WriteToDebugLog(s,0,true,"Absent ClientDbIt structure");
		return 0;
	}

	OrderStruct CurOrder;
	int Res=ReturnMyOrderDB(OrderId, clients_it->second.ClientId, &CurOrder);
	std::string ErrorString="Ok";

	if(Res==2) //возврат нового заказа
	{
		std::list <OrderStruct>::iterator FreeOrderListIt;
		for(FreeOrderListIt=FreeOrderList.begin();FreeOrderListIt!=FreeOrderList.end();FreeOrderListIt++)
		{
			//проверяем состояние захватываемого заказа и его наличие в списке свободных заказов
			if(FreeOrderListIt->OrderId==OrderId)
			{
				PropagateOrder(OTaxiUltraBlockReason_NotifyNewDel, *FreeOrderListIt);
				PropagateOrder(OTaxiUltraBlockReason_NotifyChange, *FreeOrderListIt);
				FreeOrderList.erase(FreeOrderListIt);
				break;
			}
		}
	}

	OTaxiUltraClientStatus Status=OTUStatus_Ok;
	switch (Res)
	{
	case -1:
		ErrorString="Заказ не найден";
		Status=OTUStatus_SetOrderStatusError;
		break;    
	case -2:
		ErrorString="Заказ не принят ни одним клиентом";
		Status=OTUStatus_SetOrderStatusError;
		break;    
	case -3:
		ErrorString="Заказ не принадлежит пользователю, возвращающему его";
		Status=OTUStatus_SetOrderStatusError;
		break;    
	case -4:
		ErrorString="Заказ закрыт";
		Status=OTUStatus_SetOrderStatusError;
		break;    
	case -5:
		ErrorString="Состояние заказа не позволяет его вернуть";
		Status=OTUStatus_SetOrderStatusError;
		break;
	case -6:
		ErrorString="Не удалось записать изменения в БД";
		Status=OTUStatus_SetOrderStatusError;
		break;    
	}

	if(ErrorString=="Ok")
	{
		PropagateOrder(OTaxiUltraBlockReason_NotifyChange, CurOrder);
	}

	/////////////////////////////
	Json::Value root_put;
	root_put["reason"]=Json::Value(OTaxiUltraBlockReason_NumStatus);
	root_put["CommandId"]=Json::Value(CommandId);
	root_put["CommandCode"]=OTUCommand_ReturnOrder;
	root_put["Num"]=Json::Value(CurOrder.OrderId);
	root_put["ErrorString"]=Json::Value(ErrorString);
	root_put["OperationStatus"]=Json::Value(Status);

	Json::FastWriter writer;
	std::string StrPut=writer.write(root_put);
	WriteToDebugLog(s,0,false,"String to put");
	WriteToDebugLog(s,0,false,"%s",StrPut.c_str());
	SendTextBlock(s,StrPut);
	return 0;
}

int RefuseOrder(int s, bool IsAutent, unsigned int CommandId, Json::Value &root)
{
	WriteToDebugLog(s,0,false,"RefuseOrder");

	int OrderId=root.get("OrderId", "" ).asInt();

	std::map <int,ClientContext>::iterator clients_it;
	clients_it=clients.find(s);
	if(clients_it==clients.end())
	{
		WriteToDebugLog(s,0,true,"Absent client structure");
		return 0;
	}    

	if(!IsAutent)
	{
		int ClientId=Authentif(s,IsAutent,CommandId,OTUCommand_Customer_Refuse,root);
		if(ClientId<0)
			return ClientId;
	}

	std::map <int,ClientStruct>::iterator ClientDbIt=ClientDb.find(clients_it->second.ClientId);
	if(ClientDbIt==ClientDb.end())
	{
		WriteToDebugLog(s,0,true,"Absent ClientDbIt structure");
		return 0;
	}

	OrderStruct CurOrder;
	int Res=RefuseMyOrderDB(OrderId, clients_it->second.ClientId, &CurOrder);
	std::string ErrorString="Ok";

	if(Res==2) //возврат нового заказа
	{
		std::list <OrderStruct>::iterator FreeOrderListIt;
		for(FreeOrderListIt=FreeOrderList.begin();FreeOrderListIt!=FreeOrderList.end();FreeOrderListIt++)
		{
			//проверяем состояние захватываемого заказа и его наличие в списке свободных заказов
			if(FreeOrderListIt->OrderId==OrderId)
			{
				PropagateOrder(OTaxiUltraBlockReason_NotifyNewDel, *FreeOrderListIt);
				PropagateOrder(OTaxiUltraBlockReason_NotifyChange, *FreeOrderListIt);
				FreeOrderList.erase(FreeOrderListIt);
				break;
			}
		}
	}

	OTaxiUltraClientStatus Status=OTUStatus_Ok;
	switch (Res)
	{
	case -1:
		ErrorString="Заказ не найден";
		Status=OTUStatus_SetOrderStatusError;
		break;    
	case -2:
		ErrorString="Заказ не принят ни одним клиентом";
		Status=OTUStatus_SetOrderStatusError;
		break;    
	case -3:
		ErrorString="Заказ не принадлежит пользователю, возвращающему его";
		Status=OTUStatus_SetOrderStatusError;
		break;    
	case -4:
		ErrorString="Заказ закрыт";
		Status=OTUStatus_SetOrderStatusError;
		break;    
	case -5:
		ErrorString="Состояние заказа не позволяет его вернуть";
		Status=OTUStatus_SetOrderStatusError;
		break;
	case -6:
		ErrorString="Не удалось записать изменения в БД";
		Status=OTUStatus_SetOrderStatusError;
		break;    
	}

	if(ErrorString=="Ok")
	{
		PropagateOrder(OTaxiUltraBlockReason_NotifyChange, CurOrder);
	}

	/////////////////////////////
	Json::Value root_put;
	root_put["reason"]=Json::Value(OTaxiUltraBlockReason_NumStatus);
	root_put["CommandId"]=Json::Value(CommandId);
	root_put["CommandCode"]=OTUCommand_ReturnOrder;
	root_put["Num"]=Json::Value(CurOrder.OrderId);
	root_put["ErrorString"]=Json::Value(ErrorString);
	root_put["OperationStatus"]=Json::Value(Status);

	Json::FastWriter writer;
	std::string StrPut=writer.write(root_put);
	WriteToDebugLog(s,0,false,"String to put");
	WriteToDebugLog(s,0,false,"%s",StrPut.c_str());
	SendTextBlock(s,StrPut);
	return 0;
}

int SendOrderStatus(int s,bool IsAutent,unsigned int CommandId, Json::Value &root)
{
	WriteToDebugLog(s,0,false,"ReturnOrder");

	int OrderId=root.get("OrderId", "" ).asInt();
	int StatusId=root.get("StatusId", "" ).asInt();
	float Price=root.get("Price", "" ).asDouble();

	std::map <int,ClientContext>::iterator clients_it;
	clients_it=clients.find(s);
	if(clients_it==clients.end())
	{
		WriteToDebugLog(s,clients_it->second.ClientId,true,"Absent client structure");
		return 0;
	}    

	if(!IsAutent)
	{
		int ClientId=Authentif(s,IsAutent,CommandId,OTUCommand_SetOrderStatus,root);
		if(ClientId<0)
			return ClientId;
	}

	std::map <int,ClientStruct>::iterator ClientDbIt=ClientDb.find(clients_it->second.ClientId);
	if(ClientDbIt==ClientDb.end())
	{
		WriteToDebugLog(s,0,true,"Absent ClientDbIt structure");
		return 0;
	}

	OrderStruct CurOrder;
	int Res=WriteOrderParamsDB(OrderId, Price, StatusId, OrderStateNotPublished, &CurOrder);

	if(Res==2) //изменение нового заказа
	{
		std::list <OrderStruct>::iterator FreeOrderListIt;
		for(FreeOrderListIt=FreeOrderList.begin();FreeOrderListIt!=FreeOrderList.end();FreeOrderListIt++)
		{
			//проверяем состояние захватываемого заказа и его наличие в списке свободных заказов
			if(FreeOrderListIt->OrderId==OrderId)
			{
				*FreeOrderListIt=CurOrder;
				break;
			}
		}
	}

	std::string ErrorString="Ok";
	OTaxiUltraClientStatus Status=OTUStatus_Ok;
	switch (Res)
	{
	case -1:
		ErrorString="Заказ не найден";
		Status=OTUStatus_SetOrderStatusError;
		break;    
	case -2:
		ErrorString="Некорректно указаны параметры состояния заказа и цены";
		Status=OTUStatus_SetOrderStatusError;
		break;    
	case -3:
		ErrorString="Не удалось записать изменения в БД";
		Status=OTUStatus_SetOrderStatusError;
		break;    
	case -4:
		ErrorString="Заказ закрыт";
		Status=OTUStatus_SetOrderStatusError;
		break;    
	}

	if(ErrorString=="Ok")
	{
		PropagateOrder(OTaxiUltraBlockReason_NotifyChange, CurOrder);
		if(Res==2)
		{
			PropagateOrder(OTaxiUltraBlockReason_NotifyNew, CurOrder);
		}
	}

	/////////////////////////////
	Json::Value root_put;
	root_put["reason"]=Json::Value(OTaxiUltraBlockReason_NumStatus);
	root_put["CommandId"]=Json::Value(CommandId);
	root_put["CommandCode"]=OTUCommand_SetOrderStatus;
	root_put["Num"]=Json::Value(CurOrder.OrderId);
	root_put["ErrorString"]=Json::Value(ErrorString);
	root_put["OperationStatus"]=Json::Value(Status);

	Json::FastWriter writer;
	std::string StrPut=writer.write(root_put);
	WriteToDebugLog(s,0,false,"String to put");
	WriteToDebugLog(s,0,false,"%s",StrPut.c_str());
	SendTextBlock(s,StrPut);
	return 0;
}

int ForceReturnOrder(int s,bool IsAutent,unsigned int CommandId, Json::Value &root)
{
	WriteToDebugLog(s,0,false,"ForceReturnOrder");

	int OrderId=root.get("OrderId", "" ).asInt();

	std::map <int,ClientContext>::iterator clients_it;
	clients_it=clients.find(s);
	if(clients_it==clients.end())
	{
		WriteToDebugLog(s,0,true,"Absent client structure");
		return 0;
	}

	if(!IsAutent)
	{
		int ClientId=Authentif(s,IsAutent,CommandId,OTUCommand_ForceReturnOrder,root);
		if(ClientId<0)
			return ClientId;
	}

	std::map <int,ClientStruct>::iterator ClientDbIt=ClientDb.find(clients_it->second.ClientId);
	if(ClientDbIt==ClientDb.end())
	{
		WriteToDebugLog(s,0,true,"Absent ClientDbIt structure");
		return 0;
	}
	OrderStruct CurOrder;
	int Res=ReturnForeignOrderDB(OrderId, clients_it->second.ClientId, &CurOrder);
	//сносим заказ, чтобы бывший аксептор его не видел
	CurOrder.Acceptor=0;
	PropagateOrder(OTaxiUltraBlockReason_NotifyChangeDel, CurOrder);

	std::string ErrorString="Ok";
	OTaxiUltraClientStatus Status=OTUStatus_Ok;
	switch (Res)
	{
	case -1:
		ErrorString="Заказ не найден";
		Status=OTUStatus_SetOrderStatusError;
		break;    
	case -2:
		ErrorString="Заказ не принят ни одним клиентом";
		Status=OTUStatus_SetOrderStatusError;
		break;    
	case -3:
		ErrorString="Заказ не принадлежит пользователю, возвращающему его";
		Status=OTUStatus_SetOrderStatusError;
		break;    
	case -4:
		ErrorString="Заказ закрыт";
		Status=OTUStatus_SetOrderStatusError;
		break;    
	case -5:
		ErrorString="Состояние заказа не позволяет его вернуть";
		Status=OTUStatus_SetOrderStatusError;
		break;
	case -6:
		ErrorString="Не удалось записать изменения в БД";
		Status=OTUStatus_SetOrderStatusError;
		break;    
	}

	if(ErrorString=="Ok")
	{
		FreeOrderList.push_back(CurOrder);
		PropagateOrder(OTaxiUltraBlockReason_NotifyNew, CurOrder);
		PropagateOrder(OTaxiUltraBlockReason_NotifyChange, CurOrder);
	}

	/////////////////////////////
	Json::Value root_put;
	root_put["reason"]=Json::Value(OTaxiUltraBlockReason_NumStatus);
	root_put["CommandId"]=Json::Value(CommandId);
	root_put["CommandCode"]=OTUCommand_ForceReturnOrder;
	root_put["Num"]=Json::Value(CurOrder.OrderId);
	root_put["ErrorString"]=Json::Value(ErrorString);
	root_put["OperationStatus"]=Json::Value(Status);

	Json::FastWriter writer;
	std::string StrPut=writer.write(root_put);
	WriteToDebugLog(s,0,false,"String to put");
	WriteToDebugLog(s,0,false,"%s",StrPut.c_str());
	SendTextBlock(s,StrPut);
	return 0;
}

int AddCarDescription(int s,bool IsAutent,unsigned int CommandId, Json::Value &root)
{
	WriteToDebugLog(s,0,false,"AddCarDescription");
	std::map <int,ClientContext>::iterator clients_it;
	clients_it=clients.find(s);
	if(clients_it==clients.end())
	{
		WriteToDebugLog(s,0,true,"Absent client structure");
		return 0;
	}

	if(!IsAutent)
	{
		int ClientId=Authentif(s,IsAutent,CommandId,OTUCommand_AddCarDescription,root);
		if(ClientId<0)
			return ClientId;
	}

	int OrderId=root.get("OrderId", "" ).asInt();
	std::string FullDescription=root.get("FullDescription", "" ).asString();
	std::string GovNumber=root.get("GovNumber", "" ).asString();
	int CallNumber=root.get("CallNumber", "" ).asInt();
	int ToCarMin=0;
	time_t SetCarInfoTime=time(NULL);

	try{
		ToCarMin=root.get("ToCarMin", "" ).asInt();
	}catch(std::exception &)
	{}

	std::string ErrorString="Ok";
	OTaxiUltraClientStatus Status=OTUStatus_Ok;
	OrderStruct CurOrder;
	int Res=AddCarDescriptionDB(OrderId,clients_it->second.ClientId,FullDescription,GovNumber,CallNumber, &CurOrder, SetCarInfoTime, ToCarMin);
	if(Res<0)
	{
		ErrorString="Ошибка добавления описания ТС";
		Status=OTUStatus_CommonError;
	}

	///////////////
	if(ErrorString=="Ok")
	{
		PropagateOrder(OTaxiUltraBlockReason_NotifyChange, CurOrder);
	}

	Json::Value root_put;
	root_put["reason"]=Json::Value(OTaxiUltraBlockReason_NumStatus);
	root_put["CommandId"]=Json::Value(CommandId);
	root_put["CommandCode"]=OTUCommand_AddCarDescription;
	root_put["Num"]=Json::Value(CurOrder.OrderId);
	root_put["ErrorString"]=Json::Value(ErrorString);
	root_put["OperationStatus"]=Json::Value(Status);

	Json::FastWriter writer;
	std::string StrPut=writer.write(root_put);
	WriteToDebugLog(s,0,false,"String to put");
	WriteToDebugLog(s,0,false,"%s",StrPut.c_str());
	SendTextBlock(s,StrPut);
	return 0;
}


int GetStatement(int s,bool IsAutent,unsigned int CommandId, Json::Value &root)
{
	WriteToDebugLog(s,0,false,"GetStatement");
	std::map <int,ClientContext>::iterator clients_it;
	clients_it=clients.find(s);
	if(clients_it==clients.end())
	{
		WriteToDebugLog(s,0,true,"Absent client structure");
		return 0;
	} 
	if(!IsAutent)
	{
		int ClientId=Authentif(s,IsAutent,CommandId,OTUCommand_GetStatement,root);
		if(ClientId<0)
			return ClientId;
	}

	//////////////////////////////

	int Statement=1234;


	/////////////////////////////

	Json::Value root_put;
	root_put["reason"]=Json::Value(OTaxiUltraBlockReason_Statement);
	root_put["CommandId"]=Json::Value(CommandId);
	root_put["CommandCode"]=OTUCommand_GetStatement;
	root_put["Num"]=Json::Value(Statement);
	root_put["ErrorString"]=Json::Value("Ok");
	root_put["OperationStatus"]=Json::Value(OTUStatus_Ok);

	Json::FastWriter writer;
	std::string StrPut=writer.write(root_put);
	WriteToDebugLog(s,0,false,"String to put");
	WriteToDebugLog(s,0,false,"%s",StrPut.c_str());
	SendTextBlock(s,StrPut);
	return 0;
}

int GetTariffByPubisherAndAcceptor(int s, bool IsAutent, unsigned int CommandId, Json::Value &root)
{
	WriteToDebugLog(s,0,false,"GetTariffByPubisherAndAcceptor");

	if(!IsAutent)
	{
		int ClientId=Authentif(s,IsAutent,CommandId,OTUCommand_GetTariffByPubisherAndAcceptor,root);
		if(ClientId<0)
			return ClientId;
	}

	int Publisher=root.get("Publisher", "" ).asInt();
	int Acceptor=root.get("Acceptor", "" ).asInt();
	int CityId=root.get("CityId", "" ).asInt();

	//////////////////////////////
	TariffStruct CurTariff;
	CurTariff=GetOrderTariff(Publisher,CityId);
	/////////////////////////////

	Json::Value root_put;
	root_put["reason"]=Json::Value(OTaxiUltraBlockReason_Tariff);
	root_put["CommandId"]=Json::Value(CommandId);
	root_put["CommandCode"]=OTUCommand_GetTariffByPubisherAndAcceptor;
	root_put["Num"]=Json::Value(0);
	root_put["ErrorString"]=Json::Value("Ok");
	root_put["OperationStatus"]=Json::Value(OTUStatus_Ok);

	root_put["TariffId"]=CurTariff.TariffId;

	root_put["Publisher"]=Publisher;
	root_put["Acceptor"]=Acceptor;
	root_put["CityId"]=CityId;

	root_put["ServicePercent"]=CurTariff.ServicePercent;
	root_put["ServiceFixed"]=CurTariff.ServiceFixed;
	root_put["PublisherPercent"]=CurTariff.PublisherPercent;
	root_put["PublisherFixed"]=CurTariff.PublisherFixed;
	root_put["ServiceReturnForeit"]=CurTariff.ServiceReturnForeit;
	root_put["PublisherReturnForeit"]=CurTariff.PublisherReturnForeit;
	root_put["MinOrderPrice"]=CurTariff.MinOrderPrice;

	Json::FastWriter writer;
	std::string StrPut=writer.write(root_put);
	WriteToDebugLog(s,0,false,"String to put");
	WriteToDebugLog(s,0,false,"%s",StrPut.c_str());
	SendTextBlock(s,StrPut);
	return 0;
}



int SetOrderNotifyMode(int s, bool IsAutent, unsigned int CommandId, Json::Value &root)
{
	WriteToDebugLog(s,0,false,"SetOrderNotifyMode");

	int Mode=root.get("Mode", "" ).asInt();
	int BeginTime=root.get("BeginTime", "" ).asUInt();
	int EndTime=root.get("EndTime", "" ).asUInt();

	std::map <int,ClientContext>::iterator clients_it;
	clients_it=clients.find(s);
	if(clients_it==clients.end())
	{
		WriteToDebugLog(s,0,true,"Absent client structure");
		return 0;
	}

	if(!IsAutent)
	{
		int ClientId=Authentif(s,IsAutent,CommandId,OTUCommand_SetOrderNotifyMode,root);
		if(ClientId<0)
			return ClientId;
	}

	clients_it->second.NotifyMode=Mode;
	clients_it->second.NotifyBeginTime=BeginTime;
	clients_it->second.NotifyEndTime=EndTime;

	Json::Value root_put;
	root_put["reason"]=Json::Value(OTaxiUltraBlockReason_NumStatus);
	root_put["CommandId"]=Json::Value(CommandId);
	root_put["CommandCode"]=OTUCommand_SetOrderNotifyMode;
	root_put["Num"]=Json::Value(0);
	root_put["ErrorString"]=Json::Value("Ok");
	root_put["OperationStatus"]=Json::Value(OTUStatus_Ok);

	Json::FastWriter writer;
	std::string StrPut=writer.write(root_put);
	WriteToDebugLog(s,0,false,"String to put");
	WriteToDebugLog(s,0,false,"%s",StrPut.c_str());
	SendTextBlock(s,StrPut);

	//уничтожаем старые заказы в соответствии с новыми правилами
	CheckOldOrders();

	//цикл по измененным заказам, которые не вписываются в новые временные рамки

	//получаем заказы из СУБД
	std::string AddStr=" WHERE \"Publisher\"=\'";
	AddStr+=ToString(clients_it->second.ClientId);
	AddStr+="\' OR \"Acceptor\"=\'";
	AddStr+=ToString(clients_it->second.ClientId);
	AddStr+="\';";
	std::list <OrderStruct> TmpOrderList=GetOrderList(AddStr);

	std::list <OrderStruct>::iterator TmpOrderListIt;
	for(TmpOrderListIt=TmpOrderList.begin();TmpOrderListIt!=TmpOrderList.end();TmpOrderListIt++)
	{
		if(IsNotifyCheck(TmpOrderListIt->WriteTime,clients_it->second))
		{
			Json::Value root_put;
			root_put["reason"]=Json::Value(OTaxiUltraBlockReason_NotifyChange);
			root_put["CommandId"]=Json::Value(0);
			root_put["CommandCode"]=Json::Value(0);
			root_put["Num"]=Json::Value(0);
			root_put["ErrorString"]=Json::Value("Ok");
			root_put["OperationStatus"]=Json::Value(OTUStatus_Ok);

			root_put["OrderId"]=Json::Value(TmpOrderListIt->OrderId);

			root_put["UniKey"]=Json::Value(TmpOrderListIt->UniKey);

			int OrderState=TmpOrderListIt->OrderState;
			if((OrderState==OrderStateAccept)&&(TmpOrderListIt->Acceptor==clients_it->second.ClientId))
				OrderState=OrderStateAcceptWe;
			root_put["UltraOrderState"]=Json::Value(OrderState);

			root_put["CityIdFrom"]=Json::Value(TmpOrderListIt->CityIdFrom);
			root_put["CityIdTo"]=Json::Value(TmpOrderListIt->CityIdTo);
			root_put["StreetFrom"]=Json::Value(TmpOrderListIt->StreetFrom);
			root_put["AdditionalFrom"]=Json::Value(TmpOrderListIt->AdditionalFrom);
			root_put["LatitudeFrom"]=Json::Value(TmpOrderListIt->LatitudeFrom);
			root_put["LongitudeFrom"]=Json::Value(TmpOrderListIt->LongitudeFrom);
			root_put["StreetTo"]=Json::Value(TmpOrderListIt->StreetTo);
			root_put["AdditionalTo"]=Json::Value(TmpOrderListIt->AdditionalTo);
			root_put["LatitudeTo"]=Json::Value(TmpOrderListIt->LatitudeTo);
			root_put["LongitudeTo"]=Json::Value(TmpOrderListIt->LongitudeTo);
			root_put["CarrierType"]=Json::Value(TmpOrderListIt->CarrierType);
			root_put["Price"]=Json::Value(TmpOrderListIt->Price);
			std::string PhoneNumber=TmpOrderListIt->ClientPhone;
			if((OrderState==OrderStateNew)&&(TmpOrderListIt->Publisher!=clients_it->second.ClientId))
			{
				PhoneNumber="";
			}
			root_put["ClientPhone"]=Json::Value(PhoneNumber);
			root_put["PhoneId"]=Json::Value(TmpOrderListIt->PhoneId);
			root_put["Comment"]=Json::Value(TmpOrderListIt->Comment);
			root_put["CarDescription"]=Json::Value(TmpOrderListIt->CarDescription);
			root_put["CarGovNumber"]=Json::Value(TmpOrderListIt->CarGovNumber);
			root_put["CarCallNumber"]=Json::Value(TmpOrderListIt->CarCallNumber);
			root_put["PreliminaryTime"]=Json::Value((unsigned int)(TmpOrderListIt->PreliminaryTime));
			root_put["Time0Km1Fixed2"]=Json::Value(TmpOrderListIt->Time0Km1Fixed2);
			root_put["StatusId"]=Json::Value(TmpOrderListIt->StatusId);
			root_put["WriteTime"]=Json::Value((unsigned int)(TmpOrderListIt->WriteTime));
			root_put["Publisher"]=Json::Value(TmpOrderListIt->Publisher);
			root_put["Acceptor"]=Json::Value(TmpOrderListIt->Acceptor);
			root_put["ServiceComission"]=Json::Value(TmpOrderListIt->ServiceComission);
			root_put["PublisherComission"]=Json::Value(TmpOrderListIt->PublisherComission);
			root_put["AcceptTime"]=Json::Value((unsigned int)(TmpOrderListIt->AcceptTime));

			root_put["TimeToLiveSec"]=Json::Value((int)(TmpOrderListIt->TimeToLiveSec));
			root_put["ToCarMin"]=Json::Value((int)(TmpOrderListIt->ToCarMin));
			root_put["SetCarInfoTime"]=Json::Value((unsigned int)(TmpOrderListIt->SetCarInfoTime));

			SentOrderInfo CurSOInfo;
			CurSOInfo.OrderId=TmpOrderListIt->OrderId;
			CurSOInfo.IsNew=false;
			CurSOInfo.WriteTime=TmpOrderListIt->WriteTime;
			clients_it->second.SentOrders.push_back(CurSOInfo);

			Json::FastWriter writer;
			std::string StrPut=writer.write(root_put);
			clients_it->second.AdditionalInfo+=StrPut;
			clients_it->second.AdditionalInfo+="\b";
		}
	}
	return 1;
}

int GetOrderFullInfo(int s, bool IsAutent, unsigned int CommandId, Json::Value &root)
{
	WriteToDebugLog(s,0,false,"GetOrderFullInfo");

	OTaxiUltraClientStatus Status=OTUStatus_CommonError;
	std::string ErrorString="Не удалось получить информацию о заказе";

	int OrderId=root.get("OrderId", "" ).asInt();

	std::map <int,ClientContext>::iterator clients_it;
	clients_it=clients.find(s);
	if(clients_it==clients.end())
	{
		WriteToDebugLog(s,0,true,"Absent client structure");
		return 0;
	}  

	if(!IsAutent)
	{
		int ClientId=Authentif(s,IsAutent,CommandId,OTUCommand_GetOrderFullInfo,root);
		if(ClientId<0)
			return ClientId;
	}

	std::map <int,ClientStruct>::iterator ClientDbIt=ClientDb.find(clients_it->second.ClientId);
	if(ClientDbIt==ClientDb.end())
	{
		WriteToDebugLog(s,0,true,"Absent ClientDbIt structure");
		return 0;
	}

	std::list <OrderStruct> CurList;
	OrderStruct CurOrder;
	std::string AdditionStr="WHERE \"id\"=\'";
	AdditionStr+=ToString(OrderId);
	AdditionStr+="\'";
	CurList=GetOrderList(AdditionStr);
	if(CurList.size()==1)
	{
		CurOrder=*CurList.begin();
		Status=OTUStatus_Ok;
		std::string ErrorString="Ok";
	}


	Json::Value root_put;
	root_put["reason"]=Json::Value(OTaxiUltraBlockReason_OrderFullInfo);
	root_put["CommandId"]=Json::Value(CommandId);
	root_put["CommandCode"]=OTUCommand_GetOrderFullInfo;
	root_put["Num"]=Json::Value(CurOrder.OrderId);
	root_put["ErrorString"]=Json::Value(ErrorString);
	root_put["OperationStatus"]=Json::Value(Status);

	root_put["OrderId"]=Json::Value(CurOrder.OrderId);
	root_put["UniKey"]=Json::Value(CurOrder.UniKey);
	int OrderState=CurOrder.OrderState;
	if((OrderState==OrderStateAccept)&&(CurOrder.Acceptor==clients_it->second.ClientId))
		OrderState=OrderStateAcceptWe;
	root_put["UltraOrderState"]=Json::Value(OrderState);
	root_put["CityIdFrom"]=Json::Value(CurOrder.CityIdFrom);
	root_put["CityIdTo"]=Json::Value(CurOrder.CityIdTo);
	root_put["StreetFrom"]=Json::Value(CurOrder.StreetFrom);
	root_put["AdditionalFrom"]=Json::Value(CurOrder.AdditionalFrom);
	root_put["LatitudeFrom"]=Json::Value(CurOrder.LatitudeFrom);
	root_put["LongitudeFrom"]=Json::Value(CurOrder.LongitudeFrom);
	root_put["StreetTo"]=Json::Value(CurOrder.StreetTo);
	root_put["AdditionalTo"]=Json::Value(CurOrder.AdditionalTo);
	root_put["LatitudeTo"]=Json::Value(CurOrder.LatitudeTo);
	root_put["LongitudeTo"]=Json::Value(CurOrder.LongitudeTo);
	root_put["CarrierType"]=Json::Value(CurOrder.CarrierType);
	root_put["Price"]=Json::Value(CurOrder.Price);
	std::string PhoneNumber=CurOrder.ClientPhone;
	if((OrderState==OrderStateNew)&&(CurOrder.Publisher!=clients_it->second.ClientId))
	{
		PhoneNumber="";
	}
	root_put["ClientPhone"]=Json::Value(PhoneNumber);
	root_put["PhoneId"]=Json::Value(CurOrder.PhoneId);
	root_put["Comment"]=Json::Value(CurOrder.Comment);
	root_put["CarDescription"]=Json::Value(CurOrder.CarDescription);
	root_put["CarGovNumber"]=Json::Value(CurOrder.CarGovNumber);
	root_put["CarCallNumber"]=Json::Value(CurOrder.CarCallNumber);
	root_put["PreliminaryTime"]=Json::Value((unsigned int)(CurOrder.PreliminaryTime));
	root_put["Time0Km1Fixed2"]=Json::Value(CurOrder.Time0Km1Fixed2);
	root_put["StatusId"]=Json::Value(CurOrder.StatusId);
	root_put["WriteTime"]=Json::Value((unsigned int)(CurOrder.WriteTime));
	root_put["Publisher"]=Json::Value(CurOrder.Publisher);
	root_put["Acceptor"]=Json::Value(CurOrder.Acceptor);
	root_put["ServiceComission"]=Json::Value(CurOrder.ServiceComission);
	root_put["PublisherComission"]=Json::Value(CurOrder.PublisherComission);
	root_put["AcceptTime"]=Json::Value((unsigned int)(CurOrder.AcceptTime));
	root_put["TimeToLiveSec"]=Json::Value((int)(CurOrder.TimeToLiveSec));
	root_put["ToCarMin"]=Json::Value((int)(CurOrder.ToCarMin));
	root_put["SetCarInfoTime"]=Json::Value((unsigned int)(CurOrder.SetCarInfoTime));


	Json::FastWriter writer;
	std::string StrPut=writer.write(root_put);
	WriteToDebugLog(s,0,false,"String to put");
	WriteToDebugLog(s,0,false,"%s",StrPut.c_str());
	SendTextBlock(s,StrPut);
	return 1;
}

int GetOrderPartInfo(int s,bool IsAutent,unsigned int CommandId, Json::Value &root)
{
	WriteToDebugLog(s,0,false,"GetOrderFullInfo");

	OTaxiUltraClientStatus Status=OTUStatus_CommonError;
	std::string ErrorString="Не удалось получить информацию о заказе";

	int OrderId=root.get("OrderId", "" ).asInt();

	std::map <int,ClientContext>::iterator clients_it;
	clients_it=clients.find(s);
	if(clients_it==clients.end())
	{
		WriteToDebugLog(s,0,true,"Absent client structure");
		return 0;
	}

	if(!IsAutent)
	{
		int ClientId=Authentif(s,IsAutent,CommandId,OTUCommand_GetOrderPartialInfo,root);
		if(ClientId<0)
			return ClientId;
	}

	std::map <int,ClientStruct>::iterator ClientDbIt=ClientDb.find(clients_it->second.ClientId);
	if(ClientDbIt==ClientDb.end())
	{
		WriteToDebugLog(s,0,true,"Absent ClientDbIt structure");
		return 0;
	}

	std::list <OrderStruct> CurList;
	OrderStruct CurOrder;
	std::string AdditionStr="WHERE \"id\"=\'";
	AdditionStr+=ToString(OrderId);
	AdditionStr+="\'";
	CurList=GetOrderList(AdditionStr);
	if(CurList.size()==1)
	{
		CurOrder=*CurList.begin();
		Status=OTUStatus_Ok;
		std::string ErrorString="Ok";
	}


	Json::Value root_put;
	root_put["reason"]=Json::Value(OTaxiUltraBlockReason_OrderPartialInfo);
	root_put["CommandId"]=Json::Value(CommandId);
	root_put["CommandCode"]=OTUCommand_GetOrderPartialInfo;
	root_put["Num"]=Json::Value(CurOrder.OrderId);
	root_put["ErrorString"]=Json::Value(ErrorString);
	root_put["OperationStatus"]=Json::Value(Status);

	root_put["OrderId"]=Json::Value(CurOrder.OrderId);
	int OrderState=CurOrder.OrderState;
	if((OrderState==OrderStateAccept)&&(CurOrder.Acceptor==clients_it->second.ClientId))
				OrderState=OrderStateAcceptWe;
	root_put["UltraOrderState"]=Json::Value(OrderState);
	root_put["Price"]=Json::Value(CurOrder.Price);
	std::string PhoneNumber=CurOrder.ClientPhone;
	if((OrderState==OrderStateNew)&&(CurOrder.Publisher!=clients_it->second.ClientId))
	{
		PhoneNumber="";
	}
	root_put["ClientPhone"]=Json::Value(PhoneNumber);
	root_put["PhoneId"]=Json::Value(CurOrder.PhoneId);
	root_put["CarDescription"]=Json::Value(CurOrder.CarDescription);
	root_put["CarGovNumber"]=Json::Value(CurOrder.CarGovNumber);
	root_put["CarCallNumber"]=Json::Value(CurOrder.CarCallNumber);
	root_put["StatusId"]=Json::Value(CurOrder.StatusId);
	root_put["AcceptTime"]=Json::Value((unsigned int)(CurOrder.AcceptTime));
	root_put["Acceptor"]=Json::Value(CurOrder.Acceptor);
	root_put["ServiceComission"]=Json::Value(CurOrder.ServiceComission);
	root_put["PublisherComission"]=Json::Value(CurOrder.PublisherComission);

	Json::FastWriter writer;
	std::string StrPut=writer.write(root_put);
	WriteToDebugLog(s,0,false,"String to put");
	WriteToDebugLog(s,0,false,"%s",StrPut.c_str());
	SendTextBlock(s,StrPut);
	return 1;
}


int SendTextMessage(int s,bool IsAutent,unsigned int CommandId, Json::Value &root)
{
	WriteToDebugLog(s,0,false,"SendTextMessage");

	int ClientId=root.get("ClientId", "" ).asInt();
	std::string MessageText=root.get("MessageText", "" ).asString();

	if(!IsAutent)
	{
		int ClientId=Authentif(s,IsAutent,CommandId,OTUCommand_SendMessage,root);
		if(ClientId<0)
			return ClientId;
	}

	///////////////

	//цикл по всем клиентам
	std::map <int,ClientContext>::iterator clients_it;
	for(clients_it=clients.begin();clients_it!=clients.end();clients_it++)
	{
		Json::Value root_put;
		root_put["reason"]=Json::Value(OTaxiUltraBlockReason_TextMessage);
		root_put["CommandId"]=Json::Value(0);
		root_put["CommandCode"]=Json::Value(0);
		root_put["Num"]=Json::Value(0);
		root_put["ErrorString"]=Json::Value(MessageText);
		root_put["OperationStatus"]=Json::Value(0);
		root_put["OrderId"]=Json::Value(-1);

		if(clients_it->second.ClientId==ClientId)
		{
			WriteToDebugLog(s,0,false,"ClientId=%d SocketId=%d",ClientId,clients_it->first);
			Json::FastWriter writer;
			std::string StrPut=writer.write(root_put);
			clients_it->second.AdditionalInfo+=StrPut;
			clients_it->second.AdditionalInfo+="\b";
		}
	}

	///////////////
	std::string ErrorString="Ok";
	int Status=OTUStatus_Ok;

	Json::Value root_put;
	root_put["reason"]=Json::Value(OTaxiUltraBlockReason_NumStatus);
	root_put["CommandId"]=Json::Value(CommandId);
	root_put["CommandCode"]=OTUCommand_SendMessage;
	root_put["Num"]=Json::Value(-1);
	root_put["ErrorString"]=Json::Value(ErrorString);
	root_put["OperationStatus"]=Json::Value(Status);

	Json::FastWriter writer;
	std::string StrPut=writer.write(root_put);
	WriteToDebugLog(s,0,false,"String to put");
	WriteToDebugLog(s,0,false,"%s",StrPut.c_str());
	SendTextBlock(s,StrPut);
	return 0;
}


void CheckOldOrders() //тут должна выполняться чистка старых новых заказов
{
	std::list <OrderStruct>::iterator FreeOrderListIt,FreeOrderListIt1;
	time_t CurTime=time(0);
	//цикл по новым заказам
	for(FreeOrderListIt=FreeOrderList.begin();FreeOrderListIt!=FreeOrderList.end();)
	{
		//время заказа истекло
		if(((FreeOrderListIt->TimeToLiveSec>0)&&(FreeOrderListIt->WriteTime+FreeOrderListIt->TimeToLiveSec<CurTime))||(FreeOrderListIt->WriteTime+ExpiredTime<CurTime))
		{
			WriteToDebugLog(0,0,false,"FreeOrderList - item OrderId=%d delete",FreeOrderListIt->OrderId);
			//заказ записывается в БД со статусом "возврат публикатору"
			WriteToDebugLog(0,0,false,"CheckOldOrders - PropagateOrder >");

			OrderStruct CurOrder;
			WriteOrderParamsDB(FreeOrderListIt->OrderId,-1,GetReturnedStatusId(),OrderStateReturned,&CurOrder);

			WriteToDebugLog(0,0,false,"CheckOldOrders - PropagateOrder >");

			//все заинтересованные клиенты получают сообщение об удалении заказа из списка новых
			PropagateOrder(OTaxiUltraBlockReason_NotifyNewDel, *FreeOrderListIt);
			//устанвка нового статуса возвращаемому заказу
			FreeOrderListIt->StatusId=GetReturnedStatusId();
			FreeOrderListIt->OrderState=OrderStateReturned;
			PropagateOrder(OTaxiUltraBlockReason_NotifyChange, *FreeOrderListIt);

			WriteToDebugLog(0,0,false,"CheckOldOrders - PropagateOrder <");

			//заказ удаляется из списка свободных заказаов
			FreeOrderListIt1=FreeOrderListIt;
			FreeOrderListIt1++;
			FreeOrderList.erase(FreeOrderListIt);
			FreeOrderListIt=FreeOrderListIt1;
		}
		else
		{
			FreeOrderListIt++;
		}
	}

	//цикл по измененным заказам, которые не вписываются в новые временные рамки
	std::map <int,ClientContext>::iterator clients_it;
	for(clients_it=clients.begin();clients_it!=clients.end();clients_it++)
	{
		std::list <SentOrderInfo>::iterator SentOrdersIt;
		for(SentOrdersIt=clients_it->second.SentOrders.begin();SentOrdersIt!=clients_it->second.SentOrders.end();)
		{
			if((SentOrdersIt->IsNew==false)&&(!IsNotifyCheck(SentOrdersIt->WriteTime,clients_it->second)))
			{
				Json::Value root_put;
				root_put["reason"]=Json::Value(OTaxiUltraBlockReason_NotifyChangeDel);
				root_put["CommandId"]=Json::Value(0);
				root_put["CommandCode"]=Json::Value(0);
				root_put["Num"]=Json::Value(0);
				root_put["ErrorString"]=Json::Value("Ok");
				root_put["OperationStatus"]=Json::Value(OTUStatus_Ok);

				root_put["OrderId"]=Json::Value(SentOrdersIt->OrderId);

				std::list <SentOrderInfo>::iterator SentOrdersIt1=SentOrdersIt;
				SentOrdersIt1++;
				clients_it->second.SentOrders.erase(SentOrdersIt);
				SentOrdersIt=SentOrdersIt1;

				Json::FastWriter writer;
				std::string StrPut=writer.write(root_put);
				clients_it->second.AdditionalInfo+=StrPut;
				clients_it->second.AdditionalInfo+="\b";
				WriteToDebugLog(0,0,false,"CheckOldOrders ClientId=%d, AdditionalInfo=%s",clients_it->second.ClientId,clients_it->second.AdditionalInfo.c_str());
			}
			else
			{
				SentOrdersIt++;
			}
		}
	}
	CheckOldOrdersDB();
}

void SentOrderToNewConnection(int s)
{
	std::map <int,ClientContext>::iterator clients_it;
	clients_it=clients.find(s);
	if(clients_it==clients.end())
	{
		WriteToDebugLog(s,0,true,"Absent client structure. socket=%d",s);
		return;
	}
	std::map <int,ClientStruct>::iterator ClientDbIt=ClientDb.find(clients_it->second.ClientId);
	if(ClientDbIt==ClientDb.end())
	{
		WriteToDebugLog(s,0,true,"Absent ClientDbIt structure. ClientId=%d",clients_it->second.ClientId);
		return;
	}

	std::list <OrderStruct>::iterator FreeOrderListIt;
	for(FreeOrderListIt=FreeOrderList.begin();FreeOrderListIt!=FreeOrderList.end();FreeOrderListIt++)
	{
		if(IsInCityList(FreeOrderListIt->CityIdFrom, ClientDbIt->second.CityIdList) && IsInCityList(FreeOrderListIt->CityIdTo, ClientDbIt->second.CityIdList))
		{
			if(FreeOrderListIt->Publisher==clients_it->second.ClientId)
				continue;

			Json::Value root_put;
			root_put["reason"]=Json::Value(OTaxiUltraBlockReason_NotifyNew);
			root_put["CommandId"]=Json::Value(0);
			root_put["CommandCode"]=Json::Value(0);
			root_put["Num"]=Json::Value(0);
			root_put["ErrorString"]=Json::Value("Ok");
			root_put["OperationStatus"]=Json::Value(OTUStatus_Ok);

			root_put["OrderId"]=Json::Value(FreeOrderListIt->OrderId);
			root_put["UniKey"]=Json::Value(FreeOrderListIt->UniKey);
			int OrderState=FreeOrderListIt->OrderState;
			if((OrderState==OrderStateAccept)&&(FreeOrderListIt->Acceptor==clients_it->second.ClientId))
				OrderState=OrderStateAcceptWe;
			root_put["UltraOrderState"]=Json::Value(OrderState);
			root_put["CityIdFrom"]=Json::Value(FreeOrderListIt->CityIdFrom);
			root_put["CityIdTo"]=Json::Value(FreeOrderListIt->CityIdTo);
			root_put["StreetFrom"]=Json::Value(FreeOrderListIt->StreetFrom);
			root_put["AdditionalFrom"]=Json::Value(FreeOrderListIt->AdditionalFrom);
			root_put["LatitudeFrom"]=Json::Value(FreeOrderListIt->LatitudeFrom);
			root_put["LongitudeFrom"]=Json::Value(FreeOrderListIt->LongitudeFrom);
			root_put["StreetTo"]=Json::Value(FreeOrderListIt->StreetTo);
			root_put["AdditionalTo"]=Json::Value(FreeOrderListIt->AdditionalTo);
			root_put["LatitudeTo"]=Json::Value(FreeOrderListIt->LatitudeTo);
			root_put["LongitudeTo"]=Json::Value(FreeOrderListIt->LongitudeTo);
			root_put["CarrierType"]=Json::Value(FreeOrderListIt->CarrierType);
			root_put["Price"]=Json::Value(FreeOrderListIt->Price);
			std::string PhoneNumber=FreeOrderListIt->ClientPhone;
			if((OrderState==OrderStateNew)&&(FreeOrderListIt->Publisher!=clients_it->second.ClientId))
			{
				PhoneNumber="";
			}
			root_put["ClientPhone"]=Json::Value(PhoneNumber);
			root_put["PhoneId"]=Json::Value(FreeOrderListIt->PhoneId);
			root_put["Comment"]=Json::Value(FreeOrderListIt->Comment);
			root_put["CarDescription"]=Json::Value(FreeOrderListIt->CarDescription);
			root_put["CarGovNumber"]=Json::Value(FreeOrderListIt->CarGovNumber);
			root_put["CarCallNumber"]=Json::Value(FreeOrderListIt->CarCallNumber);
			root_put["PreliminaryTime"]=Json::Value((unsigned int)(FreeOrderListIt->PreliminaryTime));
			root_put["Time0Km1Fixed2"]=Json::Value(FreeOrderListIt->Time0Km1Fixed2);
			root_put["StatusId"]=Json::Value(FreeOrderListIt->StatusId);
			root_put["WriteTime"]=Json::Value((unsigned int)(FreeOrderListIt->WriteTime));
			root_put["Publisher"]=Json::Value(FreeOrderListIt->Publisher);
			root_put["Acceptor"]=Json::Value(FreeOrderListIt->Acceptor);
			root_put["ServiceComission"]=Json::Value(FreeOrderListIt->ServiceComission);
			root_put["PublisherComission"]=Json::Value(FreeOrderListIt->PublisherComission);
			root_put["AcceptTime"]=Json::Value((unsigned int)(FreeOrderListIt->AcceptTime));
			root_put["TimeToLiveSec"]=Json::Value((int)(FreeOrderListIt->TimeToLiveSec));
			root_put["ToCarMin"]=Json::Value((int)(FreeOrderListIt->ToCarMin));
			root_put["SetCarInfoTime"]=Json::Value((unsigned int)(FreeOrderListIt->SetCarInfoTime));

			Json::FastWriter writer;
			std::string StrPut=writer.write(root_put);
			clients_it->second.AdditionalInfo+=StrPut;
			clients_it->second.AdditionalInfo+="\b";
		}
	}
}

void PutAsyncMessages() //тут должна выполняться рассылка строк из AdditionalString
{
	//цикл по всем клиентам
	WriteToDebugLog(0,0,false,"PutAsyncMessages");

	std::map <int,ClientContext>::iterator clients_it,clients_it1;
	for(clients_it=clients.begin();clients_it!=clients.end();clients_it++)
	{
		bool IsSent;
		do
		{
			IsSent=false;
			std::string::size_type Sep=clients_it->second.AdditionalInfo.find_first_of('\b');
			if(Sep!=std::string::npos)
			{
				std::string CurString=clients_it->second.AdditionalInfo.substr(0,Sep);
				WriteToDebugLog(clients_it->first,clients_it->second.ClientId,false,"String to put: %s",CurString.c_str());
				if(SendTextBlock(clients_it->first,CurString))
				{
					clients_it->second.AdditionalInfo=clients_it->second.AdditionalInfo.substr(Sep+1,std::string::npos);
					IsSent=true;
				}
				else
				{
					WriteToDebugLog(clients_it->first,clients_it->second.ClientId,true,"Send error");
					closesocket(clients_it->first);
					clients_it1=clients_it,clients_it++;
					clients.erase(clients_it1);
					WriteConnectonState();
					continue;

				}
			}
		}while(IsSent);
	}
}

void PropagateOrder(int Reason, const OrderStruct &Order)
{
	WriteToDebugLog(0,0,false,"PropagateOrder Reason=%d, OrderId=%d",Reason,Order.OrderId);

	//цикл по всем клиентам
	std::map <int,ClientContext>::iterator clients_it;
	for(clients_it=clients.begin();clients_it!=clients.end();clients_it++)
	{
		Json::Value root_put;
		root_put["reason"]=Json::Value(Reason);
		root_put["CommandId"]=Json::Value(0);
		root_put["CommandCode"]=Json::Value(0);
		root_put["Num"]=Json::Value(0);
		root_put["ErrorString"]=Json::Value("Ok");
		root_put["OperationStatus"]=Json::Value(0);

		root_put["OrderId"]=Json::Value(Order.OrderId);

		//новые заказы рассылаются только такси, работающие в городах, указанных в полях "откуда" и "куда" заказа
		if((Reason==OTaxiUltraBlockReason_NotifyNew)||(Reason==OTaxiUltraBlockReason_NotifyNewDel))
		{
			std::map <int,ClientStruct>::iterator ClientDbIt=ClientDb.find(clients_it->second.ClientId);

			if(ClientDbIt!=ClientDb.end())
			{
				if(Reason==OTaxiUltraBlockReason_NotifyNew)
				{
					if((Order.Publisher==clients_it->second.ClientId))
					{
						WriteToDebugLog(0,0,false,"PropagateOrder ClientId=%d Order.Publisher==Publisher",clients_it->second.ClientId);
						continue;
					}

					if(RelationsCheck(Order.Publisher,clients_it->second.ClientId)<0)
					{
						WriteToDebugLog(0,0,false,"PropagateOrder ClientId=%d RelationsCheck",clients_it->second.ClientId);
						continue;
					}
				}

				const std::list<int> &ClientCityList=ClientDbIt->second.CityIdList;
				if(IsInCityList(Order.CityIdFrom, ClientCityList) /*&& IsInCityList(Order.CityIdTo, ClientCityList)*/)
				{
					if(Reason==OTaxiUltraBlockReason_NotifyNew)
					{
						root_put["UniKey"]=Json::Value(Order.UniKey);

						int OrderState=Order.OrderState;
						if((OrderState==OrderStateAccept)&&(Order.Acceptor==clients_it->second.ClientId))
							OrderState=OrderStateAcceptWe;
						root_put["UltraOrderState"]=Json::Value(OrderState);
						root_put["CityIdFrom"]=Json::Value(Order.CityIdFrom);
						root_put["CityIdTo"]=Json::Value(Order.CityIdTo);
						root_put["StreetFrom"]=Json::Value(Order.StreetFrom);
						root_put["AdditionalFrom"]=Json::Value(Order.AdditionalFrom);
						root_put["LatitudeFrom"]=Json::Value(Order.LatitudeFrom);
						root_put["LongitudeFrom"]=Json::Value(Order.LongitudeFrom);
						root_put["StreetTo"]=Json::Value(Order.StreetTo);
						root_put["AdditionalTo"]=Json::Value(Order.AdditionalTo);
						root_put["LatitudeTo"]=Json::Value(Order.LatitudeTo);
						root_put["LongitudeTo"]=Json::Value(Order.LongitudeTo);
						root_put["CarrierType"]=Json::Value(Order.CarrierType);
						root_put["Price"]=Json::Value(Order.Price);
						std::string PhoneNumber=Order.ClientPhone;
						if((OrderState==OrderStateNew)&&(Order.Publisher!=clients_it->second.ClientId))
						{
							PhoneNumber="";
						}
						root_put["ClientPhone"]=Json::Value(PhoneNumber);
						root_put["PhoneId"]=Json::Value(Order.PhoneId);
						root_put["Comment"]=Json::Value(Order.Comment);
						root_put["CarDescription"]=Json::Value(Order.CarDescription);
						root_put["CarGovNumber"]=Json::Value(Order.CarGovNumber);
						root_put["CarCallNumber"]=Json::Value(Order.CarCallNumber);
						root_put["PreliminaryTime"]=Json::Value((unsigned int)(Order.PreliminaryTime));
						root_put["Time0Km1Fixed2"]=Json::Value(Order.Time0Km1Fixed2);
						root_put["StatusId"]=Json::Value(Order.StatusId);
						root_put["WriteTime"]=Json::Value((unsigned int)(Order.WriteTime));
						root_put["Publisher"]=Json::Value(Order.Publisher);
						root_put["Acceptor"]=Json::Value(Order.Acceptor);
						root_put["ServiceComission"]=Json::Value(Order.ServiceComission);
						root_put["PublisherComission"]=Json::Value(Order.PublisherComission);
						root_put["AcceptTime"]=Json::Value((unsigned int)(Order.AcceptTime));
						root_put["TimeToLiveSec"]=Json::Value((int)(Order.TimeToLiveSec));
						root_put["ToCarMin"]=Json::Value((int)(Order.ToCarMin));
						root_put["SetCarInfoTime"]=Json::Value((unsigned int)(Order.SetCarInfoTime));
						SentOrderInfo CurSOInfo;
						CurSOInfo.OrderId=Order.OrderId;
						CurSOInfo.IsNew=true;
						CurSOInfo.WriteTime=Order.WriteTime;
						clients_it->second.SentOrders.push_back(CurSOInfo);
					}
					if(Reason==OTaxiUltraBlockReason_NotifyNewDel) //Del передаем только хозяину заказа и тем соединениям, которые в свое время получили уведомление OTaxiUltraBlockReason_NotifyNew 
					{
						std::list <SentOrderInfo>::iterator SentOrdersIt,SentOrdersIt1;
						bool bFind=false;
						if((Order.Publisher==clients_it->second.ClientId)||(Order.Acceptor==clients_it->second.ClientId))
						{
							WriteToDebugLog(0,0,false,"Del New Order send to publisher/acceptor");
							bFind=true;
						}
						for(SentOrdersIt=clients_it->second.SentOrders.begin();SentOrdersIt!=clients_it->second.SentOrders.end();)
						{
							if((SentOrdersIt->OrderId==Order.OrderId)&&(SentOrdersIt->IsNew==true))
							{
								bFind=true;
								SentOrdersIt1=SentOrdersIt;
								SentOrdersIt1++;
								clients_it->second.SentOrders.erase(SentOrdersIt);
								SentOrdersIt=SentOrdersIt1;
							}
							else
								SentOrdersIt++;
						}
						if(!bFind)
							continue;
					}
					Json::FastWriter writer;
					std::string StrPut=writer.write(root_put);
					clients_it->second.AdditionalInfo+=StrPut;
					clients_it->second.AdditionalInfo+="\b";
				}
				else
				{
					WriteToDebugLog(0,0,false,"PropagateOrder ClientId=%d Not in city list",clients_it->second.ClientId);
				}
			}
		}
		//изменения заказов рассылаются только такси-публикатору и такси-аксептору заказа
		if((Reason==OTaxiUltraBlockReason_NotifyChange)||(Reason==OTaxiUltraBlockReason_NotifyChangeDel))
		{
			if((clients_it->second.ClientId==Order.Publisher) || (clients_it->second.ClientId==Order.Acceptor))
			{
				if(IsNotifyCheck(Order.WriteTime,clients_it->second))
				{
					if(Reason==OTaxiUltraBlockReason_NotifyChange)
					{
						root_put["UniKey"]=Json::Value(Order.UniKey);
						int OrderState=Order.OrderState;
						if((OrderState==OrderStateAccept)&&(Order.Acceptor==clients_it->second.ClientId))
							OrderState=OrderStateAcceptWe;
						root_put["UltraOrderState"]=Json::Value(OrderState);
						root_put["CityIdFrom"]=Json::Value(Order.CityIdFrom);
						root_put["CityIdTo"]=Json::Value(Order.CityIdTo);
						root_put["StreetFrom"]=Json::Value(Order.StreetFrom);
						root_put["AdditionalFrom"]=Json::Value(Order.AdditionalFrom);
						root_put["LatitudeFrom"]=Json::Value(Order.LatitudeFrom);
						root_put["LongitudeFrom"]=Json::Value(Order.LongitudeFrom);
						root_put["StreetTo"]=Json::Value(Order.StreetTo);
						root_put["AdditionalTo"]=Json::Value(Order.AdditionalTo);
						root_put["LatitudeTo"]=Json::Value(Order.LatitudeTo);
						root_put["LongitudeTo"]=Json::Value(Order.LongitudeTo);
						root_put["CarrierType"]=Json::Value(Order.CarrierType);
						root_put["Price"]=Json::Value(Order.Price);
						std::string PhoneNumber=Order.ClientPhone;
						if((OrderState==OrderStateNew)&&(Order.Publisher!=clients_it->second.ClientId))
						{
							PhoneNumber="";
						}
						root_put["ClientPhone"]=Json::Value(PhoneNumber);
						root_put["PhoneId"]=Json::Value(Order.PhoneId);
						root_put["Comment"]=Json::Value(Order.Comment);
						root_put["CarDescription"]=Json::Value(Order.CarDescription);
						root_put["CarGovNumber"]=Json::Value(Order.CarGovNumber);
						root_put["CarCallNumber"]=Json::Value(Order.CarCallNumber);
						root_put["PreliminaryTime"]=Json::Value((unsigned int)(Order.PreliminaryTime));
						root_put["Time0Km1Fixed2"]=Json::Value(Order.Time0Km1Fixed2);
						root_put["StatusId"]=Json::Value(Order.StatusId);
						root_put["WriteTime"]=Json::Value((unsigned int)(Order.WriteTime));
						root_put["Publisher"]=Json::Value(Order.Publisher);
						root_put["Acceptor"]=Json::Value(Order.Acceptor);
						root_put["ServiceComission"]=Json::Value(Order.ServiceComission);
						root_put["PublisherComission"]=Json::Value(Order.PublisherComission);
						root_put["AcceptTime"]=Json::Value((unsigned int)(Order.AcceptTime));
						root_put["TimeToLiveSec"]=Json::Value((int)(Order.TimeToLiveSec));
						root_put["ToCarMin"]=Json::Value((int)(Order.ToCarMin));
						root_put["SetCarInfoTime"]=Json::Value((unsigned int)(Order.SetCarInfoTime));
						SentOrderInfo CurSOInfo;
						CurSOInfo.OrderId=Order.OrderId;
						CurSOInfo.IsNew=false;
						CurSOInfo.WriteTime=Order.WriteTime;
						clients_it->second.SentOrders.push_back(CurSOInfo);
					}
					if(Reason==OTaxiUltraBlockReason_NotifyChangeDel) //Del передаем только тем соединениям, которые в свое время получили уведомление OTaxiUltraBlockReason_NotifyNew 
					{
						std::list <SentOrderInfo>::iterator SentOrdersIt,SentOrdersIt1;
						bool bFind=false;
						for(SentOrdersIt=clients_it->second.SentOrders.begin();SentOrdersIt!=clients_it->second.SentOrders.end();)
						{
							if((SentOrdersIt->OrderId==Order.OrderId)&&(SentOrdersIt->IsNew==false))
							{
								bFind=true;
								SentOrdersIt1=SentOrdersIt;
								SentOrdersIt1++;
								clients_it->second.SentOrders.erase(SentOrdersIt);
								SentOrdersIt=SentOrdersIt1;
							}
							else
								SentOrdersIt++;
						}
						if(!bFind)
							continue;
					}
					Json::FastWriter writer;
					std::string StrPut=writer.write(root_put);
					clients_it->second.AdditionalInfo+=StrPut;
					clients_it->second.AdditionalInfo+="\b";
				}
			}
		}
		WriteToDebugLog(0,0,false,"PropagateOrder ClientId=%d, AdditionalInfo=%s",clients_it->second.ClientId,clients_it->second.AdditionalInfo.c_str());
	}
}

//проверка, надо ли уведомлять об изменении данного клиента
bool IsNotifyCheck(time_t WriteTime,const ClientContext &Client)
{
	switch(Client.NotifyMode)
	{
	case -1: ///ни одного заказа
		{
			return false;
		}
	case 0: ///все заказы
		{
			return true;
		}
	case 1:
		{
			time_t CurTime=time(0);
			if(CurTime-Client.NotifyBeginTime<WriteTime)
				return true;
			else
				return false;
		}
	default:
		{
			if((Client.NotifyBeginTime<WriteTime)&&(Client.NotifyEndTime>WriteTime))
				return true;
			else
				return false;
		}
	}
	return false;
}

